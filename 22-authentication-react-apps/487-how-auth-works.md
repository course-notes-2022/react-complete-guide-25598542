# How Authentication Works

Let's learn how **authentication** works.

## What is Authentication? Why Do We Need It?

Authentication is needed if **content should be protected**, i.e. the content
should _not_ be accessible by everyone. In this case, the requesting application
should **authenticate**, i.e. get **permission** from the server where the
content is **hosted** to access the content.

## How Does the React App Get this Permission?

To obtain the permission from the server, the client sends a **request** with
**user credentials** such as an email/password combination. If the credentials
are **valid** the server sends back a **response** that grants the user access
to the resources.

### What Does this Response Look Like?

The server must respond with a response that can be **validated**. The server
must be able to inspect the response and confirm that it was indeed issued
correctly to the identified user, and is un-expired.

There are 2 main solutions: **server-side sessions** and **authenticated
tokens**.

Server-side sessions store a uniqe identifier on the server and send the same
identifier to the client. The client sends the identifier along with future
requests for protected resources. The server can check if the ID is valid (i.e.
previously issued by server to client).

The downside of server-side sessions is that it requires **tight coupling**
between the client and server. With React apps, we're often dealing with
**decoupled clients**:

![decoupled clients](./screenshots/decoupled-clients.png)

This is where **authentication tokens** come in.

### How Do Authentication Tokens Work?

After a user sends valid credentials, we create (but do not store) a "token" and
**send it to the client**.

Important: the **validity of the token** can be proven **only by the server that
issued it**, because the token was created with the help of a "secret" that
_only the creating server knows_. The client attaches the token to **future
requests** to the server, and if the server determines that the token was issued
by it and is valid, the server grants permission to the client on the requested
resource.

We can see an example of this in action in the `backend/util/auth.js` file of
the chapter project:

```js
const { sign, verify } = require('jsonwebtoken');
const { compare } = require('bcryptjs');
const { NotAuthError } = require('./errors');

const KEY = 'supersecret';

// Creates the auth token
function createJSONToken(email) {
  return sign({ email }, KEY, { expiresIn: '1h' });
}

// Validates the token passed from the client
function validateJSONToken(token) {
  return verify(token, KEY);
}

function isValidPassword(password, storedPassword) {
  return compare(password, storedPassword);
}

// Intercepts client requests and checks for
// valid auth token before granting/denying access
function checkAuthMiddleware(req, res, next) {
  if (req.method === 'OPTIONS') {
    return next();
  }
  if (!req.headers.authorization) {
    console.log('NOT AUTH. AUTH HEADER MISSING.');
    return next(new NotAuthError('Not authenticated.'));
  }
  const authFragments = req.headers.authorization.split(' ');

  if (authFragments.length !== 2) {
    console.log('NOT AUTH. AUTH HEADER INVALID.');
    return next(new NotAuthError('Not authenticated.'));
  }
  const authToken = authFragments[1];
  try {
    const validatedToken = validateJSONToken(authToken);
    req.token = validatedToken;
  } catch (error) {
    console.log('NOT AUTH. TOKEN INVALID.');
    return next(new NotAuthError('Not authenticated.'));
  }
  next();
}

exports.createJSONToken = createJSONToken;
exports.validateJSONToken = validateJSONToken;
exports.isValidPassword = isValidPassword;
exports.checkAuth = checkAuthMiddleware;
```

This implies that we need to **store the auth token on the client side** and
send it with each request that requires authentication! We'll implement this
throughout this module.
