# Working With Query Parameters

Let's learn about a new routing concept we haven't seen yet.

Inspect the `AuthForm` component:

```js
import { useState } from 'react';
import { Form } from 'react-router-dom';

import classes from './AuthForm.module.css';

function AuthForm() {
  const [isLogin, setIsLogin] = useState(true);

  function switchAuthHandler() {
    setIsLogin((isCurrentlyLogin) => !isCurrentlyLogin);
  }

  return (
    <>
      <Form method="post" className={classes.form}>
        <h1>{isLogin ? 'Log in' : 'Create a new user'}</h1>
        <p>
          <label htmlFor="email">Email</label>
          <input id="email" type="email" name="email" required />
        </p>
        <p>
          <label htmlFor="image">Password</label>
          <input id="password" type="password" name="password" required />
        </p>
        <div className={classes.actions}>
          <button onClick={switchAuthHandler} type="button">
            {isLogin ? 'Create new user' : 'Login'}
          </button>
          <button>Save</button>
        </div>
      </Form>
    </>
  );
}

export default AuthForm;
```

Note that we're currently using a piece of **internal state**, `isLogin`, to
dynamically display a title and button text depending on its value.

This is a **completely valid approach**, and we could continue to do it this
way. However, we can _also_ use this opportunity to learn how to leverage
**query parameters** in our application.

## Using Query Parameters

**Query parameters** (or "search parameters") are additional data that we can
append to a URL path that allow us to add **additional parameters** to a URL
query. Query parameters appear after a `?` in the URL path:

> `http://localhost/auth?mode=signup`

The advantage of using query params is that we can now use the same **route** to
directly link users to _either_ the `signup` _or_ `login` UIs.

Let's refactor `AuthForm.js` to use query parameters:

```js
import { Form, Link, useSearchParams } from 'react-router-dom';

import classes from './AuthForm.module.css';

function AuthForm() {
  const [searchParams, setSearchParams] = useSearchParams();
  const isLogin = searchParams.get('mode') === 'login';

  return (
    <>
      <Form method="post" className={classes.form}>
        <h1>{isLogin ? 'Log in' : 'Create a new user'}</h1>
        <p>
          <label htmlFor="email">Email</label>
          <input id="email" type="email" name="email" required />
        </p>
        <p>
          <label htmlFor="image">Password</label>
          <input id="password" type="password" name="password" required />
        </p>
        <div className={classes.actions}>
          <Link to={`?mode=${isLogin ? 'signup' : 'login'}`}>
            {isLogin ? 'Create new user' : 'Login'}
          </Link>
          <button>Save</button>
        </div>
      </Form>
    </>
  );
}

export default AuthForm;
```

Save changes, and note that we can now click between the `login` and `signup`
states using the `Link` and the **query parameters update accordingly**:

![use search params](./screenshots/use-search-params.png)
