# Managing the Token Expiration

What is the flaw?

Imagine that the user logs in at 1:00pm, uses the application for 10 minutes,
and then **refreshes the browser**. The `RootLayout` component will be
**re-rendered**, and start a new timer counting down from 60 minutes, however
the **token will still expire in 50 minutes**. Therefore, it's not enough to
always set the timer to 1 hour. We need to manage and register the **actual
token expiration**.

To do this, we need to store the **token expiration time** on local storage
alongside the token.

Open `Authentication.js`, and add the following:

```js
import { json, redirect } from 'react-router-dom';
import AuthForm from '../components/AuthForm';

function AuthenticationPage() {
  return <AuthForm />;
}

export default AuthenticationPage;

export async function action({ request }) {
  const searchParams = new URL(request.url).searchParams;
  const mode = searchParams.get('mode') || 'signup';

  if (mode !== 'login' && mode !== 'signup') {
    throw json({ message: 'Unsupported mode' }, { status: 422 });
  }
  const data = await request.formData();
  const authData = {
    email: data.get('email'),
    password: data.get('password'),
  };

  const response = await fetch(`http://localhost:8080/${mode}`, {
    method: 'POST',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(authData),
  });

  if (response.status === 422 || response.status === 401) {
    return response;
  }

  if (!response.ok) {
    throw json({ message: 'Could not authenticate user' }, { status: 500 });
  }

  const resData = await response.json();
  const token = resData.token;

  localStorage.setItem('token', token);

  // Store expiration date on local storage
  const expirationDate = new Date();
  expirationDate.setHours(expirationData.getHours() + 1);
  localStorage.setItem('expiration', expirationDate.toISOString());

  return redirect('/');
}
```

Add the following to `components/util/auth.js`:

```js
import { redirect } from 'react-router-dom';

// Create the `getTokenDuration` method
export function getTokenDuration() {
  const storedExpirationDate = localStorage.getItem('expiration');
  const expirationDate = new Date(storedExpirationDate);
  const now = new Date();
  const duration = expirationDate.getTime() - now.getTime();
  return duration;
}

export function getAuthToken() {
  const token = localStorage.getItem('token');

  // Get token duration and return 'EXPIRED'
  // if token expired
  const tokenDuration = getTokenDuration();
  if (tokenDuration < 0) {
    return 'EXPIRED';
  }

  return token;
}

export function tokenLoader() {
  return getAuthToken();
}

export function checkAuthLoader() {
  const token = getAuthToken();

  if (!token) {
    return redirect('/auth');
  }

  return null;
}
```

We can now check for the case when the `getAuthToken` function returns
"EXPIRED", and log the user out in that case. Refactor `Root.js` as follows:

```js
import { redirect } from 'react-router-dom';

export function getTokenDuration() {
  const storedExpirationDate = localStorage.getItem('expiration');
  const expirationDate = new Date(storedExpirationDate);
  const now = new Date();
  const duration = expirationDate.getTime() - now.getTime();
  return duration;
}

export function getAuthToken() {
  const token = localStorage.getItem('token');

  if (!token) {
    return null;
  }

  const tokenDuration = getTokenDuration();

  if (tokenDuration < 0) {
    return 'EXPIRED';
  }

  return token;
}

export function tokenLoader() {
  return getAuthToken();
}

export function checkAuthLoader() {
  const token = getAuthToken();

  if (!token) {
    return redirect('/auth');
  }

  return null;
}
```

Finally, refactor `Logout.js` to remove the `expiration` property from local
storage at the same time as it removes `token`:

```js
import { redirect } from 'react-router-dom';

export function action() {
  localStorage.removeItem('token');
  localStorage.removeItem('expiration');
  return redirect('/');
}
```
