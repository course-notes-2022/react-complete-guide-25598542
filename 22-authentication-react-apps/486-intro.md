# Intro: Adding Authentication to React Apps

In this module we'll dive into **authentication** in React apps. We'll learn:

- How authentication works in React Apps

- Implementing user authentication

- Adding authentication persistence and auto-logout

Along the way, we'll learn a few new routing concepts as well!
