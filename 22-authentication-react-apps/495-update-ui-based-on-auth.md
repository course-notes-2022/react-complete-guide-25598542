# Update the UI Based on Auth Status

Let's now update the UI based on the existence of the token. We want to make the
token easily available on all our routes, and the exists/non-exists info is
updated immediately so that our UI reflects the status.

We can't use our custom `getToken` helper function in `MainNavigation`, because
this function is called when the component function is re-executed but **does
not trigger a re-execution** if the token is deleted. We need a way to react to
the change in the token's status!

We _could_ use the Context API to manage the token state across the application.
This would be a perfectly valid approach. But let's look at a way to leverage
`react-router-dom`.

We can go to our root application route, and add a loader that extracts the
token from local storage. We then make the token available through the loader
data in all other routes. react router will automatically re-evaluate the loader
data if we log out; re-fetch the token, determine the token doesn't exist, and
update all the components below the root route.

Update `components/util/auth.js` as follows:

```js
export function getAuthToken() {
  const token = localStorage.getItem('token');
  return token;
}

export function tokenLoader() {
  return getAuthToken();
}
```

Update `App.js` to add our `tokenLoader` as the `loader` property on our
**root** route:

```js
import { tokenLoader } from './components/util/auth';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    loader: tokenLoader, // Add tokenLoader
    id: 'root',
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader,
          },
          {
            path: ':eventId',
            id: 'event-detail',
            loader: eventDetailLoader,
            children: [
              {
                index: true,
                element: <EventDetailPage />,
                action: deleteEventAction,
              },
              {
                path: 'edit',
                element: <EditEventPage />,
                action: manipulateEventAction,
              },
            ],
          },
          {
            path: 'new',
            element: <NewEventPage />,
            action: manipulateEventAction,
          },
        ],
      },
      {
        path: 'newsletter',
        element: <NewsletterPage />,
        action: newsletterAction,
      },
      {
        path: 'auth',
        element: <AuthenticationPage />,
        action: signupAction,
      },
      {
        path: 'logout',
        action: logoutAction,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

Adding `tokenLoader` at the _root_ level ensures that this function will be
called **whenever the user navigates anywhere in the application**, **or submits
a form**.

We can now use the token data anywhere in our application. Assign an `id`
attribute of `root` to the root route, to give us a handle for fetching the
token data. Refactor `MainNavigation.js`:

```js
import {
  Form,
  NavLink,
  useRouteLoaderData, // Allows us to grab the token data
} from 'react-router-dom';

import classes from './MainNavigation.module.css';
import NewsletterSignup from './NewsletterSignup';

function MainNavigation() {
  const token = useRouteLoaderData('root'); // Get the loader data from 'root' route

  return (
    <header className={classes.header}>
      <nav>
        <ul className={classes.list}>
          <li>
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
              end
            >
              Home
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/events"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Events
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/newsletter"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Newsletter
            </NavLink>
          </li>
          {
            // Show authentication link if user is NOT logged in
            !token && (
              <li>
                <NavLink
                  to="/auth"
                  className={({ isActive }) =>
                    isActive ? classes.active : undefined
                  }
                >
                  Authentication
                </NavLink>
              </li>
            )
          }
          {
            // Show logout link if user IS logged in
            token && (
              <li>
                <Form action="/logout" method="post">
                  <button>Logout</button>
                </Form>
              </li>
            )
          }
        </ul>
      </nav>
      <NewsletterSignup />
    </header>
  );
}

export default MainNavigation;
```

Save changes and refresh. Note that upon initial navigation we see the
"Authentication" link in the main navigation. Login with a valid user, and note
that the link turns into a "Logout" link.

Update `EventsNavigation.js` as follows:

```js
import { NavLink, useRouteLoaderData } from 'react-router-dom';

import classes from './EventsNavigation.module.css';

function EventsNavigation() {
  const token = useRouteLoaderData('root');

  return (
    <header className={classes.header}>
      <nav>
        <ul className={classes.list}>
          <li>
            <NavLink
              to="/events"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
              end
            >
              All Events
            </NavLink>
          </li>
          {
            // Display link to create a new event
            // only if user is logged in
            token && (
              <li>
                <NavLink
                  to="/events/new"
                  className={({ isActive }) =>
                    isActive ? classes.active : undefined
                  }
                >
                  New Event
                </NavLink>
              </li>
            )
          }
        </ul>
      </nav>
    </header>
  );
}

export default EventsNavigation;
```

Update the `EventItem.js` file as follows:

```js
import { Link, useRouteLoaderData, useSubmit } from 'react-router-dom';

import classes from './EventItem.module.css';

function EventItem({ event }) {
  const token = useRouteLoaderData('root');
  const submit = useSubmit();

  function startDeleteHandler() {
    const proceed = window.confirm('Are you sure?');

    if (proceed) {
      submit(null, { method: 'delete' });
    }
  }

  return (
    <article className={classes.event}>
      <img src={event.image} alt={event.title} />
      <h1>{event.title}</h1>
      <time>{event.date}</time>
      <p>{event.description}</p>
      {
        // display edit/delete controls only to
        // authenticated users
        token && (
          <menu className={classes.actions}>
            <Link to="edit">Edit</Link>
            <button onClick={startDeleteHandler}>Delete</button>
          </menu>
        )
      }
    </article>
  );
}

export default EventItem;
```

Note that we are now conditionally showing the controls based on the
availability of the `token`.
