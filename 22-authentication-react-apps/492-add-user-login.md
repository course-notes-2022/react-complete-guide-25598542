# Adding User Login

Our login functionality already works, because the `action` we created for the
`AuthenticationPage` already sends the data we need to log in.

As a next step, we can finally focus on the **token** we are getting from the
backend, and on passing it to the backend along with outgoing requests.

For now, make a small adjustment to the `ErrorPage` component to ensure it
displays appropriately. Remove the `MainNavigation` from the `import` and the
component function's returned JSX:

```js
import { useRouteError } from 'react-router-dom';

import PageContent from '../components/PageContent';

function ErrorPage() {
  const error = useRouteError();

  let title = 'An error occurred!';
  let message = 'Something went wrong!';

  if (error.status === 500) {
    message = error.data.message;
  }

  if (error.status === 404) {
    title = 'Not found!';
    message = 'Could not find resource or page.';
  }

  return (
    <>
      <PageContent title={title}>
        <p>{message}</p>
      </PageContent>
    </>
  );
}

export default ErrorPage;
```
