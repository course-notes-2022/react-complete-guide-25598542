# Adding Automatic Logout

Let's look at one more piece of authentication. Right now, our token never
**expires**. The backend is already creating a tokent that expires in 1 hour.
Therefore, we also want to log the user out and **clear the token from local
storage** after expiry.

We have various options. One option is to use the `useEffect` hook in the
`RootLayout` to start a timer when it loads:

```js
import { Outlet, useLoaderData } from 'react-router-dom';
import { useEffect } from 'react';
import MainNavigation from '../components/MainNavigation';

function RootLayout() {
  // const navigation = useNavigation();
  const token = useLoaderData();
  const submit = useSubmit(); // useSubmit allows us to programmatically submit a form

  useEffect(() => {
    if (!token) {
      return;
    }
    setTimeout(() => {
      submit(null, { action: '/logout', method: 'post' });
    }, 60 * 60 * 1000);
  }, [token, submit]);
  return (
    <>
      <MainNavigation />
      <main>
        {/* {navigation.state === 'loading' && <p>Loading...</p>} */}
        <Outlet />
      </main>
    </>
  );
}

export default RootLayout;
```

With this, we start a timer that expires after 1 hour, at which time we will
send a request to the `/logout` route, which in turn invokes the following
`action` that removes the `token` attribute from local storage and then
redirects to `auth`:

```js
export function action() {
  localStorage.removeItem('token');
  return redirect('/');
}
```

Our solution is not yet complete; there is one flaw that we'll address in the
next lesson.
