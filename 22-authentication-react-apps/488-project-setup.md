# Project Setup and Route Setup

Let's set up our module project. It's based on the same project we used in the
"routing" module, with a few new components. Install the dependencies in both
the `backend` and `frontend` directories, and start each in its own terminal
window.

We can now start writing code. Let's start by ensuring that we have a way of
going to the auth page, perhaps by adding a `/auth` route:

`App.js`:

```js
// Imports omitted...
import AuthenticationPage from './pages/Authentication';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader,
          },
          {
            path: ':eventId',
            id: 'event-detail',
            loader: eventDetailLoader,
            children: [
              {
                index: true,
                element: <EventDetailPage />,
                action: deleteEventAction,
              },
              {
                path: 'edit',
                element: <EditEventPage />,
                action: manipulateEventAction,
              },
            ],
          },
          {
            path: 'new',
            element: <NewEventPage />,
            action: manipulateEventAction,
          },
        ],
      },
      {
        path: 'newsletter',
        element: <NewsletterPage />,
        action: newsletterAction,
      },
      // Add new `auth` route
      {
        path: 'auth',
        element: <AuthenticationPage />,
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

Save changes and refresh. You should be able to see the following when visiting
the `/auth` route:

![auth route added](./screenshots/auth-route-added.png)
