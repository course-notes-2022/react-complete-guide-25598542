# Avoiding Component Function Executions with `memo()`

Right now, our app has a problem. Refresh the browser, and enter a figure into
the input next to "Set Counter". Observe the logged output in the console:

![whole tree re-rendered](./screenshots/whole-tree-rerendered.png)

Note that the **entire component tree** is being re-rendered _every time we
enter a value into the input field_. This is because the input and its change
handling logic currently live in the `App` component:

```jsx
import { useState } from 'react';

import Counter from './components/Counter/Counter.jsx';
import Header from './components/Header.jsx';
import { log } from './log.js';

function App() {
  log('<App /> rendered');

  const [enteredNumber, setEnteredNumber] = useState(0);
  const [chosenCount, setChosenCount] = useState(0);

  function handleChange(event) {
    setEnteredNumber(+event.target.value);
  }

  function handleSetClick() {
    setChosenCount(enteredNumber);
    setEnteredNumber(0);
  }

  return (
    <>
      <Header />
      <main>
        <section id="configure-counter">
          <h2>Set Counter</h2>
          <input type="number" onChange={handleChange} value={enteredNumber} />
          <button onClick={handleSetClick}>Set</button>
        </section>
        <Counter initialCount={chosenCount} />
      </main>
    </>
  );
}

export default App;
```

The `handleChange` function updates a value on the `App` component state, which
triggers a re-execution of the `App` component function **and all its
children**, as we learned earlier. This is sub-optimal!

## Fixing the Problem

We have two main options for fixing the issue. The first one is the `memo`
function.

### `memo()`

React gives us a function that we can wrap around our component function that
prevents un-necessary component function executions: the `memo` function. Let's
use it in the `Counter` component:

```jsx
import { memo, useState } from 'react';

import IconButton from '../UI/IconButton.jsx';
import MinusIcon from '../UI/Icons/MinusIcon.jsx';
import PlusIcon from '../UI/Icons/PlusIcon.jsx';
import CounterOutput from './CounterOutput.jsx';
import { log } from '../../log.js';

function isPrime(number) {
  log('Calculating if is prime number', 2, 'other');
  if (number <= 1) {
    return false;
  }

  const limit = Math.sqrt(number);

  for (let i = 2; i <= limit; i++) {
    if (number % i === 0) {
      return false;
    }
  }

  return true;
}

// Wrap the component function in `memo()`
const Counter = memo(function Counter({ initialCount }) {
  log('<Counter /> rendered', 1);
  const initialCountIsPrime = isPrime(initialCount);

  const [counter, setCounter] = useState(initialCount);

  function handleDecrement() {
    setCounter((prevCounter) => prevCounter - 1);
  }

  function handleIncrement() {
    setCounter((prevCounter) => prevCounter + 1);
  }

  return (
    <section className="counter">
      <p className="counter-info">
        The initial counter value was <strong>{initialCount}</strong>. It{' '}
        <strong>is {initialCountIsPrime ? 'a' : 'not a'}</strong> prime number.
      </p>
      <p>
        <IconButton icon={MinusIcon} onClick={handleDecrement}>
          Decrement
        </IconButton>
        <CounterOutput value={counter} />
        <IconButton icon={PlusIcon} onClick={handleIncrement}>
          Increment
        </IconButton>
      </p>
    </section>
  );
});

export default Counter;
```

This observes the `props` of the component function, and **compares the old and
new prop values**. If the prop values are **exactly the same**, `memo`
**prevents the re-execution of the component function** by the _parent_
component function. `memo()` will not prevent re-execution if the **internal
component state changes**.

Save the changes and refresh the browser. Enter a character in the input field,
and notice that only the `App` and `Header` components are re-rendered this
time:

![memo in action](./screenshots/memo-in-action.png)

## Don't Overuse `memo()`!

With this, you might be tempted to wrap **all your components** with `memo()`.
Avoid doing this! To use `memo()` responsibly, observe the following rules:

- Use it **as high up in the component tree as possible**: Blocking a component
  execution there will also block _all child component executions_

- Checking `props` with `memo()` **costs performance**: Wrapping it around
  **all** your components will add a lot of unnecessary checks

- Don't use `memo()` on components where **props will change frequently**:
  `memo()` would just perform meaningless checks which cost performance
