# React Builds a Component Tree/How React Works Behind the Scenes

Download the demo project for this module and install the dependencies with
`npm install`. We'll use this project to explore the topics we discussed in the
last lecture. We'll start with how React updates the DOM.

## How Does React Update the DOM?

Rendering a component to the screen in React means simply that React executes
the **component function**. React executes the `render` function below to render
the `App` component in the `#root` element:

```jsx
import ReactDOM from 'react-dom/client';

import App from './App.jsx';
import './index.css';

ReactDOM.createRoot(document.getElementById('root')).render(<App />);
```

Doing so executes the `App` component function, which means all the code in
`App` gets executed step-by-step:

```jsx
import { useState } from 'react';

import Counter from './components/Counter/Counter.jsx';
import Header from './components/Header.jsx';
import { log } from './log.js';

function App() {
  log('<App /> rendered');

  const [enteredNumber, setEnteredNumber] = useState(0);
  const [chosenCount, setChosenCount] = useState(0);

  function handleChange(event) {
    setEnteredNumber(+event.target.value);
  }

  function handleSetClick() {
    setChosenCount(enteredNumber);
    setEnteredNumber(0);
  }

  return (
    <>
      <Header />
      <main>
        <section id="configure-counter">
          <h2>Set Counter</h2>
          <input type="number" onChange={handleChange} value={enteredNumber} />
          <button onClick={handleSetClick}>Set</button>
        </section>
        <Counter initialCount={chosenCount} />
      </main>
    </>
  );
}

export default App;
```

In the case of our `App` component, React will:

1. Initialize the constants created with `useState`
2. DEFINE (not execute) the handler functions `handleChange` and
   `handleSetClick`
3. Translates the JSX defined by the `return` statement into JavaScript that
   then renders the specified elements on the screen

Inside the JSX, we see several HTML elements and other components, both built-in
and custom. When **custom** components are encountered, React executes **those
component functions**, from top to bottom. It also passes any **props** to the
component function that the function receives from its parent.

### React Builds A Component Tree

React creates a **tree** of components as it executes these component functions.
For our app, the tree looks like this:

![react builds component tree](./screenshots/builds-component-tree.png)
