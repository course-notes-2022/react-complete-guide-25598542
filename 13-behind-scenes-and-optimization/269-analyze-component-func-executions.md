# Analyzing Component Function Executions via React's DevTools Profiler

Understanding that React builds a component tree, and how it executes component
functions, is essential. We can use the React **Dev Tools** extension's
**Profiler** for understanding which components are being updated and
re-rendered under which circumstances.

Open the browser's developer tools and click the "Profiler" tab. Click the
"Start profiling" button in the top-left corner. Begin interacting with the
page: increment/decrement the counter. Click the "Stop profiling" button, and
notice the output:

![profiler in action](./screenshots/profiler.png)

## What Are we Seeing?

Notice that we have two different "views" available to us in the Profiler:
**Flamegraph** and **Ranked**.

**Flamegraph** shows us the component **relationships**; ancestor components are
**above** descendent components. If we hover over a component, we can see
whether or not the component was **re-rendered** as part of each ordinal step.
For example, if we examine step 2 of 11:

![step 2](./screenshots/profiler-step-2.png)

We can see that the `App` component did _not_ re-render after clicking the
"Decrement" button. This is **expected**, because in our `App`, the
decrement/increment function lives in the `Counter` component, and _does not
change any `App` state_.

In React, re-renders **propagate _down_, not _up_**! A change in the `App`
component state will trigger a re-execution of `App` and all its child component
functions, but a change in the child component state will **not** re-execute
`App`!

We can see this in the flamegraph. All the **child components** of `Counter`
were re-executed. We can easily see this in the "Ranked" view, which shows us
**only the re-rendered components**, and which component was responsible for the
re-render on top.

### Recording Why A Component Rendered

We can also output an explanation of **why a component render occurred** by
clicking
`Profiler > View Settings > Profiler > Record Why a Component Rendered`. If we
click "Start Profiling" and record a session again, we get the following output:

![why render occurred](./screenshots/why-render-occurred.png)

**Understanding how and why our components render is important for helping us
understand how to _optimize_ our applications!**
