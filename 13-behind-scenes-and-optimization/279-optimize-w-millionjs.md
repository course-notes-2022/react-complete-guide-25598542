# Optimizing React with `Million.js`

Let's look at a **package** we can use to optimize performance of our React
applications: `Million.js`.

[`Million.js`](https://million.dev/) is a free package that automatically
optimizes React apps and claims to be up to 70% faster than React. You can dive
deep into all the functionality in the documentation, or install it in
`automatic` mode as follows:

1. Install `million` with `npm install million`

2. Add the `million` compiler to your build tool:

![add million compiler](./screenshots/install-million-compiler.png)

`vite.config.js`:

```js
import million from 'million/compiler';
import { defineConfig } from 'vite';
import react from '@vitejs/plugin-react';

// https://vitejs.dev/config/
export default defineConfig({
  plugins: [million.vite({ auto: true }), react()],
});
```

That's all it takes to install in `automatic` mode!

## Ignoring Troublesome Components

Right now, if we run this code our application will break. Million has trouble
with our icon components. We can tell Million to **ignore** those components
with the `// million-ignore` statement:

```jsx
import { log } from '../../../log.js';

// million-ignore
export default function ArrowRightIcon(props) {
  log('<ArrowRightIcon /> rendered', 3);

  return (
    <svg
      xmlns="http://www.w3.org/2000/svg"
      fill="none"
      viewBox="0 0 24 24"
      strokeWidth={1.5}
      stroke="currentColor"
      {...props}
    >
      <path
        strokeLinecap="round"
        strokeLinejoin="round"
        d="M4.5 12h15m0 0l-6.75-6.75M19.5 12l-6.75 6.75"
      />
    </svg>
  );
}
```

Save the changes. Note that our app works as before. We probably won't notice
many improvements in this toy app, but in a larger app we could experience
significant performance gains. `million` replaces React's virtual DOM with a
more efficient mechanism that is faster than the existing algorithm built into
React. Dive into the docs to learn more!
