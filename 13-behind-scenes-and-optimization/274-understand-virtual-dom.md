# React Uses a Virtual DOM: Time to Explore It!

Let's now look at what happens if a component function is executed, and how the
JSX code is rendered on the screen.

## The Virtual DOM

React uses the concept of a **virtual DOM** to determine **which parts of the
real DOM** to update when a component function re-executes. It creates and
compares virtual DOM snapshots to figure out which parts of the rendered UI need
to be updated.

## How Does the Virtual DOM Work?

1. Upon intial load (or full refresh), the React app creates the **component
   tree** and renders it inside the root element of `index.html`:

![step 1](./screenshots/step-1-create-component-tree.png)

2. React creates a virtual DOM **snapshot** of the target HTML code:

![step 2](./screenshots/step-2-v-dom-snapshot.png)

We're not yet reaching out to the _real_ DOM, just creating a _snapshot_ of what
that DOM should look like.

3. React **compares the virtual DOM snapshot** to the **actual DOM**, and
   _updates what's different_ in the actual DOM.

React performs the **same process** after a user interaction (a button click,
for example).

> Just because a component function _executes_ doesn't necessarily result in a
> change in the DOM! DOM updates are **expensive**, thus React only makes
> **changes** if there's a diff in the **snapshots**!
