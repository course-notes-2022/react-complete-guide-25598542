# More Reasons for Why Keys Matter

Adding a key has another benefit: React can easily compare the **previous DOM
snapshot** to the **current DOM snapshot** and update **only the parts that have
changed**, as is its default behavior.
