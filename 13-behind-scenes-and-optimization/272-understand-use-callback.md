# Understanding the `useCallback()` Hook

Currently if we click the "increment" or "decrement" buttons, we still have a
lot of unnecessary re-renders happening:

![increment-decrement rerenders](./screenshots/incr-decr-rerenders.png)

It makes sense to re-execute the `CounterOuput` component function, as its
`value` prop has changed in order to display the current count. But the
`IconButton` _has_ no internal state, and the `props` haven't changed, so
re-executing the functions doesn't make a lot of sense...

Or _have_ the `props` changed?

Observe the `IconButton` component:

```jsx
import { memo } from 'react';
import { log } from '../../log.js';

const IconButton = memo(function IconButton({ children, icon, ...props }) {
  log('<IconButton /> rendered', 2);

  const Icon = icon;
  return (
    <button {...props} className="button">
      <Icon className="button-icon" />
      <span className="button-text">{children}</span>
    </button>
  );
});

export default IconButton;
```

Note that it receives two **named** props (`children` and `icon`), and
`...props` as a spread operator.

`children` is a string in this case, and doesn't change from the first render.
`icon` is a pointer to another component, and also doesn't change. What about
`...props`?

Observe `Counter`. Note that we're passing two **functions** as additional props
to the `IconButton` components:

```jsx
import { memo, useState } from 'react';

import IconButton from '../UI/IconButton.jsx';
import MinusIcon from '../UI/Icons/MinusIcon.jsx';
import PlusIcon from '../UI/Icons/PlusIcon.jsx';
import CounterOutput from './CounterOutput.jsx';
import { log } from '../../log.js';

function isPrime(number) {
  log('Calculating if is prime number', 2, 'other');
  if (number <= 1) {
    return false;
  }

  const limit = Math.sqrt(number);

  for (let i = 2; i <= limit; i++) {
    if (number % i === 0) {
      return false;
    }
  }

  return true;
}

export default function Counter({ initialCount }) {
  log('<Counter /> rendered', 1);
  const initialCountIsPrime = isPrime(initialCount);

  const [counter, setCounter] = useState(initialCount);

  function handleDecrement() {
    setCounter((prevCounter) => prevCounter - 1);
  }

  function handleIncrement() {
    setCounter((prevCounter) => prevCounter + 1);
  }

  return (
    <section className="counter">
      <p className="counter-info">
        The initial counter value was <strong>{initialCount}</strong>. It{' '}
        <strong>is {initialCountIsPrime ? 'a' : 'not a'}</strong> prime number.
      </p>
      <p>
        <IconButton icon={MinusIcon} onClick={handleDecrement}>
          Decrement
        </IconButton>
        <CounterOutput value={counter} />
        <IconButton icon={PlusIcon} onClick={handleIncrement}>
          Increment
        </IconButton>
      </p>
    </section>
  );
}
```

These functions are **nested functions**, i.e. they are created **inside the
component function**. This means that they are **re-created in memory as new
Objects** (recall that functions are Objects in JS) _every time the component
re-renders_. Every time the `Counter` component function re-executes, a **new
function object is created** and **passed to the button as `props`**. This
change in button props causes the **button also** to rerender!

## `useCallback()`

We can avoid this unnecessary re-execution by using the `useCallback()` hook.
`useCallback()` **prevents the re-creation** of functions. It is used:

- When passing a **function** as `props`
- In conjunction with `useMemo` to avoid **unnecessary re-executions**
- (When a **function** is a **dependency** of `useEffect`)<sup>\*</sup>

`useCallback()` takes two parameters:

- The **function definition** of the function itself
- a **dependency array** of all props, state, or context values used **inside
  the function** (if any of these values _change_, a **new function would need
  to be created**)

`useCallback` returns the defined function as an **immutable function** that
will not be re-created unnecessarily!

Make the following changes to `Counter.jsx`:

```tsx
import { useState, memo, useCallback, useMemo, useEffect } from 'react';

import IconButton from '../UI/IconButton.jsx';
import MinusIcon from '../UI/Icons/MinusIcon.jsx';
import PlusIcon from '../UI/Icons/PlusIcon.jsx';
import CounterOutput from './CounterOutput.jsx';
import { log } from '../../log.js';
import CounterHistory from './CounterHistory.jsx';

function isPrime(number) {
  log('Calculating if is prime number', 2, 'other');

  if (number <= 1) {
    return false;
  }

  const limit = Math.sqrt(number);

  for (let i = 2; i <= limit; i++) {
    if (number % i === 0) {
      return false;
    }
  }

  return true;
}

const Counter = memo(function Counter({ initialCount }) {
  log('<Counter /> rendered', 1);

  const initialCountIsPrime = useMemo(
    () => isPrime(initialCount),
    [initialCount]
  );

  const [counterChanges, setCounterChanges] = useState([
    { value: initialCount, id: Math.random() * 1000 },
  ]);

  const currentCounter = counterChanges.reduce(
    (prevCounter, counterChange) => prevCounter + counterChange.value,
    0
  );

  // Create new `handleDecrement` constant
  // that wraps the `handleDecrement` function
  // in `useCallback()`
  const handleDecrement = useCallback(function handleDecrement() {
    // setCounter((prevCounter) => prevCounter - 1);
    setCounterChanges((prevCounterChanges) => [
      { value: -1, id: Math.random() * 1000 },
      ...prevCounterChanges,
    ]);
  }, []);

  // Create new `handleIncrement` constant
  // that wraps the `handleIncrement` function
  // in `useCallback()`
  const handleIncrement = useCallback(function handleIncrement() {
    setCounterChanges((prevCounterChanges) => [
      { value: 1, id: Math.random() * 1000 },
      ...prevCounterChanges,
    ]);
  }, []);

  return (
    <section className="counter">
      <p className="counter-info">
        The initial counter value was <strong>{initialCount}</strong>. It{' '}
        <strong>is {initialCountIsPrime ? 'a' : 'not a'}</strong> prime number.
      </p>
      <p>
        <IconButton icon={MinusIcon} onClick={handleDecrement}>
          Decrement
        </IconButton>
        <CounterOutput value={currentCounter} />
        <IconButton icon={PlusIcon} onClick={handleIncrement}>
          Increment
        </IconButton>
      </p>
      <CounterHistory history={counterChanges} />
    </section>
  );
});

export default Counter;
```

Save all changes and refresh. Click the increment/decrement buttons and note
that this buttons and icons **are not re-executed**.
