# Module Introduction

In this module, we'll look behind the scenes of React to fully understand how it
works, including:

- How React **updates the DOM**
- Avoiding **unnecessary updates**
- A closer look at **keys**
- State scheduling and batching
