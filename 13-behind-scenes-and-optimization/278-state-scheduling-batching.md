# State Scheduling and Batching

We're almost done with this section, but let's look at one more state-related
concept in React: how state updates are **scheduled** and **executed**.

When a state-updating function is called (i.e. the **second** argument returned
by the `useState` hook), the update will be **scheduled** by React. It will _not
be executed immediately_, thus if we `console.log` the state immediately after
updating it we get the **same state**. Instead, the state-updating function
triggers a re-execution of the component function, and the new state will be
available in the **new component** returned by the function.

Because state updates in React are **scheduled**, it is a best practice to
update state values that depend on the **old state** by passing a **function**
to the state-updating function, as we've seen before:

```jsx
const handleDecrement = useCallback(function handleDecrement() {
  setCounterChanges(
    /*
    New array object depends on the 
    value of the previous array
    */
    (prevCounterChanges) => [
      {
        value: -1,
        id: Math.random() * 1000,
      },
      ...prevCounterChanges,
    ]
  );
}, []);
```

When using this approach, React guarantees that you will get the latest state
snapshot (`prevCounterChanges`, in this case). If multiple state updates should
be scheduled, they will be updated in the order in which they appear:

```jsx
function App() {
  log('<App /> rendered');

  const [chosenCount, setChosenCount] = useState(0);
  const handleSetCount = (count) => {
    setChosenCount(count);
    /*
    The code below will update the OLD value of `chosenCount`, NOT the current value that we
    set in the previous line!
    */
    setChosenCount(chosenCount + 1);
  };

  // Omitted...
}
```

Instead, if we pass a **function**:

```jsx
function App() {
  log('<App /> rendered');

  const [chosenCount, setChosenCount] = useState(0);
  const handleSetCount = (count) => {
    setChosenCount(count);
    /*
    THIS code works as expected!
    */
    setChosenCount((prevChosenCount) => prevChosenCount + 1);
  };

  // Omitted...
}
```

We get our new count, plus one. The updates are guaranteed to execute in the
order specified, and the latest update gets the latest state.

Note that if you have **multiple state updates** in a single function as we have
above, you will _not_ end up with multiple component function executions. React
ensures that this doesn't happen by performing **state batching**, which means
that multiple state updates in the same function are **batched together**,
leading to just **one** state update and one component function execution.
