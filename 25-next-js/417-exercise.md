# Exercise

- Create 3 new routes:
  - `/meals`
  - `/meals/share`
  - `/community`

Each route can render a simple React component that outputs a title. Feel free
to add links to allow users to navigate.

- Create a new **dynamic route**:
  - `/meals/<some-slug>`
