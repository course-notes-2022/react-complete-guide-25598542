# Adding Another Route via the Filesystem

NextJS has the concept of server components, which are converted to HTML on the
server side and sent to the browser. The file name matters, as it is the
`page.js` file that Next looks for to convert. But how can we add a **second
page** to the application?

In the `app` directory, we can add new **routes** by adding new **folders**. For
example, if we want to support a `/about` route, we need to:

- Add an `about` folder in the `app` directory, and
- Add a `page.js` file in the `about` folder if we want to render a page:

```js
export default function AboutPage() {
  return (
    <main>
      <h1>About Us</h1>
    </main>
  );
}
```
