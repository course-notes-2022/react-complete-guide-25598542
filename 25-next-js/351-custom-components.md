# Adding Custom Components & CSS Modules

Let's create the MeetupDetailsPage component in `[meetupId]/index.js`:

```javascript
import MeetupDetails from '../../components/meetups/MeetupDetails';

const MeetupDetailsPage = () => {
  return (
    <MeetupDetails
      title="Fake Title"
      description="This is a fake description"
      image=""
      alt="A fake meetup"
      address="1234 Main Street"
    />
  );
};

export default MeetupDetailsPage;
```

Let's also create a `MeetupDetails` presentation component in
`/components/meetups`:

```javascript
import classes from './MeetupDetails.module.css';

const MeetupDetails = (props) => {
  return (
    <section className={classes.detail}>
      <img src={props.image} alt={props.title} />
      <h1>{props.title}</h1>
      <address>{props.address}</address>
      <p>{props.description}</p>
    </section>
  );
};

export default MeetupDetails;
```

Note that we're also adding scoped styles via CSS modules in `MeetupDetails`.
