import Link from 'next/link';

export default function MealsPage() {
  return (
    <main>
      <Link href="/">Home</Link>
      <Link href="/meals/share">Share</Link>
    </main>
  );
}
