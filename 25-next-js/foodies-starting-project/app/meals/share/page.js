import Link from 'next/link';
export default function SharePage() {
  return (
    <div>
      <h1>Share Page</h1>
      <Link href="/meals">Back to Meals</Link>
    </div>
  );
}
