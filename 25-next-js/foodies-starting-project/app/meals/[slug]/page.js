import Link from 'next/link';

export default function SlugPage({ params }) {
  return (
    <main>
      <h1>Slug Page</h1>
      <p>{params.slug}</p>
      <Link href="/meals">Back to Meals</Link>
      <Link href="/">Back to Home</Link>
    </main>
  );
}
