# Key Feature 2: Simplified Routing with File-based Routing

**Routing** means giving the user the **illusion** of having multiple pages in a
Single-Page App. We change what's visible onscreen **without** making additional
requests for HTML to the server.

Often we accomplish routing in React with a separate package, such as
`react-router`. NextJS has support for routing **build in**. What's more, you
can define pages and routes with **files and folders**, instead of **code**.
Simpler!
