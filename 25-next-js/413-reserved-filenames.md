# Reserved File Names, Custom Components and How to Organize a NextJS Project

The `globals.css` file is imported into the `layoutjs` file, and makes the
styles contained available to every loaded page. The `icon.png` file is another
special file name that Next uses as a `favicon`.

## Defining Custom Components

We are not limited to the reserved components that Next defines for us. We can
create our own components, just as we do in traditional React applications.
Create a new file, `header.js`, anywhere in the project:

```js
export default function Header() {
  return (
    <>
      <img src="/logo.png" alt="A server surrounded by magic sparkles." />
      <h1>Welcome to this NextJS Course!</h1>
    </>
  );
}
```

We can now import our Header component into the Home component, and use it as we
normally would in a React app:

```js
import Link from 'next/link';
import Header from './header';
export default function Home() {
  return (
    <main>
      <Header />
      <p>🔥 Let&apos;s get started! 🔥</p>
      <p>
        <Link href="/about">About Us</Link>
      </p>
    </main>
  );
}
```

We could store this component anywhere we like: in a new `components` directory,
for example.
[See the NextJS docs](https://nextjs.org/docs/app/building-your-application/routing/colocation)
for a discussion of different strategies.

Finally, note that we can refer to the **root directory of the project**
(_outside_ of the `app` directory) inside our component files with the `@`
symbol:

```js
import Link from 'next/link';
import Header from '@/components/header';
export default function Home() {
  return (
    <main>
      <Header />
      <p>🔥 Let&apos;s get started! 🔥</p>
      <p>
        <Link href="/about">About Us</Link>
      </p>
    </main>
  );
}
```

This alias is configured in the `jsconfig` file.
