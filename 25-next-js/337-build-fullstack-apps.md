# Key Feature 3: Build Fullstack Apps

NextJS makes it **easy to add backend code** to our React apps. We can easily
add a nodeJS backend to store data, fetch data, authenticate, etc. without
**building a standalone REST API project**.
