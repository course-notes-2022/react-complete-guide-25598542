# Optimizing Images with the NextJS Image Component

Right now, we're using the HTML `img` element for displaying our header image.
NextJS gives us a special `Image` component that allows us to output images in
an **optimized way**, for example by **automatically lazy-loading images** and
giving us **responsive images**.

The `Image` component has many configuration options, but in its simplest form
all we need to do is replace the `img` element with Next's `Image` component:

```js
import Link from 'next/link';
import logoImg from '@/assets/logo.png';
import classes from './main-header.module.css';
import Image from 'next/image';

export default function MainHeader() {
  return (
    <header className={classes.header}>
      <Link className={classes.logo} href="/">
        <Image src={logoImg} alt="a plate with food on it" />
        NextLevel Food
      </Link>
      <nav className={classes.nav}>
        <ul>
          <li>
            <Link href="/meals">Browse Meals</Link>
          </li>
          <li>
            <Link href="/community">Foodies Community</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}
```

Note that inspecting the element rendered in the developer tools, we get a bunch
of attributes that Next adds on our behalf for image optimization, including
**automatically generating the optimal file format** for the browser used to
render the content:

![next js image](./screenshots/next-image.png)

Observe in the console that Next gives us a warning message to add the
`priority` property to this image, as it was detected as the Largest Contentful
Paint (LCP).
[See the NextJS documentation for more details.](https://nextjs.org/docs/pages/api-reference/components/image#priority)

```js
import Link from 'next/link';
import logoImg from '@/assets/logo.png';
import classes from './main-header.module.css';
import Image from 'next/image';

export default function MainHeader() {
  return (
    <header className={classes.header}>
      <Link className={classes.logo} href="/">
        <Image src={logoImg} priority alt="a plate with food on it" />
        NextLevel Food
      </Link>
      <nav className={classes.nav}>
        <ul>
          <li>
            <Link href="/meals">Browse Meals</Link>
          </li>
          <li>
            <Link href="/community">Foodies Community</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}
```
