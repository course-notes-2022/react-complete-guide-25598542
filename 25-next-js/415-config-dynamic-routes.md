# Configuring Dynamic Routes and Using Route Parameters

Imagine we want to have a `blog` route in our application, with different blog
post folders nested inside it. We'd need to add a new folder whenever a new post
is added to our storage, which is not scalable. We need a **dynamic route**: one
that we define _once_, and reuse for each post.

We can define dynamic routes in Next using the special `[]` syntax:

![dynamic route file structure](./screenshots/dynamic-route-file-structure.png)

Create the above structure in your application. Add the following in `page.js`:

```js
import Link from 'next/link';

export default function BlogPage() {
  return (
    <main>
      <h1>The Blog</h1>
      <p>
        <Link href="/blog/post-1">Post 1</Link>
      </p>
      <p>
        <Link href="/blog/post-2">Post 2</Link>
      </p>
    </main>
  );
}
```

Add the following in `blog/[slug]/page.js`:

```js
export default function BlogPostPage({ params }) {
  return (
    <main>
      <h1>Blog Post</h1>
      <p>{params.slug}</p>
    </main>
  );
}
```

Note that `params` is a prop that Next passes **automatically** to a dynamic
route's `page` components. `params` is an Object where each `key` is the name of
the param in the `[]` ("slug" in this case), and the `value` is the value of the
route parameter in the URL. This allows us to use the slug value to fetch the
associated blog post from a DB, for example.
