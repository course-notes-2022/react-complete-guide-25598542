# Key Feature 1: Built-in SSR

Perhaps the **most important** feature NextJS offers is **Server-Side
Rendering** (SSR). SSR means that the pages served are **pre-rendered** on the
**server** and sent to the browser as HTML.

By default, React apps are rendered on the **client side**. A (bundled) script
loads the **entire app** into a minimal HTML file at **runtime**. This could be
an issue for several reasons:

- User may experience "no-content" during page load
- **SEO**: if your app is public facing, with a lot of content that should be
  indexible by search engines, a typical React app is a problem.

React actually **has** functionality to implement SSR, but it can be tricky to
get working right. NextJS provides **built-in SSR** without any additional setup
required .
