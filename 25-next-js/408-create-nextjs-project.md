# Creating a NextJS Project

To create a new NextJS project, run the following command in a terminal window:

> `npx create-next-app@latest <app-name>`

[See the documentation at https://nextjs.org](https://nextjs.org/) for more
information.

Run `npm install` in the project root to install the dependencies. Run
`npm run dev` to start the development server.
