# Working with Pages and Layouts

The `layout.js` file defines the "shell" that surrounds one or more NextJS
**pages**:

![pages and layouts](./screenshots/pages-and-layouts.png)

Every Next project needs at least one root `layout` file in the `app` directory.
In this file, we export a React component that renders an `<html>` and a
`<body>` tag:

```js
import './globals.css';

export const metadata = {
  title: 'NextJS Course App',
  description: 'Your first NextJS app!',
};

export default function RootLayout({ children }) {
  return (
    <html lang="en">
      <body>{children}</body>
    </html>
  );
}
```

The `head` tag is not explicitly rendered here, but the `metadata` export
creates values that will be used in `head` metadata for all pages controlled by
this layout.

`children` is the content of the **currently-active page**.
