# The `_app.js` File and Layout Wrapper

Let's take a look at `_app.js` and some of the layout components we're using in
our app.

The `Layout` component **wraps content** that should be framed by the layout. It
uses `props.children` to display whatever is between the `Layout` tags. We can
wrap the `Layout` component arount our `MeetupList` component to create a nice
layout by importing it into `index.js` and using it:

```javascript
const HomePage = () => {
  return (
    <Layout>
      <MeetupList meetups={DUMMY_MEETUPS} />
    </Layout>
  );
};

export default HomePage;
```

Note that we _could_ follow this approach and wrap **each individual** component
in the `Layout`, but this is **sub-optimal**. Instead, we can use `_app.js`.

`_app.js` contains the `MyApp` component, which is the **root** component of the
application. It receives props, and uses object destructuring to pull out two
props (that are passed automatically by NextJS):

- `Component`: holds the actual **page content** to be rendered
- `pageProps`: specific props our pages might be getting

We can simply wrap `Component` in our `Layout` component, and apply our layout
to **all** our pages:

```javascript
import Layout from '../components/layout/Layout';
import '../styles/globals.css';

function MyApp({ Component, pageProps }) {
  return (
    <Layout>
      <Component {...pageProps} />
    </Layout>
  );
}

export default MyApp;
```
