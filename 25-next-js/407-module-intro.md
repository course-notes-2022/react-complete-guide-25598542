# Module Intro

In this section, we'll build a special new, **full stack** application combined
into **one project**. This section is about working with **NextJS**, a React
framework that allows you to build full-stack apps with React. We'll learn:

- What is NextJS and Why would you use it?
- Routing, pages, and server components
- Fetching and sending data
- Styling, image upload and managing page metadata
