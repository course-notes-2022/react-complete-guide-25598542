# Data Fetching for Static Pages

![ssr vs. ssg](./screenshots/ssg-ssr.png)

If we want our **pre-rendered** HTML pages to be **fully hydrated with data**
prior to being loaded in the browser and before React takes over, we have two
options:

1. **Static Site Generation**:

2. **Server-Side Rendering**:

## Static Site Generation

This is the approach you'll typically use. With SSG, the page content is build
when the **application is built** for production, _not_ when a page is requested
from the server. If the application data **changes** after deployment, you must
**redeploy** the app.

For most applications, this is acceptable if page data does not change
frequently. In cases where it **does** change, we have options.

### Implement SSG

Let's look at how to implement SSG. NextJS already prepares and generates your
pages statically during the build process. If we need to add **data fetching**
to a page component, you can do so by exporting a special function called
`getStaticProps()` from your **page** components.

NextJS will look for a function called `getStaticProps` during the build process
and **execute it** during the pre-rendering process before it calls the
component function. `getStaticProps` job is to **prepare props containing the
data** the page needs. NextJS will wait for `getStaticProps` to resolve
**before** executing the component function.

```javascript
import MeetupList from '../components/meetups/MeetupList';

const DUMMY_MEETUPS = [
  //...
];
const HomePage = (props) => {
  return <MeetupList meetups={props.meetups} />;
};

export async function getStaticProps() {
  // Execute ANY server-side code; will NEVER end up or execute on the client side, ONLY during the build process
  return {
    props: {
      meetups: DUMMY_MEETUPS,
    },
  };
}

export default HomePage;
```

Our page is now **pre-rendered** with the **full data in the HTML**. Great for
SEO!
