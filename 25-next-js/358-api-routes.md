# Intro to API Routes

Let's now finish this app by adding a **real backend** with a **real database**.
NextJS makes it easy to build an API alongside our frontend app.

## API Routes

API Routes allow you to build API endpoints. To add them, create a folder inside
the `pages` folder called `api`. Next will pick up any JS files in `api` and use
them to respond to HTTP requests.

Create files inside `api` that are run in response to requests:

`/api/new-meetup.js`:

```javascript
// api/new-meetup

const handler = (req, res) => {
  if (req.method === 'POST') {
    const data = req.body;

    const { title, image, address, description } = data;
  }
};

export default handler;
```
