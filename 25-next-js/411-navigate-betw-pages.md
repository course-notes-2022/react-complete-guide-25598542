# Navigating Between Pages

Let's link to the `about` page from the `home` page. Add the following to
`home`:

```js
import Link from 'next/link';

export default function Home() {
  return (
    <main>
      <img src="/logo.png" alt="A server surrounded by magic sparkles." />
      <h1>Welcome to this NextJS Course!</h1>
      <p>🔥 Let&apos;s get started! 🔥</p>
      {/* <p><a href="/about">About Us</a></p> */}
      <p>
        <Link href="/about">About Us</Link>
      </p>
    </main>
  );
}
```

NextJS allows us to have a **server side** and **client side** working together:

![navigating between pages](./screenshots/navigating-betw-pages.png)

We get the **server-side rendered** HTML page if we are navigating to it for the
first time, but if we are navigating **within the client**, we get the same
**client-side, SPA behavior** we're used to in React SPAs.
