# Revisiting the Concept of Layouts

Recall that **layouts** in Nextjs act as **wrappers** around **pages**. We can
also have **nested layouts**. Note that any **nested layouts** are nested in the
**root layout**, which is _always active_.

As with all React components, layout components accept a `children` prop. In
layout components, `children` represents the content of the **nested** layout.

Create the following content in `/app/meals/layout.js`:

```js
export default function MealsLayout({ children }) {
  return (
    <>
      <p>Meals Layout</p>
      {children}
    </>
  );
}
```

Save changes and refresh the browser. Visit the root route, and note that the
`MealsLayout` component is _not_ shown. Visit the `meals` route, and note that
the `MealsLayout` is visible.

We won't be using the `MealsLayout`, so feel free to delete the component file.
