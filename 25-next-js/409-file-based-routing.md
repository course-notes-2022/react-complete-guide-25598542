# Understanding File-based Routing & React Server Components

NextJS uses **files and folders** to define routes. Only files and folders
inside the `app` folder are considered! `app` is the most important folder in a
modern Next project:

![file system based routing](./screenshots/file-system-based-routing.png)

`page.js` and `layout.js` are **reserved file names**. `page.js` tells Next that
it should render a page. It contains a **component** that is a **Server
component**.

Server components are regular React components that require a special
**"environment"**. Next provides such an environment. Server components are
rendered **only on the server, _never on the client_**. The following is a
server component:

```js
export default function Home() {
  return (
    <main>
      <img src="/logo.png" alt="A server surrounded by magic sparkles." />
      <h1>Welcome to this NextJS Course!</h1>
      <p>🔥 Let&apos;s get started! 🔥</p>
    </main>
  );
}
```

If we were to add a `console.log` statement to the above component, we would see
the log statement **only on the server**. The returned JSX code is converted to
HTML and sent **over the network** to the client to be **rendered by the
browser**.
