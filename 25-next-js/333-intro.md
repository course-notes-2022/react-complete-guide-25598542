# Intro

We'll now learn to build large-scale, production-ready apps with **NextJS**.

## Module Content

- What is NextJS?
- Why might we use it?
- File based routing and Page pre-rendering
- Data Fetching and Adding an API
