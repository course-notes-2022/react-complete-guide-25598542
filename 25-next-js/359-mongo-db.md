# Working with MongoDB

In this lesson we'll connect to a MongoDB Atlas instance running in the cloud.
Add the following to `/api/new-meetup.js`:

```javascript
// api/new-meetup
import { MongoClient } from 'mongodb';

const handler = async (req, res) => {
  if (req.method === 'POST') {
    const data = req.body;

    const client = await MongoClient.connect(
      `mongodb+srv://username:password@meetupsappcluster.oy0ecri.mongodb.net/meetups?retryWrites=true&w=majority`
    );

    const db = client.db('meetups');

    const result = await db.collection('meetups').insertOne(data);
    console.log(result);
    client.close();
    res.status(201).json({ message: 'Meetup inserted' });
  }
};

export default handler;
```
