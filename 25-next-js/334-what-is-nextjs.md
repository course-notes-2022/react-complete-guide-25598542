# What is NextJS?

**NextJS** is "the React framework for production". Another way to think about
it is a "fullstack framework for React".

NextJS is a a framework that **builds on** React, and makes building large-scale
React apps easier.

![what is nextjs](./screenshots/what-is-nextjs.png)
