# Adding Nested Paths and Pages/Routes

There is an **alternative** to organizing pages using a **named file**. We could
also create a named **sub-directory**, and creating an **`index.js`** file in
the directory.

Note that if we want to create a **nested path**, such as
`news/something-important`, we **must** do so in a folder:

```
|-pages
|--/news
|----/index.js # Loads when /news/ is visited
|----/something-important.js # Loads when /news/something-important is visited

```
