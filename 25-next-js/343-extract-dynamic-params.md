# Extracting Dynamic Parameter Values

NextJS gives us a special hook for extracting the dynamic param values:

```javascript
import { useRouter } from 'next/router';

function DetailPage() {
  const router = useRouter();

  const newsId = router.query.newsId;

  // Send request to API to fetch news item with newsId param

  return <h1>The Detail Page</h1>;
}

export default DetailPage;
```
