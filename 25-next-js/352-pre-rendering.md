# How Pre-Rendering Works and What Problems we Face

Let's imagine that we have a backend API that is serving our meetup data. We
fetch the data upon loading our landing page via the `useEffect()` hook:

```javascript
const HomePage = () => {
  const [meetupsList, setMeetupsList] = useState([]);
  useEffect(() => {
    // Fetch meetups from API layer
    setMeetupsList(DUMMY_MEETUPS);
  }, []);
  return <MeetupList meetups={meetupsList} />;
};

export default HomePage;
```

It's important to note that `useEffect()` runs for the first time **after** the
initial component render. This means that our users may notice a "pending" state
while the meetups are being fetched, since `meetupsList` is an **empty array**
the first time the component renders. This _may_ or _may not_ be the experience
we want!

Additionally, this is a problem for **SEO**. Our pre-rendered NextJS page will
return the data in it's **initial, empty** state, since NextJS does not
re-render the page after the **second** use effect call updates the page state.

Thankfully, NextJS has solutions for this problem!
