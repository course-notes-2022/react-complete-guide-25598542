# Preparing Paths. with `getStaticPaths()`

`getStaticPaths()` is a function that you must export in a **page** component if
the page is **dynamic**, AND you're using `getStaticProps()`.

Recall that NextJS must **pre-generate** pages for **all possible `id`
parameters** of our meetups. We need to pregenerate for all IDs.

```javascript
import MeetupDetails from '../../components/meetups/MeetupDetails';

const MeetupDetailsPage = () => {
  return (
    <MeetupDetails
      title="Fake Title"
      description="This is a fake description"
      image=""
      alt="A fake meetup"
      address="1234 Main Street"
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [
      {
        params: {
          meetupId: 'm1',
        },
      },
      {
        params: {
          meetupId: 'm2',
        },
      },
    ],
    fallback: false, // `false` indicates that the paths param includes ALL possible meetupId values. If the user attempts to enter a non-supported id, a 404 is returned
  };
}

export async function getStaticProps(context) {
  const meetupId = context.params.meetupId;

  return {
    props: {
      meetupData: {
        image: '',
        id: meetupId,
        title: 'First Meetup',
        description: 'A first meetup',
        address: '1234 Pine Street',
      },
    },
  };
}
export default MeetupDetailsPage;
```
