# Adding First Pages

Let's add our first pages to our new app. For now, we can delete the `pages/api`
directory and `pages/index.js` file, and the `Home.module.css` file.

Add the following to the `pages` directory:

`index.js`:

```javascript
// our-domain/

const HomePage = () => {
  return <h1>The Home Page</h1>;
};

export default HomePage;
```

`news.js`:

```javascript
// our-domain/news

const NewsPage = () => {
  return <h1>The News Page</h1>;
};

export default NewsPage;
```
