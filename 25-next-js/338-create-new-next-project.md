# Creating a New NextJS Project and App

To create a new NextJS project, we need to do the following:

- Install `node` and `npm`.
- In a terminal, run `npx create-next-app`

Once the command finishes, you can open the project folder and view the
contents:

![next app structure](./screenshots/next-app-structure.png)
