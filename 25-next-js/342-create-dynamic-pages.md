# Creating Dynamic Pages (with Parameters)

We probably want to be able to display **dynamic** data on our news details
page, i.e. load individual news items based on links that the user clicks on the
`News` page. The content would probably be fetched from the DB and loaded into
the details page. To do so, we can use **dynamic paths**:

```
|-pages
|--/news
|----/index.js
|----/[newsId].js # Square brackets indicate a dynamic path

```
