# Working with Params for SSG Data Fetching

```javascript
import MeetupDetails from '../../components/meetups/MeetupDetails';

const MeetupDetailsPage = () => {
  return (
    <MeetupDetails
      title="Fake Title"
      description="This is a fake description"
      image=""
      alt="A fake meetup"
      address="1234 Main Street"
    />
  );
};

export async function getStaticProps(context) {
  const meetupId = context.params.meetupId;

  return {
    props: {
      meetupData: {
        image: '',
        id: meetupId,
        title: 'First Meetup',
        description: 'A first meetup',
        address: '1234 Pine Street',
      },
    },
  };
}
export default MeetupDetailsPage;
```
