# Analyzing the Created Project

We have several directories in our created project:

- `pages`: The folder defines the pages that compose our app, as well as the
  file-based routing.
- `public`: Holds public resources (images, etc.). Note that there is **no
  `index.html`** file. This is because NextJS dynamically pre-renders a page
  when the server receives a request.
- `styles`: Contains stylesheets
