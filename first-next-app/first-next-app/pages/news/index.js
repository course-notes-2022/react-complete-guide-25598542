import Link from 'next/link';
import { Fragment } from 'react';

const NewsPage = () => {
  return (
    <Fragment>
      <ul>
        <li>
          <Link href="/news/next-is-great">Next is a Great Framework</Link>
        </li>
        <li>
          <Link href="/news/something-else">Something Else</Link>
        </li>
      </ul>
    </Fragment>
  );
};

export default NewsPage;
