# Outputting Dynamic Data & Working with Expressions in JSX

Note that we typically want to use **dynamic** data with our components to make
them truly reusable.

```javascript
import './ExpenseItem.css';

export const ExpenseItem = () => {
  const expenseDate = new Date(2021, 2, 28);
  const expenseTitle = 'Car Insurance';
  const expenseAmount = 279.0;

  return (
    <div className="expense-item">
      <div className="expense-item__description">
        {expenseDate.toISOString()}
      </div>
      <h2>{expenseTitle}</h2>
      <div className="expense-item__price">${expenseAmount}</div>
    </div>
  );
};
```

We can output dynamic data in a template with the `{}` syntax. You can put **any
valid JS expression between the `{}`**.
