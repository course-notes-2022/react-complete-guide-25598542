# Intro

In this section, we'll explore all the basics to build **component-driven user
interfaces** with React. We'll learn:

- React core syntax and JSX
- How to build React components
- How to work with **data** in React apps

Along the way, we'll build an example web app to practice the concepts we'll
learn: an **expense tracker** app.
