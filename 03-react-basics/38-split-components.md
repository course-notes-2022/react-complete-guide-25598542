# Splitting Components into Multiple Components

We can and should look to **split components into smaller components** when the
component file gets too complex:

```javascript
export const ExpenseDate = (props) => {
  return (
    <div>
      <div>{props.month}</div>
      <div>{props.day}</div>
      <div>{props.year}</div>
    </div>
  );
};

import { ExpenseDate } from '../ExpenseDate/ExpenseDate';
import './ExpenseItem.css';

export const ExpenseItem = (props) => {
  const month = props.expenseDate.toLocaleString('en-US', { month: 'long' });
  const day = props.expenseDate.toLocaleString('en-US', { day: '2-digit' });
  const year = props.expenseDate.getFullYear();

  return (
    <div className="expense-item">
      <ExpenseDate month day year />
      <div className="expense-item__description">
        {props.expenseDate.toISOString()}
      </div>
      <h2>{props.expenseTitle}</h2>
      <div className="expense-item__price">${props.expenseAmount}</div>
    </div>
  );
};
```

Note that we've refactored the expense date display to a **new component file**.
We can then simply import the `export` value of the component and use it where
it's needed.
