# React Code is Written in a "Declarative" Way

## How is a Component Built?

A React component combines HTML, CSS, and JavaScript. We combine **components**
to build UIs. React uses a **declarative** approach to build components, i.e.
you **define the desired end state(s)** and React figures out **what elements
need to be created in the DOM**.
