# Building a First Custom Component

We'll build our first custom component, the `AddExpense` component, in this
lesson.

Best practice in React is to create **each component in its own directory**. The
directory should contain the component function, the CSS (if applicable), and
test spec file (if applicable).

## React Component Tree

![react component tree](./screenshots/react-component-tree.png)
