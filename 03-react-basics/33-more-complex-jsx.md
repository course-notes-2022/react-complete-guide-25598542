# Writing More Complex JSX Code

Note that you **must** have only **one** "root" element in a React component
function:

```javascript
export const ExpenseItem = () => {
  return (
    <div>
      <div>March 28th 2022</div>
      <h2>Car Insurance</h2>
      <div>$279.00</div>
    </div>
  );
};
```

You can nest multiple elements within the root element.
