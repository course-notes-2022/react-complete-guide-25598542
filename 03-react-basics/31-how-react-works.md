# How React Works

React is all about **components**, which are essentially **custom HTML
elements**. We can see this in our `App` component function:

```javascript
function App() {
  return (
    <div>
      <h2>Let's get started!</h2>
    </div>
  );
}

export default App;
```

`App` is a **function** that returns **JSX** representing the desired **end
state** of our UI.
