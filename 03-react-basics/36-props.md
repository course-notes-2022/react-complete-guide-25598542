# Passing Data via `props`

`props` allow us to pass **truly dynamic data** to our component functions, just
as we pass **parameters** to a regular JS function:

```javascript
import { ExpenseItem } from './components/ExpenseItem/ExpenseItem';

function App() {
  const expenses = [
    { title: 'Car Insurance', amount: 300.67, date: new Date(2022, 3, 19) },
    { title: 'Pet Food', amount: 9.99, date: new Date(2022, 7, 11) },
    { title: 'Child Support', amount: 750.0, date: new Date(2022, 0, 1) },
  ];

  return (
    <div>
      <h2>Let's get started!</h2>
      <ExpenseItem
        expenseDate={expenses[0].date}
        expenseTitle={expenses[0].title}
        expenseAmount={expenses[0].amount}
      ></ExpenseItem>
      <ExpenseItem
        expenseDate={expenses[1].date}
        expenseTitle={expenses[1].title}
        expenseAmount={expenses[1].amount}
      ></ExpenseItem>
      <ExpenseItem
        expenseDate={expenses[2].date}
        expenseTitle={expenses[2].title}
        expenseAmount={expenses[2].amount}
      ></ExpenseItem>
    </div>
  );
}

export default App;

import './ExpenseItem.css';

export const ExpenseItem = (props) => {
  return (
    <div className="expense-item">
      <div className="expense-item__description">
        {props.expenseDate.toISOString()}
      </div>
      <h2>{props.expenseTitle}</h2>
      <div className="expense-item__price">${props.expenseAmount}</div>
    </div>
  );
};
```
