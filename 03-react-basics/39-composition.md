# The Concept of "Composition" (`children` props)

**Composition** refers to building applications from smaller building blocks.
One specific use case of composition involves creating a component that is
simply a **shell**, or **container**, around content of an **arbitrary type**.
We would typically want to simply wrap this content in the component and output
the content between the opening and closing component tags:

```javascript
import './Card.css';

export const Card = (props) => {
  return <div className="card">{props.children}</div>;
};

import { ExpenseDate } from '../ExpenseDate/ExpenseDate';
import { Card } from '../Card/Card';
import './ExpenseItem.css';

export const ExpenseItem = (props) => {
  const month = props.expenseDate.toLocaleString('en-US', { month: 'long' });
  const day = props.expenseDate.toLocaleString('en-US', { day: '2-digit' });
  const year = props.expenseDate.getFullYear();

  return (
    <Card>
      <div className="expense-item">
        <ExpenseDate month={month} day={day} year={year} />
        <div className="expense-item__description">
          {props.expenseDate.toISOString()}
        </div>
        <h2>{props.expenseTitle}</h2>
        <div className="expense-item__price">${props.expenseAmount}</div>
      </div>
    </Card>
  );
};
```

`children` is a **special attribute on `props`** that is **always** received,
_even if you don't pass it explicitly_. The value of `children` is **always the
content between the opening and closing component tags**.
