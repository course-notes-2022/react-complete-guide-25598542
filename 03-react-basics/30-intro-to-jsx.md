# Intro to JSX

**JSX** is a special syntax **invented by the React team**. It is **HTML-like**
syntax that allows us to embed **templates** inside our **Javascript files**.
This works because of the special transformations taking place behind the scenes
in React:

```javascript
function App() {
  return (
    <div>
      <h2>Let's get started!</h2>
    </div>
  );
}

export default App;
```
