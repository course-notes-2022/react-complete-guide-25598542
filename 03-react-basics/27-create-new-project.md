# Create a New React Project

The easiest way to get started with a new React project is to use the **Create
React App (CRA)** utility. CRA scaffolds a new React app, and gives you a
**development server** that auto-updates as you make changes to your application
code, as well as **production optimizations**.

Note that you must have `nodejs` installed first in order to install CRA. CRA
uses `nodejs` for the dev server as well as production optimizations.

## Create a New CRA App

1. `npx create-react-app <app-name>`
2. `cd react-complete-guide`
3. `npm run start`

You should see your app running at `http://localhost:3000`. We are now ready to
edit our application code in VS Code (or your chosen text editor/IDE).
