# Basic CSS Styling

We can create styles for our components simply by creating a normal CSS file
alongside our component and **importing** it:

```javascript
import './ExpenseItem.css';

export const ExpenseItem = () => {
  return (
    <div className="expense-item">
      <div className="expense-item__description">March 28th 2022</div>
      <h2>Car Insurance</h2>
      <div className="expense-item__price">$279.00</div>
    </div>
  );
};
```

Note that you **must** use the `className` attribute, _not_ `class`, which is a
reserved keyword in Javascript.
