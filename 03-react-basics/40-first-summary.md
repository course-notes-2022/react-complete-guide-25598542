# A First Summary

This section is about **components**, and building UIs with components in React.
We split our code across **multiple files** and composed our UI by combining
components. In the end, our components are parsed into HTML elements in the
browser DOM.

In the end, our application is still static. The data is **hard coded**, and we
don't have any **interaction**. We'll learn how to do that in the next chapter.
