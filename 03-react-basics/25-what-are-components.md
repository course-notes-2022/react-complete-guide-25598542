# What are Components?

React is a JavaScript library for building **user interfaces**. It makes
building compolex interactive and reactive user interfaces **simpler**. To do
this, React embraces a concept called "components".

## Why Components?

Because all user interfaces in React are made up of **components**. Components
allow us to encapsulate views, styles, and business logic into **reusable
building blocks** in our user interface.

In React, you build these individual components and tell React how to combine
them into a UI:

![why components?](./screenshots/why-components.png)
