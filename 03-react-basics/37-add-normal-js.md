# Adding "Normal" Javascript Logic to React Components

The `ExpenseItem` component is now reusable, with dynamic values being passed
via `props`.

Recall that we are not restricted to outputting props only in our JSX. We can
put any valid JS expression between `{}`. We can also create **variables**
within our component function, just as we can in any JS function:

```javascript
import './ExpenseItem.css';

export const ExpenseItem = (props) => {
  const month = props.expenseDate.toLocaleString('en-US', { month: 'long' });
  const day = props.expenseDate.toLocaleString('en-US', { day: '2-digit' });
  const year = props.expenseDate.getFullYear();

  return (
    <div className="expense-item">
      <div>
        <div>{month}</div>
        <div>{day}</div>
        <div>{year}</div>
      </div>
      <div className="expense-item__description">
        {props.expenseDate.toISOString()}
      </div>
      <h2>{props.expenseTitle}</h2>
      <div className="expense-item__price">${props.expenseAmount}</div>
    </div>
  );
};
```
