# A Closer Look at JSX

In our `package.json`, we have two dependencies: `react` and `react-dom`. In the
past, you needed to **explicitly import `react`** in **all React component
files**, or any files in which you used JSX. We needed it in the past because
under the hood, JSX is transformed to **methods called on the React object**:

```javascript
return React.createElement(
  'div', // element to create
  {}, // object with element attribute config
  React.createElement('h2', {}, 'Lets get started'),
  React.createElement(
    Expenses, // POINTER to custom component
    {
      items: expenses,
    }
  )
);
```

is the same as:

```javascript
return (
  <div>
    <h2>Let's get started</h2>
    <Expenses items={expenses} />
  </div>
);
```

however, the latter is **considerably easier to read** and **less complex to
write**.
