# Analyzing a Standard React Project

The `index.js` file is the **first file that is executed** when the application
is loaded in the browser:

```javascript
import ReactDOM from 'react-dom/client';

import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(<App />);
```

Note that certain transformations are taking place "behind the scenes", such as
code that allows for the `import`ation of CSS, parsing of `JSX` code, etc. into
code that can be run by browsers.

Essentially, `index.js` is placing our entire React application into an element
in the `index.html` file with the `id` attribute equal to `"root"`. In our case,
we're loading the `App` component into the `#root` element.
