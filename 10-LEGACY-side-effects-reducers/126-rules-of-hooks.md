# Learning the Rules of Hooks

There are 2 main rules when working with hooks:

1. Only call React hooks in **React Functions**: component functions or custom
   hooks.

2. Only call Reach hooks at the **top level**, not in nested functions or
   conditional (or other block) statements.

3. (BONUS rule) ALWAYS add everything you refer to inside of `useEffect` (i.e.
   data from the surrounding component [data or props]) as a dependency!
