# React Context Limitations

- Great for app/component-wide state, but not a replacement for component
  configuration

- NOT optimized for high-frequency changes (per second, multiple times per
  second)

- Should NOT be used to replace all component communications and props
