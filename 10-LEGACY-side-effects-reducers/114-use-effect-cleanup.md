# `useEffect` Cleanup Function

`useEffect` **returns a value** that is a **function**. This function will be
run as a **cleanup process before the `useEffect` hook runs for the next time**,
as well as before the component is dismounted from the DOM. It does **not** run
on initial render of the component.
