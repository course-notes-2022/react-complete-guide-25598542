# Intro to React Context API

We often run into a problem in larger React apps where we're passing a lot of
data through **multiple components** in the component tree, in order to get them
down/up to where they are needed. This is a sub-optimal situation, because we
must pass data through components that **don't need it**, and we introduce
**more opportunities for human error**.

![data forwarding hell](./screenshots/data-forwarding-hell.png)

Is there a _better way_? YES, with **React Context**.
