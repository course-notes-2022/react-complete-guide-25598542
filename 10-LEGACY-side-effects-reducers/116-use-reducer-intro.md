# `useReducer()` for State Management

Sometimes you have **more complex state**, for example if there are multiple
states, multiple ways of changing state, or **updating states with dependencies
on other states**.

`useState` then often becomes hard or error-prone to use; it's easy to write
bad, inefficient, or buggy code in such scenarios.

`useReducer` can be used as a **replacement** for `useState` if you need **more
powerful state management**. This does not mean you should **always** use
`useReducer`, because it is more complex and requires more setup.

## We're Being Bad...

We're already **violating** one important principle of using state in React in
our `Login` component, namely that **updates to state** should not rely on
**previous state** (unless we pass a **function** to the second value returned
by `useState`):

`Login.js`:

```javascript
// ...
const validateEmailHandler = () => {
  setEmailIsValid(enteredEmail.includes('@')); // BAD!

  setEmailIsValid((prevState) => prevState.includes('@')); // This won't work, because `prevState` gives us the previous value of `emailIsValid`, NOT `enteredEmail`!
};
```

`useReducer` is a good option for this scenario, because it allows us to
**combine our multiple states into ONE state**.
