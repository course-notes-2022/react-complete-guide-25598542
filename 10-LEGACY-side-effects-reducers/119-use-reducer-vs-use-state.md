# `useReducer()` vs. `useState()` for State Management

When should you use `useState()` vs. `useReducer()`?

Generally, you'll know when you need `useReducer()`; when using `useState()`
becomes cumbersome or you're getting a lot of bugs/unintendted behaviors.

| `useState`                                                            | `useReducer`                                                  |
| --------------------------------------------------------------------- | ------------------------------------------------------------- |
| The main state management tool                                        | Great if you need "more power"                                |
| Great for independent pieces of state/data                            | Should be considered if you have related pieces of state/data |
| Great if state updates are easy and limited to a few kinds of updates | Can be helpful if you have more complex state updates         |
