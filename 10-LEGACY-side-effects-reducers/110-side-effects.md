# What Are Side Effects and `useEffect`

React's **main job** is to render UI and react to user input:

- Evaluate and render JSX
- manage state and props
- react to user event & input
- re-evelaute component upon state and prop changes

A **side effect** (or "effect") is **anything other than this**:

- storing data in browser storage
- sending HTTP requests
- set and manage timers

These tasks must happen outside of the normal component evaluation and render
cycle, especially since they might block/delay rendering (e.g. HTTP requests).

![side effects](./screenshots/side-effects.png)

## `useEffect`

`useEffect` is a React hook. It is called as follows:

```javascript
useEffect(callback, [dependencies]);

// callback: a function that should be executed AFTER every component eveluation IF the specified dependencies changed

// dependencies: Dependencies of this effect; the callback function runs only if the dependencies changed
```

![useEffect](./screenshots/useEffect.png)
