# `useEffect()` Summary

- `useEffect()` with NO dependencies runs when the component **first mounts**,
  and with **every state update**/every time the component **re-renders**.

- `useEffect()` with an **empty** dependency array runs **once**.

-`useEffect()` with a **dependency** runs **once** when the component function
runs for the **first time**, and thereafter **only when the dependency
changes**.

- You can return a **cleanup function** from `useState()`. The cleanup function
  runs **before** the `useEffect` in which it is defined, but not before the
  **first time** the `useEffect` runs, OR when the component is removed from the
  DOM.
