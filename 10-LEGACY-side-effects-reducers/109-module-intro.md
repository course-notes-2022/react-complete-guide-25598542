# Intro

In this chapter we'll now look at 3 very important concepts you must know as a
React developer: **Effects**, **Reducers**, and **Context**. We'll learn:

- How to work with (Side) Effects
- Manage Complex State with Reducers
- Manage App-wide or Component-wide State with Context
