# Consume the Context with `useContext()`

To **consume** the content, we can simply use the `useContext` hook inside any
component that needs the context:

```javascript
import React, { useContext } from 'react'; // import useContext

import AuthContext from '../../store/auth-context'; // import the context you wish to consume
import classes from './Navigation.module.css';

const Navigation = (props) => {
  const ctx = useContext(AuthContext); // Call the useContext hook and pass the desired context

  return (
    <nav className={classes.nav}>
      <ul>
        {ctx.isLoggedIn && ( // Use it!
          <li>
            <a href="/">Users</a>
          </li>
        )}
        {ctx.isLoggedIn && (
          <li>
            <a href="/">Admin</a>
          </li>
        )}
        {ctx.isLoggedIn && (
          <li>
            <button onClick={props.onLogout}>Logout</button>
          </li>
        )}
      </ul>
    </nav>
  );
};
```
