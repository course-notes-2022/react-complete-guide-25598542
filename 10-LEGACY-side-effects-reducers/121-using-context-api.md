# Using the React Context API

- Create a new folder in the `src` directory, `store`. Note that you can have
  **multiple** contexts for **multiple** global states in a large app, or just a
  single context.

- Add the following to the `auth-context.js` file:

```javascript
import React from 'react';

const AuthContext = React.createContext(
  // Create context object
  {
    isLoggedIn: false,
  }
);

export default AuthContext;
```

- Use context in your app:
  1. **Provide** it: tell React that all components wrapped by the context
     object should have access to it
  2. **Consume** it: "hook in" to the context object

If we know that the **entire** app should access the context, we should wrap the
`<App/>` component in a **Context Provider**:

`App.js`:

```javascript
function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);
  // ...

  <React.Fragment>
    <AuthContext.Provider
      value={{
        isLoggedIn: isLoggedIn,
      }}
    >
      <MainHeader isAuthenticated={isLoggedIn} onLogout={logoutHandler} />
      <main>
        {!isLoggedIn && <Login onLogin={loginHandler} />}
        {!isLoggedIn && <Home onLogout={logoutHandler} />}
      </main>
    </AuthContext.Provider>
  </React.Fragment>;
}
```

The `Provider` component accepts a `value` prop to be passed to consuming
components that are children of `Provider`. All descendents will **re-render**
whenever the Provider's `value` prop changes.
