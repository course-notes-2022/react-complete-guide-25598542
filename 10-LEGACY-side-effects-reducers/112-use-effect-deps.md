# `useEffect` and Dependencies

The second argument to `useEffect` is a list of **dependencies**. The
`useEffect` hook will run **whenever any of the dependencies changes**. Passing
an _empty array_ to `useEffect` ensures the function runs **only once** on
initial render.

Passing **no argument** for dependencies makes the function run on **every
render of the component**. Be careful not to include logic that **updates
state** in the function, otherwise an _infinite re-render_ will occur!
