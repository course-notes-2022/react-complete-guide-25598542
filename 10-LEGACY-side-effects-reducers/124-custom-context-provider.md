# Building & Using a Custom Context Provider Component

It's a good idea to add your functions to the default context:

`auth-context.js`:

```javascript
import React from 'react';

const AuthContext = React.createContext({
  isLoggedIn: true,
  logoutHandler: () => {},
});

export default AuthContext;
```
