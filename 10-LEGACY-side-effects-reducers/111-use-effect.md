# Using the `useEffect` Hook

`useEffect` is useful for **controlling when side effects will run** and
avoiding unwanted/infinite re-renders when updating state:

`App.js`:

```javascript
import React, { useState, useEffect } from 'react';

import Login from './components/Login/Login';
import Home from './components/Home/Home';
import MainHeader from './components/MainHeader/MainHeader';

function App() {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  useEffect(() => {
    const storedLogin = localStorage.getItem('isLoggedIn');

    if (storedLogin === '1') {
      setIsLoggedIn(true);
    }
  }, []); // useEffect will run this function AFTER every component evaluation and ONLY if the dependencies change

  // ...
}

export default App;
```
