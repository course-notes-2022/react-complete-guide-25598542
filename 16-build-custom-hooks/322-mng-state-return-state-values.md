# Custom Hook: Managing State and Returning State Values

Let's now continue working to make our new `useFetch` hook functional. To do so,
we also need to manage **state**. Therefore, we'll import the `useState`
built-in hook. Let's start by updating `useFetch` as follows:

```jsx
import { useEffect, useState } from 'react';

export default function useFetch(fetchFn) {
  const [isFetching, setIsFetching] = useState();
  const [error, setError] = useState();
  const [fetchedData, setFetchedData] = useState();

  useEffect(() => {
    async function fetchData() {
      setIsFetching(true);
      try {
        const data = await fetchFn();
        setFetchedData(data);
      } catch (error) {
        setError({ message: error.message || 'Failed to fetch data.' });
      }

      setIsFetching(false);
    }

    // Return our state values from custom hook
    return {
      isFetching,
      fetchedData,
      error,
    };

    fetchData();
  }, [fetchFn]);
}
```

Note the following:

1. We are using the `useState` hook at the top of the function to manage 3
   pieces of state:

   - `isFetching`: Has the data fetch completed?
   - `error`: Did the fetch encoutner an error?
   - `fetchedData`: The data fetched from the server

2. We are passing a `fetchFn` parameter to `useFetch`. `fetchFn` is the
   **function we want to invoke** to fetch the data. Passing the function as a
   parameter makes `useFetch` **reusable across different components**.

3. We are passing `fetchFn` to the second parameter of `useEffect` as a
   **dependency**. `fetchFn` is some external data not defined INSIDE
   `useEffect`, and which could (theoretically) change, and if if _does_ change
   we want to **re-execute** `useEffect` with the new value of `fetchFn`. Note
   that we're not _executing_ `fetchFn`, just passing its value.

4. We are **returning the state values in which we're interested** from the
   custom hook.

## Using our New `useFetch` Custom Hook

Let's refactor `App.jsx` to use our new custom hook:

```jsx
import { useRef, useState, useCallback } from 'react';

import Places from './components/Places.jsx';
import Modal from './components/Modal.jsx';
import DeleteConfirmation from './components/DeleteConfirmation.jsx';
import logoImg from './assets/logo.png';
import AvailablePlaces from './components/AvailablePlaces.jsx';
import { fetchUserPlaces, updateUserPlaces } from './http.js';
import useFetch from './hooks/useFetch.js'; // Import `useFetch` custom hook
import Error from './components/Error.jsx';

function App() {
  const selectedPlace = useRef();

  const [errorUpdatingPlaces, setErrorUpdatingPlaces] = useState();

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const { isFetching, fetchedData, error } = useFetch(fetchUserPlaces); // Call `useFetch` at the top of the component and destructure the returned values

  function handleStartRemovePlace(place) {
    setModalIsOpen(true);
    selectedPlace.current = place;
  }

  function handleStopRemovePlace() {
    setModalIsOpen(false);
  }

  async function handleSelectPlace(selectedPlace) {
    setUserPlaces((prevPickedPlaces) => {
      if (!prevPickedPlaces) {
        prevPickedPlaces = [];
      }
      if (prevPickedPlaces.some((place) => place.id === selectedPlace.id)) {
        return prevPickedPlaces;
      }
      return [selectedPlace, ...prevPickedPlaces];
    });

    try {
      await updateUserPlaces([selectedPlace, ...userPlaces]);
    } catch (error) {
      setUserPlaces(userPlaces);
      setErrorUpdatingPlaces({
        message: error.message || 'Failed to update places.',
      });
    }
  }

  const handleRemovePlace = useCallback(
    async function handleRemovePlace() {
      setUserPlaces((prevPickedPlaces) =>
        prevPickedPlaces.filter(
          (place) => place.id !== selectedPlace.current.id
        )
      );

      try {
        await updateUserPlaces(
          userPlaces.filter((place) => place.id !== selectedPlace.current.id)
        );
      } catch (error) {
        setUserPlaces(userPlaces);
        setErrorUpdatingPlaces({
          message: error.message || 'Failed to delete place.',
        });
      }

      setModalIsOpen(false);
    },
    [userPlaces]
  );

  function handleError() {
    setErrorUpdatingPlaces(null);
  }

  return (
    <>
      <Modal open={errorUpdatingPlaces} onClose={handleError}>
        {errorUpdatingPlaces && (
          <Error
            title="An error occurred!"
            message={errorUpdatingPlaces.message}
            onConfirm={handleError}
          />
        )}
      </Modal>

      <Modal open={modalIsOpen} onClose={handleStopRemovePlace}>
        <DeleteConfirmation
          onCancel={handleStopRemovePlace}
          onConfirm={handleRemovePlace}
        />
      </Modal>

      <header>
        <img src={logoImg} alt="Stylized globe" />
        <h1>PlacePicker</h1>
        <p>
          Create your personal collection of places you would like to visit or
          you have visited.
        </p>
      </header>
      <main>
        {error && <Error title="An error occurred!" message={error.message} />}
        {!error && (
          <Places
            title="I'd like to visit ..."
            fallbackText="Select the places you would like to visit below."
            isLoading={isFetching}
            loadingText="Fetching your places..."
            places={userPlaces}
            onSelectPlace={handleStartRemovePlace}
          />
        )}

        <AvailablePlaces onSelectPlace={handleSelectPlace} />
      </main>
    </>
  );
}

export default App;
```

Note that when we use a custom hook in a component, any state that is managed by
the custom hook **belongs to the component in which it is used**. For example,
when calling `setIsFetching(true)` inside `useEffect` in `useFetch`, the `App`
component is **executed again** once that piece of state changes, **just as it
would be if we were managing state in `App` with `useState`**.

## Wrap-up

Let's finally modify `useFetch` to accept an initial state for the data it'll
eventually return:

```jsx
import { useEffect, useState } from 'react';

export default function useFetch(fetchFn, initialData) {
  // Pass the initial data state as a second parameter to `useFetch`
  const [isFetching, setIsFetching] = useState();
  const [error, setError] = useState();
  const [fetchedData, setFetchedData] = useState(initialData); // Pass initial data as parameter to useState to set the initial data state

  useEffect(() => {
    async function fetchData() {
      setIsFetching(true);
      try {
        const data = await fetchFn();
        setFetchedData(data);
      } catch (error) {
        setError({ message: error.message || 'Failed to fetch data.' });
      }

      setIsFetching(false);
    }

    fetchData();

    return {
      isFetching,
      fetchedData,
      error,
    };
  }, [fetchFn]);
}
```

Now, in `App.jsx`, we can pass an empty array as an **argument** to the
`useFetch` invocation:

```jsx
function App() {
  const selectedPlace = useRef();

  const [errorUpdatingPlaces, setErrorUpdatingPlaces] = useState();

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const { isFetching, fetchedData, error } = useFetch(fetchUserPlaces, []); // Pass empty array to set initial state

  // omitted...
}
```
