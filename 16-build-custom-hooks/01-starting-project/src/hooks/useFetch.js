import { useEffect, useState } from 'react';

export default function useFetch(fetchFn, initialData) {
  const [isFetching, setIsFetching] = useState();
  const [error, setError] = useState();
  const [fetchedData, setFetchedData] = useState(initialData);

  useEffect(() => {
    async function fetchData() {
      setIsFetching(true);
      try {
        const data = await fetchFn();
        setFetchedData(data);
      } catch (error) {
        setError({ message: error.message || 'Failed to fetch data.' });
      }

      setIsFetching(false);
    }

    fetchData();

    // Return our state values from custom hook
    return {
      isFetching,
      fetchedData,
      error,
      setFetchedData, // expose `setFetchedData` to allow consuming components to UPDATE state
    };
  }, [fetchFn]);
}
