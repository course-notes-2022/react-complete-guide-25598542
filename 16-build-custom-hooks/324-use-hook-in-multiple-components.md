# Using a Custom Hook in Multiple Components

As we learned, the idea behind creating custom hooks is to get **leaner
components** by outsourcing state and management logic into custom hooks.

**Another reason** is to **share custom hooks across components** if the
components share fairly similar logic. For example, the `AvailablePlaces`
component has logic that is similar to what we have in `useFetch`. We can use
our `useFetch` custom hook here as well!

Refactor `AvailablePlaces.jsx` to use our custom `useFetch` hook:

```jsx
import Places from './Places.jsx';
import Error from './Error.jsx';
import { sortPlacesByDistance } from '../loc.js';
import { fetchAvailablePlaces } from '../http.js';
import useFetch from '../hooks/useFetch.js';

export default function AvailablePlaces({ onSelectPlace }) {
  //  navigator.geolocation.getCurrentPosition((position) => {
  //         const sortedPlaces = sortPlacesByDistance(
  //           places,
  //           position.coords.latitude,
  //           position.coords.longitude
  //         );
  //         setAvailablePlaces(sortedPlaces);
  //    setIsFetching(false);

  const {
    isFetching,
    error,
    fetchedData: availablePlaces,
    setFetchedData: setAvailablePlaces,
  } = useFetch(fetchAvailablePlaces, []);

  if (error) {
    return <Error title="An error occurred!" message={error.message} />;
  }

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      isLoading={isFetching}
      loadingText="Fetching place data..."
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

Note that using our custom hook has allowed us to reduce our component file size
**significantly**. We'll look at how to bring back the location sorting logic in
the next lesson.
