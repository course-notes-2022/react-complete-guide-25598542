# Creating Flexible Custom Hooks

We can now re-use our custom `useFetch` hook in the `AvailablePlaces` component
to fetch data for our places.

Refactor `AvailablePlaces` as follows:

```jsx
import Places from './Places.jsx';
import Error from './Error.jsx';
import { sortPlacesByDistance } from '../loc.js';
import { fetchAvailablePlaces } from '../http.js';
import useFetch from '../hooks/useFetch.js';

/*
Create a new function, `fetchSortedPlaces`, that
leverages `fetchAvailablePlaces` to fetch the desired data
and then sorts them according to our sort logic in `sortPlacesByDistance`.

`fetchSortedPlaces` will be the function we pass as the `fetchFn` argument to our `useFetch` hook. Note that `useFetch` is expecting `fetchFn` to be an **async** function, thus we are returning our sorted places in a resolved `Promise`.
*/
async function fetchSortedPlaces() {
  const places = await fetchAvailablePlaces();

  return new Promise((resolve) => {
    navigator.geolocation.getCurrentPosition((position) => {
      const sortedPlaces = sortPlacesByDistance(
        places,
        position.coords.latitude,
        position.coords.longitude
      );

      resolve(sortedPlaces);
    });
  });
}

export default function AvailablePlaces({ onSelectPlace }) {
  const {
    isFetching,
    error,
    fetchedData: availablePlaces,
  } = useFetch(fetchSortedPlaces, []); // Pass `fetchSortedPlaces` as the `fetchFn` argument

  if (error) {
    return <Error title="An error occurred!" message={error.message} />;
  }

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      isLoading={isFetching}
      loadingText="Fetching place data..."
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```
