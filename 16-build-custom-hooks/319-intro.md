# Intro

We've used lots of React hooks, such as `useState` and `useEffects`. In this
section, we'll learn how to build **custom hooks** in React. We'll learn:

- The rules of hooks
- Why use custom hooks?
- Using custom hooks
