# Create a Custom Hook

Let's create a new folder in our project, `hooks` (we can name it anything). Add
a new file inside, `useFetch.js`.

Create a new function inside the file called `useFetch`. **Important: The
function name _must_ start with the word "use"!**

## Why Start Functions with `use`?

Functions in React that start with "use" are treated as hooks. React enforces
certain rules on those functions. For example, attempting to call a function
that starts with "use" inside of an `if` statement or nested inside some other
code, React will complain that hooks must be called in a React component
function or a custom hook, _even though the "use" function is valid Javascript_.

Copy the following code snippet from `App.jsx` into our new `useFetch` function:

```jsx
useEffect(() => {
  async function fetchPlaces() {
    setIsFetching(true);
    try {
      const places = await fetchUserPlaces();
      setUserPlaces(places);
    } catch (error) {
      setError({ message: error.message || 'Failed to fetch user places.' });
    }

    setIsFetching(false);
  }

  fetchPlaces();
}, []);
```

such that `useFetch` now looks like the following:

```jsx
import { useEffect } from 'react';

function useFetch() {
  useEffect(() => {
    async function fetchPlaces() {
      setIsFetching(true);
      try {
        const places = await fetchUserPlaces();
        setUserPlaces(places);
      } catch (error) {
        setError({ message: error.message || 'Failed to fetch user places.' });
      }

      setIsFetching(false);
    }

    fetchPlaces();
  }, []);
}
```

React is satisified, because we're calling the `useEffect` hook _inside_ another
function that begins with `use`.

In the end, we want to be able to refactor `App.jsx` (and any other component
that needs to fetch data) like so:

```jsx
// other imports omitted...
import { useFetch } from './hooks/useFetch.js';

function App() {
  const selectedPlace = useRef();

  const [userPlaces, setUserPlaces] = useState([]);
  const [isFetching, setIsFetching] = useState(false);
  const [error, setError] = useState();

  const [errorUpdatingPlaces, setErrorUpdatingPlaces] = useState();

  const [modalIsOpen, setModalIsOpen] = useState(false);

  // Delete all of this...
  //   useEffect(() => {
  //     async function fetchPlaces() {
  //       setIsFetching(true);
  //       try {
  //         const places = await fetchUserPlaces();
  //         setUserPlaces(places);
  //       } catch (error) {
  //         setError({ message: error.message || 'Failed to fetch user places.' });
  //       }

  //       setIsFetching(false);
  //     }

  //     fetchPlaces();
  //   }, []);

  // And replace with this...
  useFetch();

  function handleStartRemovePlace(place) {
    setModalIsOpen(true);
    selectedPlace.current = place;
  }

  // omitted...
}

export default App;
```
