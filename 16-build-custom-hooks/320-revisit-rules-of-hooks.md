# Revisiting the "Rules of Hooks" & Why to Use Hooks

Let's revisit the **Rules of Hooks**:

1. Call Hooks **inside of Component functions _only_**. React hooks must **not**
   be called outside of React component functions.

2. Call Hooks at the **top level _only_**. React hooks must not be called in
   nested code statements (e.g. inside of `if` statements)

![rules of hooks](./screenshots/rules-of-hooks.png)

## Except...

The first rule is a **little more flexible**, as:

> We can use hooks _inside of other hooks_!

This is what we'll focus on in this section.

## Why Custom Hooks?

We build custom hooks to **wrap and re-use code that goes into our component
functions**. For example, in our `App` component, we're fetching some data from
our backend in the `useEffect` hook:

```jsx
useEffect(() => {
  async function fetchPlaces() {
    setIsFetching(true);
    try {
      const places = await fetchUserPlaces();
      setUserPlaces(places);
    } catch (error) {
      setError({ message: error.message || 'Failed to fetch user places.' });
    }

    setIsFetching(false);
  }

  fetchPlaces();
}, []);
```

If we look at the `AvailablePlaces.jsx` file, we're doing something very
similar:

```jsx
useEffect(() => {
  async function fetchPlaces() {
    setIsFetching(true);

    try {
      const places = await fetchAvailablePlaces();

      navigator.geolocation.getCurrentPosition((position) => {
        const sortedPlaces = sortPlacesByDistance(
          places,
          position.coords.latitude,
          position.coords.longitude
        );
        setAvailablePlaces(sortedPlaces);
        setIsFetching(false);
      });
    } catch (error) {
      setError({
        message:
          error.message || 'Could not fetch places, please try again later.',
      });
      setIsFetching(false);
    }
  }

  fetchPlaces();
}, []);
```

This `useEffect` call, which fetches some data, sets some loading state, and
handles some errors, could be an example of some code that we'd want to make
into a **reusable piece of code**, just as we make **reusable components**. It
can't be made into a component, because it doesn't return JSX.

We could outsource this to a regular function, as this is the normal programming
methodology, but the code we want to use and share **uses hooks and state**,
which we cannot do outside of components. **This is why we need custom hooks:
functions that we can call from different places**, but are **guaranteed to be
used in components**!
