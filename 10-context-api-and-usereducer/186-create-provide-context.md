# Creating and Providing the Context

Let's learn how to create our context, and provide it to our components.

Create a new top-level directory, `store` (name is arbitrary). Create a new file
`store/shopping-cart-context.jsx`:

```jsx
import { createContext } from 'react';

const CartContext = createContext();
```

Import the `createContext` binding from `react`, and create a new constant
calling it. Note that we're naming it with an initial **capital letter**, as
**`createContext()` returns a React component**.

`createContext` takes a value as an argument that is the **initial value of the
context**. This value can be of any valid JS data type. Add a new intial context
to `createContext()`:

```jsx
import { createContext } from 'react';

export const CartContext = createContext({
  items: [],
});
```

We can now **provide** our context. You'll want to select a component that
**wraps** all the components that **will need to access the context** For us, a
great place is the `App` component. Import the `CartContext` function into `App`
and refactor the returned value of app as follows:

```jsx
return (
  <CartContext.Provider>
    <Header
      cart={shoppingCart}
      onUpdateCartItemQuantity={handleUpdateCartItemQuantity}
    />
    <Shop>
      <ul id="products">
        {DUMMY_PRODUCTS.map((product) => (
          <li key={product.id}>
            <Product {...product} onAddToCart={handleAddItemToCart} />
          </li>
        ))}
      </ul>
    </Shop>
  </CartContext.Provider>
);
```

Note that we are wrapping **all the components that need to access the context
in `CartContext.Provider`**. `Provider` is a property of `CartContext` that **is
also a component**.

We are now successfully providing our context. In the next lesson, we'll
**consume** it.
