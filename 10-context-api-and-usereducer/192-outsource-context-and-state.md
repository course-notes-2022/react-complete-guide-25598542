# Outsourcing Context & State Into a Separate Provider Component

We've successfully refactored our app to get rid of prop drilling in favor of
using Context. However, our App component is pretty bloated, as we're doing all
the context object creation locally. This works, but we can do it in a cleaner
way.

An alternative pattern is to refactor the logic into a **separate context
provider component**.

The idea is to simply grab all the context state management and value-setting
code from `App`, and refactor it to a **separate component**. Refactor
'shopping-cart-context.jsx' as follows:

```jsx
import { useState, createContext } from 'react';
import { DUMMY_PRODUCTS } from '../dummy-products';

export const CartContext = createContext({
  items: [],
  addItemToCart: () => {},
  updateItemQty: () => {},
});

export default function CartContextProvider({ children }) {
  const [shoppingCart, setShoppingCart] = useState({
    items: [],
  });

  function handleAddItemToCart(id) {
    setShoppingCart((prevShoppingCart) => {
      const updatedItems = [...prevShoppingCart.items];

      const existingCartItemIndex = updatedItems.findIndex(
        (cartItem) => cartItem.id === id
      );
      const existingCartItem = updatedItems[existingCartItemIndex];

      if (existingCartItem) {
        const updatedItem = {
          ...existingCartItem,
          quantity: existingCartItem.quantity + 1,
        };
        updatedItems[existingCartItemIndex] = updatedItem;
      } else {
        const product = DUMMY_PRODUCTS.find((product) => product.id === id);
        updatedItems.push({
          id: id,
          name: product.title,
          price: product.price,
          quantity: 1,
        });
      }

      return {
        items: updatedItems,
      };
    });
  }

  function handleUpdateCartItemQuantity(productId, amount) {
    setShoppingCart((prevShoppingCart) => {
      const updatedItems = [...prevShoppingCart.items];
      const updatedItemIndex = updatedItems.findIndex(
        (item) => item.id === productId
      );

      const updatedItem = {
        ...updatedItems[updatedItemIndex],
      };

      updatedItem.quantity += amount;

      if (updatedItem.quantity <= 0) {
        updatedItems.splice(updatedItemIndex, 1);
      } else {
        updatedItems[updatedItemIndex] = updatedItem;
      }

      return {
        items: updatedItems,
      };
    });
  }

  const ctxValue = {
    items: shoppingCart.items,
    addItemToCart: handleAddItemToCart,
    updateItemQty: handleUpdateCartItemQuantity,
  };

  return (
    <CartContext.Provider value={ctxValue}>{children}</CartContext.Provider>
  );
}
```

**Note** that we are creating a new **default** export `CartContextProvider`,
that is a component function **like any other React component function**. It
takes the `children` prop, and wraps any children passed to it. The `value` prop
is `ctxValue`. And our `App` component is **much cleaner** now:

```jsx
import { CartContextProvider } from './store/shopping-cart-context.jsx';
import Header from './components/Header.jsx';
import Shop from './components/Shop.jsx';
import { DUMMY_PRODUCTS } from './dummy-products.js';
import Product from './components/Product.jsx';

function App() {
  return (
    <CartContextProvider>
      <Header />
      <Shop>
        <ul id="products">
          {DUMMY_PRODUCTS.map((product) => (
            <li key={product.id}>
              <Product {...product} />
            </li>
          ))}
        </ul>
      </Shop>
    </CartContextProvider>
  );
}

export default App;
```

Refresh the browser, and note that it still works as before. **We can create
_multiple context files_ form _multiple contexts_ as needed, and keep our `App`
file nice and lean**!
