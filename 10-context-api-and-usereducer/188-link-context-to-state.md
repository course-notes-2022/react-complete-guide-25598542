# Linking the Context to State

Right now, our context value is **static**; it's always an object with an
`items` property that is an empty array. How can we link it to our state?

Refactor the return value of `App` to accept the `shoppingCart` state value as
its `value` prop:

```jsx
return (
    <CartContext.Provider value={shoppingCart}>
      <Header
        cart={shoppingCart}
        onUpdateCartItemQuantity={handleUpdateCartItemQuantity}
      />
      <Shop>
        <ul id="products">
          {DUMMY_PRODUCTS.map((product) => (
            <li key={product.id}>
              <Product {...product} onAddToCart={handleAddItemToCart} />
            </li>
          ))}
        </ul>
      </Shop>
    </CartContext.Provider>
  );
}
```

This allows the context to _read_ the state value, but we can't yet _update it_
because we're still passing the `handleAddItemToCart` function down as props.
This is simple to remedy.

Refactor `App.jsx` as follows:

```jsx
// imports omitted...

function App() {
  const [shoppingCart, setShoppingCart] = useState({
    items: [],
  });

  function handleAddItemToCart(id) {
    setShoppingCart((prevShoppingCart) => {
      const updatedItems = [...prevShoppingCart.items];

      const existingCartItemIndex = updatedItems.findIndex(
        (cartItem) => cartItem.id === id
      );
      const existingCartItem = updatedItems[existingCartItemIndex];

      if (existingCartItem) {
        const updatedItem = {
          ...existingCartItem,
          quantity: existingCartItem.quantity + 1,
        };
        updatedItems[existingCartItemIndex] = updatedItem;
      } else {
        const product = DUMMY_PRODUCTS.find((product) => product.id === id);
        updatedItems.push({
          id: id,
          name: product.title,
          price: product.price,
          quantity: 1,
        });
      }

      return {
        items: updatedItems,
      };
    });
  }

  // omitted...

  // Create a new object to represent the context value
  const ctxValue = {
    items: shoppingCart.items,
    addItemToCart: handleAddItemToCart, // expose the handler function to the context as well
  };

  // Pass `ctxValue` as the `value` property to the Provider
  return (
    <CartContext.Provider value={ctxValue}>
      <Header
        cart={shoppingCart}
        onUpdateCartItemQuantity={handleUpdateCartItemQuantity}
      />
      <Shop>
        <ul id="products">
          {DUMMY_PRODUCTS.map((product) => (
            <li key={product.id}>
              <Product {...product} onAddToCart={handleAddItemToCart} />
            </li>
          ))}
        </ul>
      </Shop>
    </CartContext.Provider>
  );
}

export default App;
```

**Note that we are now _including the function to add an item to the cart_** on
the `ctxValue` object, and **setting it to the `handleAddItemToCart` function
defined in the `App` component**.

Refactor the `Product` component to use the new state-changing function on our
context:

```jsx
import { useContext } from 'react';
import { CartContext } from '../store/shopping-cart-context';

export default function Product({ id, image, title, price, description }) {
  const { addItemToCart } = useContext(CartContext);

  return (
    <article className="product">
      <img src={image} alt={title} />
      <div className="product-content">
        <div>
          <h3>{title}</h3>
          <p className="product-price">${price}</p>
          <p>{description}</p>
        </div>
        <p className="product-actions">
          <button onClick={() => addItemToCart(id)}>Add to Cart</button>
        </p>
      </div>
    </article>
  );
}
```
