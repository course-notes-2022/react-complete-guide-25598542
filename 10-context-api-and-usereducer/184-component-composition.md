# Component Composition as a Solution

One possible solution for the prop drilling problem is to embrace **component
composition**. Note that component composition is usually **one part** of the
overall solution, but let's take a look at it here.

To explore, let's look at the `Shop` component:

```ts
import { DUMMY_PRODUCTS } from '../dummy-products.js';
import Product from './Product.jsx';

export default function Shop({ onAddItemToCart }) {
  return (
    <section id="shop">
      <h2>Elegant Clothing For Everyone</h2>

      <ul id="products">
        {DUMMY_PRODUCTS.map((product) => (
          <li key={product.id}>
            <Product {...product} onAddToCart={onAddItemToCart} />
          </li>
        ))}
      </ul>
    </section>
  );
}
```

We could say that the task of outputting the list of products could be moved
from the `Shop` component **directly to the `App` component**. This has the
advantage of not having to pass the `onAddItemToCart` prop through `Shop`, and
instead pass it directly from `App` to `Product`. We can then **keep** the
`Shop` component, and make it a simple **wrapper component** for outputting the
list of products.

Make the following updates to `Shop.ts`:

```ts
export default function Shop({ children }) {
  // Pass the special `children` prop
  return (
    <section id="shop">
      <h2>Elegant Clothing For Everyone</h2>
      {children} // Render `children` here
    </section>
  );
}
```

Update the return value of `App.ts` as follows:

```ts
return (
  <>
    <Header
      cart={shoppingCart}
      onUpdateCartItemQuantity={handleUpdateCartItemQuantity}
    />
    {
      //  Refactor `Shop` into a non-self closing component
      //  Add the products list as the `children`
      //  Pass `handleAddItemToCart` directly to the `Product` component
    }
    <Shop>
      <ul id="products">
        {DUMMY_PRODUCTS.map((product) => (
          <li key={product.id}>
            <Product {...product} onAddToCart={handleAddItemToCart} />
          </li>
        ))}
      </ul>
    </Shop>
  </>
);
```

With that, we're embracing component composition and have gotten rid of one
layer of component nesting.

Note that we typically don't want to use this solution **all the time**, as it
can lead to a bloated `App` component full of wrapper components. We'll look at
another part of the solution next.
