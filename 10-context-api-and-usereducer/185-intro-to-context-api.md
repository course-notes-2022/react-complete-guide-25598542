# Introducing the Context API

The **Context API** is a solution that can help us get rid of the **entire
prop-drilling problem**. It is a React built-in feature designed to make it easy
to share state across components.

The idea is that we create a context "value", and then wrap that value around
some or all of our components. The great thing about the context value is that
it can be easily connected to **state**, therefore we no longer have to **pass
state** through components. Components that need the state can simply **access
it directly from the Context**.
