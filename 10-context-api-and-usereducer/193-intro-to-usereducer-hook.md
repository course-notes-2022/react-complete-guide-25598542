# Intro to `useReducer`

When building more complex React apps, Context can be a crucial feature as it
helps with sharing state across multiple components.

In our app, we're managing the `shoppingCart` state of our `CartContextProvider`
component, and the functions to manage that state are complex. Note also that
we're passing **functions** to the `setShoppingCart` state-setting function, for
example, because we need access to the previous state and passing a function to
the `setX` function returned by `useState` hook is the way to do that:

```jsx
function handleAddItemToCart(id) {
  setShoppingCart((prevShoppingCart) => {
    const updatedItems = [...prevShoppingCart.items];

    const existingCartItemIndex = updatedItems.findIndex(
      (cartItem) => cartItem.id === id
    );
    const existingCartItem = updatedItems[existingCartItemIndex];

    if (existingCartItem) {
      const updatedItem = {
        ...existingCartItem,
        quantity: existingCartItem.quantity + 1,
      };
      updatedItems[existingCartItemIndex] = updatedItem;
    } else {
      const product = DUMMY_PRODUCTS.find((product) => product.id === id);
      updatedItems.push({
        id: id,
        name: product.title,
        price: product.price,
        quantity: 1,
      });
    }

    return {
      items: updatedItems,
    };
  });
}
```

This is a **common pattern**, thus React provides another state management hook:
`useReducer`.

## What is a Reducer?

A **reducer** is a function that reduces one or more **complex values** to a
**simpler one**. We're already using a **built-in** JS reducer in `Cart`:

```jsx
export default function Cart() {
  const { updateItemQty, items } = useContext(CartContext);

  // `reduce` is a built-in JS method on the Array class
  const totalPrice = items.reduce(
    (acc, item) => acc + item.price * item.quantity,
    0
  );
  const formattedTotalPrice = `$${totalPrice.toFixed(2)}`;

  // omitted...
}
```

`useReducer` is a React hook. Similarly to `useState`, it returns an **array**
with **two items**:

1. A **state variable**, and
2. A **dispatch** function, which you can use to dispatch **actions** that will
   be handled by a to-be-defined reducer function

The reducer function will be triggered by the dispatch of the action, and
**update our state**.

**Outside** the component function, define a new function called
`shoppingCartReducer`. We define it outside the component because it does **not
need to be recreated every time the component re-renders**.
`shoppingCartReducer` should take **two parameters**:

- `state`: The **guaranteed latest snapshot** of the state managed by the
  `useReducer` function
- `action`: The **action** that will trigger this reducer function

`shoppingCartReducer` should return the **updated state**. To connect it to the
`useReducer` hook, we pass a **pointer** to the reducer function as a first
argument to `useReducer`. Often, we want to pass a **second** argument to
`userReducer` that sets the **initial state value** for the reducer.

Refactor `shopping-cart-context` as follows:

```jsx
import { useState, createContext, useReducer } from 'react';
import { DUMMY_PRODUCTS } from '../dummy-products';

export const CartContext = createContext({
  items: [],
  addItemToCart: () => {},
  updateItemQty: () => {},
});

// Create reducer function; returning state unchanged temporarily
function shoppingCartReducer(state, action) {
  return state;
}

export default function CartContextProvider({ children }) {
  // Call `useReducer` function with reducer function and initial state
  const [shoppingCartState, shoppingCartDispatch] = useReducer(
    shoppingCartReducer,
    { items: [] }
  );

  const [shoppingCart, setShoppingCart] = useState({
    items: [],
  });

  function handleAddItemToCart(id) {
    setShoppingCart((prevShoppingCart) => {
      const updatedItems = [...prevShoppingCart.items];

      const existingCartItemIndex = updatedItems.findIndex(
        (cartItem) => cartItem.id === id
      );
      const existingCartItem = updatedItems[existingCartItemIndex];

      if (existingCartItem) {
        const updatedItem = {
          ...existingCartItem,
          quantity: existingCartItem.quantity + 1,
        };
        updatedItems[existingCartItemIndex] = updatedItem;
      } else {
        const product = DUMMY_PRODUCTS.find((product) => product.id === id);
        updatedItems.push({
          id: id,
          name: product.title,
          price: product.price,
          quantity: 1,
        });
      }

      return {
        items: updatedItems,
      };
    });
  }

  function handleUpdateCartItemQuantity(productId, amount) {
    setShoppingCart((prevShoppingCart) => {
      const updatedItems = [...prevShoppingCart.items];
      const updatedItemIndex = updatedItems.findIndex(
        (item) => item.id === productId
      );

      const updatedItem = {
        ...updatedItems[updatedItemIndex],
      };

      updatedItem.quantity += amount;

      if (updatedItem.quantity <= 0) {
        updatedItems.splice(updatedItemIndex, 1);
      } else {
        updatedItems[updatedItemIndex] = updatedItem;
      }

      return {
        items: updatedItems,
      };
    });
  }

  const ctxValue = {
    items: shoppingCartState.items, // Set context value to leverage useReducer initial state
    addItemToCart: handleAddItemToCart,
    updateItemQty: handleUpdateCartItemQuantity,
  };

  return (
    <CartContext.Provider value={ctxValue}>{children}</CartContext.Provider>
  );
}
```
