# A Different Way of Consuming Context

Let's explore **another way of consuming context**. We've learned how to use the
`useContext` hook to get access to context in a component. **_useContext is the
standard, recommended way of accessing context in a React application_**.
However, you may encounter this second method "in the wild" as well, so let's
look at it now.

There is a special component we can access on the `Context` object, just as we
accessed `Provider`: the `Consumer` component. `Consumer` can be used to wrap
JSX code that should have access to the context.

You must pass a **function** as a child between the opening and closing
`Consumer` tags. The function will receive the `context` object, and return the
JSX that should be returned by the component. See below for an example:

`Cart.jsx`:

```jsx
import { CartContext } from '../store/shopping-cart-context';

export default function Cart({ onUpdateItemQuantity }) {

  return (
    const totalPrice = cartCtx.items.reduce(
    (acc, item) => acc + item.price * item.quantity,
    0
  );
  const formattedTotalPrice = `$${totalPrice.toFixed(2)}`;

    <CartContext.Consumer>
      {(cartCtx) => {
        return (
          <div id="cart">
            {cartCtx.items.length === 0 && <p>No items in cart!</p>}
            {cartCtx.items.length > 0 && (
              <ul id="cart-items">
                {cartCtx.items.map((item) => {
                  const formattedPrice = `$${item.price.toFixed(2)}`;

                  return (
                    <li key={item.id}>
                      <div>
                        <span>{item.name}</span>
                        <span> ({formattedPrice})</span>
                      </div>
                      <div className="cart-item-actions">
                        <button
                          onClick={() => onUpdateItemQuantity(item.id, -1)}
                        >
                          -
                        </button>
                        <span>{item.quantity}</span>
                        <button
                          onClick={() => onUpdateItemQuantity(item.id, 1)}
                        >
                          +
                        </button>
                      </div>
                    </li>
                  );
                })}
              </ul>
            )}
            <p id="cart-total-price">
              Cart Total: <strong>{formattedTotalPrice}</strong>
            </p>
          </div>
        );
      }}
    </CartContext.Consumer>
  );
}
```

This approach works. It is a bit more cumbersome and difficult to read, thus is
not the recommended way of consuming context in a React app.
