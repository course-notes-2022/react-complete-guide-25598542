# Consuming the Context

Let's now consume our context.

In order to consume context in a component, we need to do three things:

1. Import the context object (in our case, `CartContext`) into the consuming
   component

2. Import the `useContext` hook from `react` into the consuming component

3. Call the `useContext` hook inside the consuming component, passing the
   context object as an argument

Make the following updates in the `Cart` component:

```jsx
import { useContext } from 'react';
import { CartContext } from '../store/shopping-cart-context';

export default function Cart({ onUpdateItemQuantity }) {
  const cartCtx = useContext(CartContext);

  const totalPrice = cartCtx.items.reduce(
    (acc, item) => acc + item.price * item.quantity,
    0
  );
  const formattedTotalPrice = `$${totalPrice.toFixed(2)}`;

  return (
    <div id="cart">
      {cartCtx.length === 0 && <p>No items in cart!</p>}
      {cartCtx.length > 0 && (
        <ul id="cart-items">
          {cartCtx.items.map((item) => {
            const formattedPrice = `$${item.price.toFixed(2)}`;

            // omitted...
    </div>
  );
}

```

Note that we're replacing the `items` prop that was being passed down from the
parent previously with the context object returned by the `useContext` hook.

**We must also pass a `value` prop to the `<CartContext.Provider/>` wrapper
component**. The default value we set when creating the context is used only if
a component that was not wrapped by the `Provider` tries to access the context
value.

Pass `value` to the `Provider` in the `App` component as follows:

```jsx
  return (
    <CartContext.Provider value={{ items: [] }}>
      <Header
        cart={shoppingCart}
        onUpdateCartItemQuantity={handleUpdateCartItemQuantity}
      />
      <Shop>
        <ul id="products">
          {DUMMY_PRODUCTS.map((product) => (
            <li key={product.id}>
              <Product {...product} onAddToCart={handleAddItemToCart} />
            </li>
          ))}
        </ul>
      </Shop>
    </CartContext.Provider>
  );
}
```
