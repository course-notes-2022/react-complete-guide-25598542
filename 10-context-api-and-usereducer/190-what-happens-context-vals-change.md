# What Happens when Context Values Change?

When you access a context value in a component, and that value **changes**, that
component function gets **re-executed** by React, just as it would be if its
**internal state** or **props** change. It must be, otherwise the UI would not
be updated if its connected context value changes!
