# Dispatching Actions & Editing State with `useReducer`

Now, let's update our state in the reducer function. We need to handle different
**actions** that should lead to different **updates** in our state.

The idea is that in the state updating fucntions where we **previously** had all
the updating logic, we now want to _dispatch an action_. The action can be
anything: a string, a number, but in most cases it's an **object** with a `type`
property:

```js
{
  type: 'ADD_ITEM';
}
```

Often, actions have **data** attached with which we want to update state. The
data can be called anything, but typically it's called `payload`:

```js
{
    type: 'ADD_ITEM',
    payload: '12345'
}
```

We can now use this action in our reducer function. The `type` property
signifies the **type of action this reducer should be concerned with**. We can
handle multiple action types in a single reducer with conditional logic:

```js
function shoppingCartReducer(state, action) {
  if (action.type === 'ADD_ITEM') {
    // do something...
  }

  if (action.type === 'UPDATE') {
    // do something else...
  }
}
```

We can now refactor the state updating logic into our reducer function:

```jsx
function shoppingCartReducer(state, action) {
  if (action.type === 'ADD_ITEM') {
    const updatedItems = [...state.items];

    const existingCartItemIndex = updatedItems.findIndex(
      (cartItem) => cartItem.id === action.payload
    );
    const existingCartItem = updatedItems[existingCartItemIndex];

    if (existingCartItem) {
      const updatedItem = {
        ...existingCartItem,
        quantity: existingCartItem.quantity + 1,
      };
      updatedItems[existingCartItemIndex] = updatedItem;
    } else {
      const product = DUMMY_PRODUCTS.find(
        (product) => product.id === action.payload
      );
      updatedItems.push({
        id: action.payload,
        name: product.title,
        price: product.price,
        quantity: 1,
      });
    }

    return {
      ...state,
      items: updatedItems,
    };
  }
}
```

and refactor `handle` to simply **dispatch an action of `ADD_ITEM` type**:

```jsx
function handleAddItemToCart(id) {
  shoppingCartDispatch({
    type: 'ADD_ITEM',
    payload: id,
  });
}
```

**We can now refactor the other state-handling function to our reducer
function**. See below for the final state of `shopping-cart-context`:

```jsx
import { createContext, useReducer } from 'react';
import { DUMMY_PRODUCTS } from '../dummy-products';

export const CartContext = createContext({
  items: [],
  addItemToCart: () => {},
  updateItemQty: () => {},
});

function shoppingCartReducer(state, action) {
  if (action.type === 'ADD_ITEM') {
    const updatedItems = [...state.items];

    const existingCartItemIndex = updatedItems.findIndex(
      (cartItem) => cartItem.id === action.payload
    );
    const existingCartItem = updatedItems[existingCartItemIndex];

    if (existingCartItem) {
      const updatedItem = {
        ...existingCartItem,
        quantity: existingCartItem.quantity + 1,
      };
      updatedItems[existingCartItemIndex] = updatedItem;
    } else {
      const product = DUMMY_PRODUCTS.find(
        (product) => product.id === action.payload
      );
      updatedItems.push({
        id: action.payload,
        name: product.title,
        price: product.price,
        quantity: 1,
      });
    }

    return {
      ...state,
      items: updatedItems,
    };
  }

  if (action.type === 'UPDATE_ITEM') {
    const updatedItems = [...state.items];
    const updatedItemIndex = updatedItems.findIndex(
      (item) => item.id === payload.productId
    );

    const updatedItem = {
      ...updatedItems[updatedItemIndex],
    };

    updatedItem.quantity += payload.amount;

    if (updatedItem.quantity <= 0) {
      updatedItems.splice(updatedItemIndex, 1);
    } else {
      updatedItems[updatedItemIndex] = updatedItem;
    }

    return {
      ...state,
      items: updatedItems,
    };
  }
}

export default function CartContextProvider({ children }) {
  const [shoppingCartState, shoppingCartDispatch] = useReducer(
    shoppingCartReducer,
    { items: [] }
  );

  function handleAddItemToCart(id) {
    shoppingCartDispatch({
      type: 'ADD_ITEM',
      payload: id,
    });
  }

  function handleUpdateCartItemQuantity(productId, amount) {
    shoppingCartDispatch({
      type: 'UPDATE_ITEM',
      payload: {
        productId,
        amount,
      },
    });
  }

  const ctxValue = {
    items: shoppingCartState.items,
    addItemToCart: handleAddItemToCart,
    updateItemQty: handleUpdateCartItemQuantity,
  };

  return (
    <CartContext.Provider value={ctxValue}>{children}</CartContext.Provider>
  );
}
```

**Note**: You can use the `useReducer` hook in **any** React component that
needs state, _not just in conjunction with `Context` API_!
