# Migrating the Entire Demo Project to Use the Context API

Let's now fully migrate the rest of the app to use the Context API.

First, refactor the `Header` component to remove the `cart` and
`onUpdateCartItem` props we were previously passing down from `App`:

```jsx
import { useRef } from 'react';
import { useContext } from 'react'; // Import useContext hook and CartContext
import { CartContext } from '../store/shopping-cart-context.jsx';
import CartModal from './CartModal.jsx';

export default function Header() {
  const modal = useRef();
  const { items } = useContext(CartContext); // Call useContext to get access to the context object in Header component

  const cartQuantity = items.length; // use the context object

  function handleOpenCartClick() {
    modal.current.open();
  }

  let modalActions = <button>Close</button>;

  if (cartQuantity > 0) {
    modalActions = (
      <>
        <button>Close</button>
        <button>Checkout</button>
      </>
    );
  }

  return (
    <>
      <CartModal ref={modal} title="Your Cart" actions={modalActions} />
      <header id="main-header">
        <div id="main-title">
          <img src="logo.png" alt="Elegant model" />
          <h1>Elegant Context</h1>
        </div>
        <p>
          <button onClick={handleOpenCartClick}>Cart ({cartQuantity})</button>
        </p>
      </header>
    </>
  );
}
```

We'll also update the `CartModal` component to use the context object. First,
update `App` to expose the `updateItemQty` method on the created context:

```jsx
// imports omitted...

function App() {
  const [shoppingCart, setShoppingCart] = useState({
    items: [],
  });

  // omitted...

  function handleUpdateCartItemQuantity(productId, amount) {
    setShoppingCart((prevShoppingCart) => {
      const updatedItems = [...prevShoppingCart.items];
      const updatedItemIndex = updatedItems.findIndex(
        (item) => item.id === productId
      );

      const updatedItem = {
        ...updatedItems[updatedItemIndex],
      };

      updatedItem.quantity += amount;

      if (updatedItem.quantity <= 0) {
        updatedItems.splice(updatedItemIndex, 1);
      } else {
        updatedItems[updatedItemIndex] = updatedItem;
      }

      return {
        items: updatedItems,
      };
    });
  }

  const ctxValue = {
    items: shoppingCart.items,
    addItemToCart: handleAddItemToCart,
    updateItemQty: handleUpdateCartItemQuantity, // Add updateItemQty property to `ctxValue`
  };

  return (
    <CartContext.Provider value={ctxValue}>
      <Header
        cart={shoppingCart}
        onUpdateCartItemQuantity={handleUpdateCartItemQuantity}
      />
      <Shop>
        <ul id="products">
          {DUMMY_PRODUCTS.map((product) => (
            <li key={product.id}>
              <Product {...product} />
            </li>
          ))}
        </ul>
      </Shop>
    </CartContext.Provider>
  );
}

export default App;
```

Add `updateItemQty` to the default context object in `CartContext`:

```jsx
import { createContext } from 'react';

export const CartContext = createContext({
  items: [],
  addItemToCart: () => {},
  updateItemQty: () => {},
});
```

Note that we can now **remove the context altogether** from the `CartModal`
component, as the only thing it was doing with the context values was passing
them down to the `Cart` component which is **already using the context**.

Refactor `CartModal` as follows:

```jsx
import { forwardRef, useImperativeHandle, useRef } from 'react';
import { createPortal } from 'react-dom';
import Cart from './Cart';

const CartModal = forwardRef(function Modal({ title, actions }, ref) {
  const dialog = useRef();

  useImperativeHandle(ref, () => {
    return {
      open: () => {
        dialog.current.showModal();
      },
    };
  });

  return createPortal(
    <dialog id="modal" ref={dialog}>
      <h2>{title}</h2>
      <Cart />
      <form method="dialog" id="modal-actions">
        {actions}
      </form>
    </dialog>,
    document.getElementById('modal')
  );
});

export default CartModal;
```

We can now refactor the `Cart` component to use the context directly:

```jsx
import { useContext } from 'react';
import { CartContext } from '../store/shopping-cart-context';

export default function Cart() {
  const { updateItemQty, items } = useContext(CartContext);

  const totalPrice = items.reduce(
    (acc, item) => acc + item.price * item.quantity,
    0
  );
  const formattedTotalPrice = `$${totalPrice.toFixed(2)}`;

  return (
    <div id="cart">
      {items.length === 0 && <p>No items in cart!</p>}
      {items.length > 0 && (
        <ul id="cart-items">
          {items.map((item) => {
            const formattedPrice = `$${item.price.toFixed(2)}`;

            return (
              <li key={item.id}>
                <div>
                  <span>{item.name}</span>
                  <span> ({formattedPrice})</span>
                </div>
                <div className="cart-item-actions">
                  <button onClick={() => updateItemQty(item.id, -1)}>-</button>
                  <span>{item.quantity}</span>
                  <button onClick={() => updateItemQty(item.id, 1)}>+</button>
                </div>
              </li>
            );
          })}
        </ul>
      )}
      <p id="cart-total-price">
        Cart Total: <strong>{formattedTotalPrice}</strong>
      </p>
    </div>
  );
}
```

Refresh your browser, and note that the app still works as before, but **we have
now completely refactored to get rid of prop drilling!**
