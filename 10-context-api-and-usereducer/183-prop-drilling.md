# Understanding Prop Drilling and Project Overview

Most React apps consist of **multiple components**, structured in a **component
tree** of parents and children components.

You'll also likely need to manage some **state**. In many cases, that state will
need to be **lifted up**, as it may be used in **one component**, but modified
in **another** component. As we've learned, we can lift state up to a parent
component, and pass it down as props to child components.

**However**, often times we need to pass props down **multiple levels**, through
components that don't _need this piece of state_, in order to get it to
components that _do need it_. This passing down of props through multiple
components is known as **prop drilling**:

![prop drilling](./screenshots/prop-drilling.png)

Prop drilling can be a problem because we need to write a lot of extra
boilerplate code. Therefore, we'll take a look at some possible solutions.
