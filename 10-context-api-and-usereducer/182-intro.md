# Module Intro

In this section, we'll learn more advanced state management features offered by
React. As you'll experience when building more complex apps, state management
can become tricky. We'll learn about:

- The problem of shared state: **Prop Drilling**
- Embracing **Component Composition**
- Sharing state with **Context**
- Managing complex state with **Reducers**
