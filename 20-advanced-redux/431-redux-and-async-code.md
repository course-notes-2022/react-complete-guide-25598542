# Redux & Async Code

Let's now learn some new concepts. We'll learn to connect Redux and **async
code**.

When we add or remove items, we want to send a request to a backend server to
**store the cart state**, and when the user **refreshes the application** we
want to fetch the cart state from the backend. For this, we'll use **Firebase**.
Of course, you could also build your own backend in NodeJS or your preferred
backend language.

How do we integrate the HTTP requests we'll need to send with our **Redux
code**? Recall that **reducers** must be **pure, synchronous** functions, so we
can't put **async logic** there.

> **NEVER** perform side effects **inside reducers**!

## Side Effects, Async Tasks and Redux

When we need to execute **side effects** and/or **async code** in Redux, we have
**two options**:

1. Do it **inside the components** (e.g. via `useEffect()`)

2. Do it inside the **action creators**

So far, we've used action creators **indirectly**. We'll see both options in the
upcoming lectures.
