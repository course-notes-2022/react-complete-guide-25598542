# Using `useEffect` with Redux

One possible, better solution would be to continue dispatching the
`addItemToCart` action from the component, and **then use `useEffect to run a
_side effect_** whenever the **store state changes** .

For example, let's make the following changes to `App.js`:

```js
import { useEffect } from 'react';
import { useSelector } from 'react-redux';
import Cart from './components/Cart/Cart';
import Layout from './components/Layout/Layout';
import Products from './components/Shop/Products';

function App() {
  const cartIsVisible = useSelector((state) => state.ui.cartIsVisible);
  const cart = useSelector((state) => state.cart);

  useEffect(() => {
    fetch('https://reactcompleteguide25598542-default-rtdb.firebaseio.com/', {
      method: 'PUT',
      body: JSON.stringify(cart),
    });
  }, [cart]);

  return (
    <Layout>
      {cartIsVisible && <Cart />}
      <Products />
    </Layout>
  );
}

export default App;
```

Note the following:

- We are selecting the `cart` piece of state from the store using the
  `useSelector` hook

- We are calling `useEffect` and using it to **send our request to update the
  backend** with the latest `cart` state. We also add `cart` to the
  **dependencies array** of `useEffect`, so that the hook will run **every time
  `cart` changes**.

- `useSelector` **automatically sets up a subscription** to the piece of state
  returned, so that a change to `cart` on the store will trigger a change to
  `cart` **defined in our component**, which will cause the `useEffect` hook to
  run and the **component function to execute again**.

- We are putting this functionality in the `App` component, but it does **not
  have to go here**.

## Fat Reducers, Lean Components

With this solution, we have created a **fat reducer**, i.e. all the
**cart-updating logic** is in the **reducer**, and the **side-effect/async
logic** is in the **component**.

Note that the **order is important**. We must:

1. **First**, update cart by dispatching the action to the reducer, and

2. **Second**, perform the side effect/async code that _depends on the updated
   state_ in the component.
