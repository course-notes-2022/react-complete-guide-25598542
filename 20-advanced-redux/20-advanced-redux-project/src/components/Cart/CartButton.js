import { useDispatch, useSelector } from 'react-redux';
import { cartActions } from '../../store/show-cart';
import classes from './CartButton.module.css';

const CartButton = (props) => {
  const dispatch = useDispatch();
  const itemCount = useSelector((state) => state.cart.itemCount);

  const toggleCart = () => {
    dispatch(cartActions.toggleShowCart());
  };

  return (
    <button onClick={toggleCart} className={classes.button}>
      <span>My Cart</span>
      <span className={classes.badge}>{itemCount}</span>
    </button>
  );
};

export default CartButton;
