import { useSelector } from 'react-redux';
import Card from '../UI/Card';
import classes from './Cart.module.css';
import CartItem from './CartItem';
import { PRODUCTS } from '../../data/products';

const Cart = (props) => {
  const itemCount = useSelector((state) => state.cart.itemCount);
  const cartContents = useSelector((state) => state.cart.cartContents);

  return (
    <Card className={classes.cart} {...props}>
      <h2>Your Shopping Cart</h2>
      <ul>
        {itemCount > 0 &&
          Object.keys(cartContents).map((key) => {
            const product = PRODUCTS.find((product) => product.title === key);
            return cartContents[key] > 0 ? (
              <CartItem
                key={product.productId}
                item={{
                  title: key,
                  quantity: cartContents[key],
                  total: product.price * cartContents[key],
                  price: product.price,
                }}
              />
            ) : null;
          })}

        {!itemCount && <em>No Items In Cart</em>}
      </ul>
    </Card>
  );
};

export default Cart;
