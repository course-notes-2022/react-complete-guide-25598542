import { useDispatch, useSelector } from 'react-redux';
import { cartActions } from '../../store/show-cart';
import Card from '../UI/Card';
import classes from './ProductItem.module.css';

const ProductItem = (props) => {
  const dispatch = useDispatch();
  const currentItemId = useSelector((state) => state.cart.currentItemId);
  const cartContents = useSelector((state) => state.cart.cartContents);

  const { title, price, description } = props;

  const addItemToCart = () => {
    dispatch(cartActions.addItemToCart(title));
  };

  return (
    <li className={classes.item}>
      <Card>
        <header>
          <h3>{title}</h3>
          <div className={classes.price}>${price.toFixed(2)}</div>
        </header>
        <p>{description}</p>
        <div className={classes.actions}>
          <button onClick={addItemToCart}>Add to Cart</button>
        </div>
      </Card>
    </li>
  );
};

export default ProductItem;
