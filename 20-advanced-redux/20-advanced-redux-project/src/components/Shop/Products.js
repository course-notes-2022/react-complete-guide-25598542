import ProductItem from './ProductItem';
import { PRODUCTS } from '../../data/products';
import classes from './Products.module.css';

const groupProductsByTitle = () => {};

const Products = (props) => {
  return (
    <section className={classes.products}>
      <h2>Buy your favorite products</h2>
      <ul>
        {PRODUCTS.map((product) => (
          <ProductItem
            key={product.productId}
            title={product.title}
            price={product.price}
            description={product.description}
          />
        ))}
      </ul>
    </section>
  );
};

export default Products;
