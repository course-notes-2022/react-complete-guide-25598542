import { createSlice } from '@reduxjs/toolkit';
import { PRODUCTS } from '../data/products';

const initializeCartContents = () => {
  const initialState = {};
  for (let p of PRODUCTS) {
    initialState[p.title] = 0;
  }
  return initialState;
};

const initialCartState = {
  isCartShown: false,
  cartContents: initializeCartContents(),
  itemCount: 0,
};

const cartSlice = createSlice({
  name: 'cart',
  initialState: initialCartState,
  reducers: {
    toggleShowCart(state) {
      state.isCartShown = !state.isCartShown;
    },

    addItemToCart(state, action) {
      if (state.cartContents[action.payload] !== undefined) {
        state.cartContents[action.payload]++;
      }
      state.itemCount += 1;
    },

    removeItemFromCart(state, action) {
      if (state.cartContents[action.payload] > 0) {
        state.cartContents[action.payload]--;
        state.itemCount -= 1;
      }
    },
  },
});

export const cartActions = cartSlice.actions;
export default cartSlice.reducer;
