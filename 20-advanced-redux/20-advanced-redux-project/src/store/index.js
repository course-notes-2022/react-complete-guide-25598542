import { configureStore } from '@reduxjs/toolkit';
import cartReducer from './show-cart';

const store = configureStore({
  reducer: {
    cart: cartReducer,
  },
});

export default store;
