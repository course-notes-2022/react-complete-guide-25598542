export const PRODUCTS = [
  {
    title: 'Large Sprocket',
    price: 19.99,
    description: 'This is the largest sprocket money can buy!',
    productId: 1,
  },
  {
    title: 'Medium Cog',
    price: 15.99,
    description: 'This cog is the best!',
    productId: 2,
  },
  {
    title: 'Small Widget',
    price: 9.99,
    description: "You've never seen a better widget than this!",
    productId: 3,
  },
];
