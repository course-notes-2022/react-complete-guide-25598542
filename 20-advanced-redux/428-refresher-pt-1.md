# Refresher: Part 1/2

In this lecture we'll begin with a starting project and add Redux to it.
Currently the starter project is not interactive; we'll add interactivity to it
in this lesson.

## Practice For You

As practice, try implementing the following functionality **using Redux**:

1. Toggle the shopping cart component between show/hide states on click of the
   `My Cart` button in the header

2. Add a product to the cart on click of the `Add to Cart` button (or increment
   the quantity by 1 if the product is already in the cart)

3. Add/remove products from the cart using the `+/-` buttons. Remove the product
   from the cart entirely if the user clicks `-` and the quantity is 1.

## Solution Walkthrough

### Installation and Show/Hide Cart

1. Install `react-redux` and `@reduxjs/toolkit`:

> `npm install @reduxjs/toolkit react-redux`

2. Create the `src/store/index.js` file (no content yet).

3. Create a `src/store/cart-slice.js` file (no content yet).

4. Crate a `src/store-ui-slice.js` file:

```js
import { createSlice } from '@reduxjs/toolkit';

const initialUiState = {
  cartIsVisible: false,
};

const uiSlice = createSlice({
  name: 'uiSlice',
  initialState: initialUiState,
  reducers: {
    toggle(state) {
      state.cartIsVisible = !state.cartIsVisible;
    },
  },
});

export const uiActions = uiSlice.actions;
export default uiSlice;
```

5. Provide the `store` to the root application component in `src/index.js`:

```js
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import store from './store';
import './index.css';
import App from './App';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
```

6. Modify the `CartButton` component to show the cart on click:

```js
import { useDispatch } from '@reduxjs/toolkit';
import { uiActions } from '../../store/ui-slice';
import classes from './CartButton.module.css';

const CartButton = (props) => {
  const dispatch = useDispatch();

  const toggleCartHandler = () => {
    dispatch(uiActions.toggle());
  };

  return (
    <button onClick={toggleCartHandler} className={classes.button}>
      <span>My Cart</span>
      <span className={classes.badge}>1</span>
    </button>
  );
};

export default CartButton;
```

7. Modify the `App.js` component to show the cart conditionally dependent upon
   the value of the state:

```js
import { useSelector } from 'react-redux';
import Cart from './components/Cart/Cart';
import Layout from './components/Layout/Layout';
import Products from './components/Shop/Products';

function App() {
  const cartIsVisible = useSelector((state) => state.ui.cartIsVisible);
  return (
    <Layout>
      {cartIsVisible && <Cart />}
      <Products />
    </Layout>
  );
}

export default App;
```

### Managing the Cart Content

1. Refactor `cart-slice.js` as follows:

```js
import { createSlice } from '@reduxjs/toolkit';

const initialCartState = {
  items: [],
  totalQuantity: 0,
};
createSlice({
  name: 'cart',
  initialState: initialCartState,
  reducers: {
    addItemToCart(state, action) {
      const newItem = action.payload;
      const existingItem = state.items.find((item) => item.id === newItem.id);
      if (!existingItem) {
        state.items.push({
          id: newItem.id,
          price: newItem.price,
          quantity: 1,
          totalPrice: newItem.price,
          title: newItem.title,
        });
      } else {
        existingItem.quantity++;
        existingItem.totalPrice += newItem.price;
      }
    },
    removeItemFromCart() {},
  },
});
```
