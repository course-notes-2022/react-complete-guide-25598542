# Handling Http States & Feedback with Redux

Let's learn to handle the response from our HTTP request, as well as errors that
may occur.

Add the `Notification` component file and CSS module file attached to the
lesson.

Refactor `App.js` as follows:

```js
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { uiActions } from './store/ui-slice';
import Cart from './components/Cart/Cart';
import Layout from './components/Layout/Layout';
import Products from './components/Shop/Products';
import Notification from './components/UI/Notification';

/*
Control the display of the Notification component on
initial render. `isInitial` is defined OUTSIDE the `App`
component definition, so it will NOT be re-evaluated
when the component is re-rendered.
*/
let isInitial = true;

function App() {
  const dispatch = useDispatch();
  const cartIsVisible = useSelector((state) => state.ui.cartIsVisible);
  const cart = useSelector((state) => state.cart);
  const notification = useSelector((state) => state.ui.notification);

  useEffect(() => {
    const sendCartData = async () => {
      dispatch(
        // dispatch when request is PENDING
        uiActions.showNotification({
          status: 'pending',
          title: 'Sending...',
          message: 'Sending cart data',
        })
      );

      // Send request
      const response = await fetch('https://FIREBASE_URL_HERE', {
        method: 'PUT',
        body: JSON.stringify(cart),
      });

      if (!response.ok) {
        throw new Error('sending cart data failed');
      }

      // Dispatch when request SUCCEEDS
      dispatch(
        uiActions.showNotification({
          status: 'success',
          title: 'Success!',
          message: 'Sent cart data successfully!',
        })
      );
    };

    // Set `isInitial` flag to false after first request
    if (isInitial) {
      isInitial = false;
      return;
    }
    sendCartData().catch((err) => {
      // Dispatch on caught ERROR
      dispatch(
        uiActions.showNotification({
          status: 'error',
          title: 'Error!',
          message: 'Sending cart data failed!',
        })
      );
    });
  }, [cart, dispatch]);

  return (
    <>
      {notification && (
        <Notification
          status={notification.status}
          title={notification.title}
          message={notification.message}
        />
      )}
      <Layout>
        {cartIsVisible && <Cart />}
        <Products />
      </Layout>
    </>
  );
}

export default App;
```

Save changes and reload. Observe that we are seeing the expected response on
saving:

![success](./screenshots/error-handling-success.png)
