# Explore Redux Dev Tools

The Redux Dev Tools are extra tools that we can use to make debugging our Redux
apps easier.

Search for `Redux Dev Tools` to find the documentation. Redux Dev Tools can be
installed as a browser extension (recommended) or a standalone app.

When using with Redux Toolkit, the Redux Dev Tools will work without any
additional configuration. With the Dev Tools, we can view actions, state, and
more.

The "Actions" panel shows the actions that have been dispatched. We can view the
state after the action, and compare the "diff" between the previous and current
state.

Clicking on `Jump` in any action allows us to "time-travel" to the state of the
application at that particular action.
