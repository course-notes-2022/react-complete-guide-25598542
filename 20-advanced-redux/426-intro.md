# Intro

In this section, we'll dive deeper into Redux and explore how to handle **async
tasks**, code organization, and the **Redux Dev Tools**.
