# Where to Put Our Logic

So, where do we put our logic if we need to send the **updated cart** to our
basic backend, while **observing the rules of Redux** and not creating any side
effects or async code in our **reducers**?

Recall our two options:

1. Put the logic **inside the components** (e.g. via `useEffect()`)

2. Put the logic inside the **action creators**

Let's look at **option 1 first**.

## Putting Side Effects/Async Code in the Component

Open `ProductItem.js`. One approach we _could_ take is to import `useSelector`,
leverage it to get the **current state of the cart**, **update** a **copy** of
that state, and then **send an HTTP request** to our backend **inside
`addToCartHandler`**. That solution might look like the following:

```js
const addToCartHandler = () => {
  const newTotalQuantity = cart.totalQuantity + 1;

  const updatedItems = cart.items.slice(); // create copy via slice to avoid mutating original state
  const existingItem = updatedItems.find((item) => item.id === id);
  if (existingItem) {
    const updatedItem = { ...existingItem }; // new object + copy existing properties to avoid state mutation
    updatedItem.quantity++;
    updatedItem.totalPrice = updatedItem.totalPrice + price;
    const existingItemIndex = updatedItems.findIndex((item) => item.id === id);
    updatedItems[existingItemIndex] = updatedItem;
  } else {
    updatedItems.push({
      id: id,
      price: price,
      quantity: 1,
      totalPrice: price,
      name: title,
    });
  }

  const newCart = {
    totalQuantity: newTotalQuantity,
    items: updatedItems,
  };

  dispatch(cartActions.replaceCart(newCart));

  // and then send Http request
  // fetch('firebase-url', { method: 'POST', body: JSON.stringify(newCart) })

  // dispatch(
  //   cartActions.addItemToCart({
  //     id,
  //     title,
  //     price,
  //   })
  // );
};
```

**This is _not_ the way we'll be implementing our solution**. The above code is
**sub-optimal** The problem with this approach is that if we used this
everywhere we needed to update the cart, we'd need to **copy all this logic
everywhere it was needed** (or outsource it to a function). Also, we **don't do
the data transformation in our reducers**.

## Fat Reducers vs. Fat Components vs. Fat Actions

Where should your logic go?

| Synchronous, side-effect free code      | Async code/code with side-effects            |
| --------------------------------------- | -------------------------------------------- |
| Prefer **reducers**                     | Prefer **action creators** or **components** |
| **Avoid** action creators or components | **Never** use reducers                       |

Currently, all the code above (except for the commented HTTP request) performs
**synchronous, side-effect free** logic (i.e. transforming the data). Thus
placing it in the component is **not** the preferred solution.
