# Redux, Side Effects and Async Tasks

Let's start with Async code and side effects in general in relation to Redux.

There is one, **super-important** rule:

> Reducers must be **pure, side-effect free, synchronous** functions!

Reducers must take an **input** and **synchronously produce the same output**
when given the same input.

But this raises a question:

> Where should side-effects and async tasks be executed?

We have two possibilities:

1. **Inside the components**, e.g. via `useEffect`

2. Inside the **action creator functions**.

Redux has a solution that allows us to **add side effects** and **perform async
tasks** for the second option! We'll look at how to implement **both solutions**
in this module.
