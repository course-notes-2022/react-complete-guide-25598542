# Using an Action Creator Thunk

Recall our two options for where to create side effects/async logic in a Redux
application:

1. Put the logic **inside the components** (e.g. via `useEffect()`)

2. Put the logic inside the **action creators**

Let's now look at **option 2**.

## Action Creators

**Action Creators** are functions that generate **action objects**. We've
already been using action creators that are generated **for us** by Redux:

```js
const uiSlice = createSlice({
  name: 'uiSlice',
  initialState: initialUiState,
  reducers: {
    toggle(state) {
      state.cartIsVisible = !state.cartIsVisible;
    },

    showNotification(state, action) {
      state.notification = {
        status: action.payload.status,
        title: action.payload.title,
        message: action.payload.message,
      };
    },
  },
});

export const uiActions = uiSlice.actions;
```

`createSlice` returns a **state slice object** with an `actions` attribute
attached. `actions` is an object where the **keys** are identical to the reducer
function names defined on the slice, and the **values** are functions that
return **unique identifiers** for the **action** associated with that reducer
function.

### Creating our Own Action Creators

We can also write **our own action creators** that will give us `thunks`. A
`thunk` is simply a **function that delays an action until later**. Thunks are
action creator functions that do **not return the action itself**, but instead
return **another function** that **eventually returns the action**:

![thunk](./screenshots/thunk.png)

### How Do Thunks Work?

Let's create our own thunk. Add the following to `cart-slice.js`:

```js
import { createSlice } from '@reduxjs/toolkit';
import { uiActions } from './ui-slice';

// Omitted...

/*
Create a new function, `sendCartData`.
`sendCartData` returns a function. That function
dispatches a notification, creates a new nested function
that sends a request. If request succeeds, we dispatch
a success notification. If it fails, we dispatch an
error notification. We do NOT call any functions YET!
*/
export const sendCartData = (cart) => {
  // Perform any async/side effect generating code here!
  return async (dispatch) => {
    dispatch(
      uiActions.showNotification({
        status: 'pending',
        title: 'Sending...',
        message: 'Sending cart data',
      })
    );

    const sendRequest = async () => {
      const response = await fetch(
        'https://reactcompleteguide25598542-default-rtdb.firebaseio.com/cart.json',
        {
          method: 'PUT',
          body: JSON.stringify(cart),
        }
      );

      if (!response.ok) {
        throw new Error('sending cart data failed');
      }
    };

    try {
      await sendRequest();
      dispatch(
        uiActions.showNotification({
          status: 'success',
          title: 'Success!',
          message: 'Sent cart data successfully!',
        })
      );
    } catch (err) {
      dispatch(
        uiActions.showNotification({
          status: 'error',
          title: 'Error!',
          message: 'Sending cart data failed!',
        })
      );
    }
  };
};

export const cartActions = cartSlice.actions;
export default cartSlice;
```

How do we call `sendCartData`, and **where**?

## Using our New Thunk

Make the following updates to `App.js`:

```js
import { useEffect } from 'react';
import { useSelector, useDispatch } from 'react-redux';
import { sendCartData } from './store/cart-slice';
import Cart from './components/Cart/Cart';
import Layout from './components/Layout/Layout';
import Products from './components/Shop/Products';
import Notification from './components/UI/Notification';

let isInitial = true;

function App() {
  const dispatch = useDispatch();
  const cartIsVisible = useSelector((state) => state.ui.cartIsVisible);
  const cart = useSelector((state) => state.cart);
  const notification = useSelector((state) => state.ui.notification);

  useEffect(() => {
    if (isInitial) {
      isInitial = false;
      return;
    }

    dispatch(sendCartData(cart));
  }, [cart, dispatch]);

  return (
    <>
      {notification && (
        <Notification
          status={notification.status}
          title={notification.title}
          message={notification.message}
        />
      )}
      <Layout>
        {cartIsVisible && <Cart />}
        <Products />
      </Layout>
    </>
  );
}

export default App;
```

Note that we are **importing our `sendCartData` action creator function, and
passing it to the `dispatch` method**, _just as we did with the Redux-provided
action creators_.

### But I Thought Action Creators Returned OBJECTS, Not Functions!

Before, we `dispatch`-ed functions that returned **action objects**. Now, we're
`dispatch`-ing functions that return **other functions**. How is this possible?

Redux with Redux Toolkit accepts action creators that return **functions** as
well as **objects**. If it sees an action creator that returns a **function**,
it will:

- **Execute** the returned function

- Automatically pass the **dispatch** argument to the returned function so that
  we can **dispatch other actions** inside that function, allowing us to perform
  \*\*side effects

- The dispatched actions reach the reducers as normal

When calling:

```js
useEffect(() => {
  if (isInitial) {
    isInitial = false;
    return;
  }

  dispatch(sendCartData(cart)); // This!
}, [cart, dispatch]);
```

`dispatch` in the `App` component, Redux will execute all the logic in the
return function of `sendCartData`, **including the dispatching of other actions
_and_ the HTTP request**.

### Fat Actions, Lean Components

Save all changes, and test in the browser. Note that we **still update the
backend as before**, but our component is now **much leaner**:

![custom action creator](./screenshots/custom-action-creator.png)

AGAIN: This is simply an **alternative** to keeping the logic in our components
("fat components"). Use whichever method makes sense for **your application**!
