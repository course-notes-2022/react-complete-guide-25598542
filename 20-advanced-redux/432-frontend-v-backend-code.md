# Frontend Code vs. Backend Code

Let's start with running the side effect **inside our component**.

## Frontend Code Depends on Backend Code

The refactoring we'll do to our **frontend** code depends heavily on the type of
**backend** we have. If we had a full API server, we could write **less code**
on the frontend, and simply send data to/receive data from the backend (and
store it in our `store`).

With our basic Firebase backend, we do _not_ have a fully-functioning API server
that has business logic to transform data. It basically just **stores incoming
data**, thus our frontend will have to do **more work**.

![frontend vs. backend code](./screenshots/frontend-v-backend-code.png)
