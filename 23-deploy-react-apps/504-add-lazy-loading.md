# Adding Lazy Loading

How do we add lazy loading?

Imagine that we want to load the `BlogPage` and all the code referenced by it
lazily in the `App` component:

```jsx
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

import BlogPage, { loader as postsLoader } from './pages/Blog';
import HomePage from './pages/Home';
import PostPage, { loader as postLoader } from './pages/Post';
import RootLayout from './pages/Root';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: 'posts',
        children: [
          { index: true, element: <BlogPage />, loader: postsLoader },
          { path: ':id', element: <PostPage />, loader: postLoader },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

To load the blog page lazily, we first remove the `import`, and re-add it in a
way that only loads it when it is needed. Refactor `App` as follows:

`import` here is the same `import` statement we use at the top of our files to
import dependencies from other files:

```js
import RootLayout from './pages/Root`
```

We can also call `import` as a **function**. Calling `import` in this way allows
us to import dependencies **dynamically**, i.e. only when it is needed.

When calling `import` in this way, we pass it a **path** to the resource we want
to import. `import` returns a Promise with the loaded file if successful.

### Lazy-Loading the `loader` Function from `BlogPage`

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

// import BlogPage, { loader as postsLoader } from './pages/Blog';
import HomePage from './pages/Home';
import PostPage, { loader as postLoader } from './pages/Post';
import RootLayout from './pages/Root';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: 'posts',
        children: [
          {
            index: true,
            element: <BlogPage />,
            // Lazy-load the `loader` function from './pages/Blog'
            loader: () =>
              import('./pages/Blog').then((module) => module.loader()),
          },
          { path: ':id', element: <PostPage />, loader: postLoader },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

On this particular module, we want to return the `loader` function. Now, the
`import` statement will be executed **only when the user navigates to the
`BlogPage` component**, _not when the `App` component loads!_

### Lazy-Load the `BlogPage` Component

Let's now lazy-load the `BlogPage` component itself:

```js
import { lazy, Suspense } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/Home';
import PostPage, { loader as postLoader } from './pages/Post';
import RootLayout from './pages/Root';

/*
Lazy-load the `BlogPage` component, and turn the Promise
returned by `import` into JSX using the `lazy` function
built into React
*/
const BlogPage = lazy(() => import('./pages/Blog'));

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: 'posts',
        children: [
          {
            index: true,
            // Wrap the BlogPage in a Suspense component
            element: (
              <Suspense fallback={<p>Loading...</p>}>
                <BlogPage />
              </Suspense>
            ),
            // Lazy-load the `loader` function from './pages/Blog'
            loader: () =>
              import('./pages/Blog').then((module) => module.loader()),
          },
          {
            path: ':id',
            element: <PostPage />,
            loader: () => import('./'),
          },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

We can see that the code for the `BlogPage` component is being lazy-loaded by
inspecting the "Network" tab in the browser dev tools:

![blog page lazy-loaded](./screenshots/blog-page-lazy-loaded.png)

Observe the `200` response for `src_pages_Blog.js.chunk.js`.

### Implementing Lazy-Loading for Individual Blog Posts

We can lazy-load the `PostPage` component in the same way we did the `BlogPage`:

```js
import { lazy, Suspense } from 'react';
import { createBrowserRouter, RouterProvider } from 'react-router-dom';

// import BlogPage, { loader as postsLoader } from './pages/Blog';
import HomePage from './pages/Home';
// import PostPage, { loader as postLoader } from './pages/Post';
import RootLayout from './pages/Root';

/*
Lazy-load the `BlogPage` component, and turn the Promise
returned by `import` into JSX using the `lazy` function
built into React
*/
const BlogPage = lazy(() => import('./pages/Blog'));

const PostPage = lazy(() => import('./pages/Post'));

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      {
        index: true,
        element: <HomePage />,
      },
      {
        path: 'posts',
        children: [
          {
            index: true,
            element: (
              <Suspense fallback={<p>Loading...</p>}>
                <BlogPage />
              </Suspense>
            ),
            // Lazy-load the `loader` function from './pages/Blog'
            loader: () =>
              import('./pages/Blog').then((module) => module.loader()),
          },
          {
            path: ':id',
            element: (
              <Suspense fallback={<p>Loading...</p>}>
                <PostPage />
              </Suspense>
            ),
            // Note that we pass the `params` object
            // to ensure the route params are present
            // for `react-router-dom`
            loader: ({ params }) =>
              import('./pages/Post').then((module) =>
                module.loader({ params })
              ),
          },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```
