# Deployment Example

Now that we've built the app, let's deploy it. It is very important that a React
**SPA** is a **static website**, i.e. it contains **only HTML, CSS, and
Javascript** and _no code that will be executed on the server_.

In this case, we need only a **static site host**.

We can find many deployment hosting providers, blog posts, etc. when searching
for "static site host". We'll use **Firebase Host** for this example.

[See the documentation at https://firebase.google.com/docs/hosting](https://firebase.google.com/docs/hosting)
for detailed instructions on how to implement Firebase Hosting to host your app.

Once setup, choose the following options when prompted at the CLI:

- Which Firebase Features do you want to set up for this directory?:
  `Hosting: Configure files for Firebase Hosting and (optionally) set up Github Action deploys`

- Project Setup:
  - Please select an option: `Use existing project`
  - Select a default Firebase project for this directory: (Select the project
    you just created)
- Hosting Setup:
  - What do you want to use as your public directory? `build`
  - Configure as a single-page app?: `y`
  - Set up automatic builds and deploys with GitHub? `No`
  - File 'build/index.html' already exists. Overwrite? `No`

Run `firebase deploy` to upload your files to Firebase. You will receive a URL
to your deployed application. You can also setup a **custom URL**.
