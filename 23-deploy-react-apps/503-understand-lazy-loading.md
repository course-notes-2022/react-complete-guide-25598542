# Understanding Lazy Loading

For this section we'll use a simple application that uses React Router, with
multiple page components and multiple pages.

Let's assume that we've thoroughly tested the code. The next step is to
**optimize** the code. We'll focus on that step in the next lessons,
specifically using **lazy-loading**.

## What is Lazy Loading?

**Lazy loading** refers to the act of loading pieces of code **only when they're
needed**.

To understand why lazy loading is important, we need to understand how our
application will be served to the user **without** lazy loading.

We have multiple `import` statements in our files where we import code from
other files. When the importing file is evaluated by the browser, the imported
file will be imported so that the importing file can run.

When the application is loaded in the browser, **all the `import` statements
must be evaluated**, and all the dependencies must be resolved before the
application can run.

Theoretically, this means that **all code files must be loaded before anytthing
can be shown on the screen**. In complex applications, this can impact
performance and initial page load.

This is where lazy loading comes in. We load components **only when they are
needed**!
