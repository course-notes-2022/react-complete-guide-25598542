# Server-Side Routing and Required Configuration

We've now uploaded and deployed our website. When we deployed the app, we chose
to configure it as a SPA. Why is this important?

The navigation in our app is provided by `react-router-dom`, a **client-side**
package that does not execute on the server. Therefore, the code for evaluating
the URL and loading the appropriate components executes on the client.

This implies that when we **click** a link in the app, the necessary routing
logic is executed on the client. But what about when the user **enters a route
into the browser bar**?

1. The browser **must** send a request to the **server** for the requested
   resource.

2. The server sends back a **response**, which in the case of a SPA is the
   **entire React application**. Regardless of whether the user requests the `/`
   index route, or another route such as `/posts`, **the server _must_ return
   the _same HTML, CSS and JS_ of the full application** so that the requested
   path can be resolved on the **client side**:

![client-server SPA cycle](./screenshots/client-server-spa-cycle.png)

**By default**, the server will look for the requested route (e.g. `/posts`) in
a file/directory _on the server_. In the case of a SPA, the server won't find
this file/directory because the routes are all generated on the client side.
This is why Firebase asked us if we wanted to configure as a single-page app.
Answering `yes` automatically set up some configuration that **instructs
Firebase to return `/index.hmtl` _no matter what route the client requests_**:

```json
{
  "hosting": {
    "public": "build",
    "ignore": ["firebase.json", "**/.*", "**/node_modules/**"],
    "rewrites": [
      {
        "source": "**",
        "destination": "/index.html"
      }
    ]
  }
}
```

**Not all providers do this configuration on your behalf**! For those that do
_not_, you must _do it manually_, and set up some redirection rule that routes
all requests to the index route.
