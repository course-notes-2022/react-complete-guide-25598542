# Deployment Steps

So how do you deploy a React app? There are a few steps involved:

1. Write and **test your code**: Manually and with automated tests

2. **Optimize code**: Optimize user experience and performance (especially
   **lazy-loading**)

3. **Build App** for production: Run build process to parse, transform and
   optimize code

4. **Upload App**: Upload production code to hosting server

5. **Configure Server**: Ensure app is served securely and as intended
