# Building the Code for Production

Let's now build our app for production. The `build` step is required because the
code we **build** is optimized for **developer experience**; it's
human-readable, and contains code like `JSX` that is not understood by browsers.
The code we'll build and **upload** to the server must be optimized for
**production**.

To build our app, we run the `npm run build` command (in `create-react-app`).
This will create the `build` folder that contains the content we must deploy to
the server. Inside the `build folder` we'll find:

- The optimized JS file with the main chunk that contains all the code we wrote,
  plus **all the 3rd-party package code** our application is using, including
  React

- The lazy-loaded chunks that we created in the previous steps.

We must deploy the **entire contents of the `build` folder**. We'll learn to do
that in the next lesson.
