# Intro

Up to now, we've built and tested our React apps **locally**. Ultimately, we
want to expose our apps to users all over the world by pushing them to a **real
deployment server**. In this module, we'll learn how to **deploy our React
apps**. We'll learn:

- Deployment **Steps and Pitfalls**
- **Server-side** Routing vs. **Client-side** Routing
