# Understanding Effect Dependencies

**Effect dependencies** are simply `prop` or `state` values that are used
**inside `useEffect`**.

The function passed to `useEffect` should run _if_ one of its dependencies
change. If the dependency array is _empty_ the function will run only once. Now,
in the `Modal` component we want to run the function whenever the `open` prop
changes.

Refactor `Modal` as follows:

```jsx
import { useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';

function Modal({ open, children }) {
  const dialog = useRef();

  useEffect(() => {
    if (open) {
      dialog.current.showModal();
    } else {
      dialog.current.close();
    }
  }, [open]);

  return createPortal(
    <dialog className="modal" ref={dialog}>
      {children}
    </dialog>,
    document.getElementById('modal')
  );
}

export default Modal;
```

Note that our modal works as before, but with `useEffect` we were able to make
our code a bit **leaner**.
