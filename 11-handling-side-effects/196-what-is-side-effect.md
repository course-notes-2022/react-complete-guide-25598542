# What is a "Side Effect"?

**Side effects** are tasks that don't inpact the current component **render
cycle**. A task that must be performed but does not directly impact the current
component's render cycle.

For example, imagine we want to sort the place cards by distance to the user's
location. `loc.js` contains a function that allows us to calculate the distance
between two points on the Earth, and a function that sorts a list by distance to
the user's location. This implies one important thing: **we need to get the
user's location**. Thankfully, there is a built-in browser utility for doing
this.

We probably want to get the user's location as soon as possible in the
application; `App.js` is probably a good place to do so. We can use the
`navigator` object, an object exposed by the browser to our JS code. This global
object has a `geolocation.getCurrentPosition()` method to get the user's
location. If the user **grants permission**, we can fetch the user's location.
`getCurrentPosition` takes a **callback function** once the user's location has
been fetched. Therefore the code that **depends on the location** should be
**inside the callback**, because the callback will be executed once the location
is available.

Add the following to `App.js`:

```js
import { useRef, useState } from 'react';

import Places from './components/Places.jsx';
import { AVAILABLE_PLACES } from './data.js';
import Modal from './components/Modal.jsx';
import DeleteConfirmation from './components/DeleteConfirmation.jsx';
import logoImg from './assets/logo.png';

function App() {
  const modal = useRef();
  const selectedPlace = useRef();
  const [pickedPlaces, setPickedPlaces] = useState([]);

  // Use `geolocation.getCurrentPosition`
  // to get the user's location and sort
  // the places by distance to location
  navigator.geolocation.getCurrentPosition((position) => {
    const {
      coords: { latitude, longitude },
    } = position;
    const sortedPlaces = sortPlacesByDistance(
      AVAILABLE_PLACES,
      latitude,
      longitude
    );
  });

  // Omitted...
}
```

This is an **example of a side effect**. The code above is **required to achieve
our desired functionality**, but **is not directly related the main goal of the
component function**, which is to render the `App` component. Note also that the
code does not finish right away, but at some point in the future after the
component is rendered.
