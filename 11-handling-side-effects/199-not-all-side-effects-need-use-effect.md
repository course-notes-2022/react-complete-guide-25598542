# Not All Side Effects Need `useEffect`

Note: **not all side-effects _require_ `useEffect`**! Remember that using
`useEffect` creates an **extra execution cycle**. Doing so unnecessarily can
impact application performance and is considered an **anti-pattern**.

Imagine that we want to store a list of place IDs in our browser's
`localStorage`. Consider the following updates to the `handleSelectPlace`
function:

```js
function handleSelectPlace(id) {
  setPickedPlaces((prevPickedPlaces) => {
    if (prevPickedPlaces.some((place) => place.id === id)) {
      return prevPickedPlaces;
    }
    const place = AVAILABLE_PLACES.find((place) => place.id === id);
    return [place, ...prevPickedPlaces];
  });

  // Another side-effect example; unrelated to component function
  // execution
  const storedIds = JSON.parse(localStorage.getItem('selectedPlaces')) || [];
  if (storedIds.indexOf(id) === -1) {
    localStorage.setItem('selectedPlaces', JSON.stringify([id, ...storedIds]));
  }
}
```

We don't **need** `useEffect` here because this code is **not updating state**.
Even if we _were_ the code in `handleSelectPlace` executes **only when the user
clicks on a place**, _not_ when the component function re-executes.

> Use `useEffect` to **avoid infinite loops**, or when running code that can run
> **only when the component function has executed at least once**!
