# Using `useEffect` for Syncing With Browser APIs

How can we use `useEffect` to make sure that the dialog is shown or hidden based
on the value of the `open` prop? We could use an `if` conditional statement:

```jsx
import { useRef } from 'react';
import { createPortal } from 'react-dom';

function Modal({ open, children }) {
  const dialog = useRef();

  if (open) {
    dialog.current.showModal();
  } else {
    dialog.current.close();
  }

  return createPortal(
    <dialog className="modal" ref={dialog}>
      {children}
    </dialog>,
    document.getElementById('modal')
  );
}

export default Modal;
```

If we try to run this code and **reload** the browser window, we get the
following error:

![dialog.current undefined error](./screenshots/dialog-current-undefined.png)

The issue is that we're calling the `showModal` and `close` methods on the
`dialog.current` ref **directly in the component function**, but _before the ref
has been rendered_. Thus the **first time the component function executes**,
`dialog` is not yet connected to the `<dialog>` element when `showModal` and
`current` are called!

This is _another use case for `useEffect()`: **syncing a value (`open` in this
case) to a DOM API**_. As we learned, `useEffect` is executed _after the
component function has completed_.

Refactor `Modal.jsx` as follows:

```jsx
import { useEffect, useRef } from 'react';
import { createPortal } from 'react-dom';

function Modal({ open, children }) {
  const dialog = useRef();

  useEffect(() => {
    if (open) {
      dialog.current.showModal();
    } else {
      dialog.current.close();
    }
  }, []); // We haven't added the dependencies, and our IDE will alert us to an error!

  return createPortal(
    <dialog className="modal" ref={dialog}>
      {children}
    </dialog>,
    document.getElementById('modal')
  );
}

export default Modal;
```

Note the comment above regarding missing dependencies. We'll take a look at
**dependencies** in the next lesson.
