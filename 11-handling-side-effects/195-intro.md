# Module Intro

In this module, we'll work on a "place picker" application that allows us to
select and/or delete places. While enhancing this application, we'll learn how
to deal with **side effects** in React apps. We'll learn:

- Understanding side effects and `useEffect()`
- Effects and **dependencies**
- When **not** to use `useEffect()`

Download the starter project and install all dependencies. Run the project with
the `npm run dev` command. It contains several components that use features that
we've already seen in earlier course sections. In the end, it's about managing
an array of places.
