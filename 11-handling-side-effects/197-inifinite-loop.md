# A Potential Problem with Side Effects: An Infinite Loop

Let's look at another potential problem of side effects. Now that we have the
user's location and the sorted places, we want to use the places in our
component. The sorted places aren't available at the outset, therefore we need
**state** that manages the available places:

```js
import { useRef, useState } from 'react';

import Places from './components/Places.jsx';
import { AVAILABLE_PLACES } from './data.js';
import Modal from './components/Modal.jsx';
import DeleteConfirmation from './components/DeleteConfirmation.jsx';
import logoImg from './assets/logo.png';

function App() {
  const modal = useRef();
  const selectedPlace = useRef();
  const [pickedPlaces, setPickedPlaces] = useState([]);
  const [availablePlaces, setAvailablePlaces] = useState([]); // Create available places state

  navigator.geolocation.getCurrentPosition((position) => {
    const {
      coords: { latitude, longitude },
    } = position;
    const sortedPlaces = sortPlacesByDistance(
      AVAILABLE_PLACES,
      latitude,
      longitude
    );
    setAvailablePlaces(sortedPlaces); // update available places state
  });

  function handleStartRemovePlace(id) {
    modal.current.open();
    selectedPlace.current = id;
  }

  function handleStopRemovePlace() {
    modal.current.close();
  }

  function handleSelectPlace(id) {
    setPickedPlaces((prevPickedPlaces) => {
      if (prevPickedPlaces.some((place) => place.id === id)) {
        return prevPickedPlaces;
      }
      const place = AVAILABLE_PLACES.find((place) => place.id === id);
      return [place, ...prevPickedPlaces];
    });
  }

  function handleRemovePlace() {
    setPickedPlaces((prevPickedPlaces) =>
      prevPickedPlaces.filter((place) => place.id !== selectedPlace.current)
    );
    modal.current.close();
  }

  return (
    <>
      <Modal ref={modal}>
        <DeleteConfirmation
          onCancel={handleStopRemovePlace}
          onConfirm={handleRemovePlace}
        />
      </Modal>

      <header>
        <img src={logoImg} alt="Stylized globe" />
        <h1>PlacePicker</h1>
        <p>
          Create your personal collection of places you would like to visit or
          you have visited.
        </p>
      </header>
      <main>
        <Places
          title="I'd like to visit ..."
          fallbackText={'Select the places you would like to visit below.'}
          places={pickedPlaces}
          onSelectPlace={handleStartRemovePlace}
        />
        <Places
          title="Available Places"
          places={availablePlaces}
          onSelectPlace={handleSelectPlace}
        />
      </main>
    </>
  );
}

export default App;
```

Note the following:

1. We create a new piece of state, `availablePlaces`, with the `useState` hook.

2. We set the state to `sortedPlaces` once they are available.

3. Setting the state triggers a re-render cycle.

4. We pass the `availablePlaces` state to the `Places` component as props.

Seems like a good solution? It's not! This solution causes an **infinite loop**!

Why? Updating the state in step 2 causes React to **re-execute** the component
function. Re-execution causes the component to fetch the location again.
Fetching the location sets the state. Setting the state causes the component
function to re-execute. Re-execution causes the component to fetch the location
_again_...

This is an example of a side effect causing a problem. That's where `useEffect`
comes in!
