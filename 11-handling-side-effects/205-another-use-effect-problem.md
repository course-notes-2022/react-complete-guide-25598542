# Preparing Another Problem That Can Be Fixed with `useEffect`

Besides the dependencies array, `useEffect` has another important feature.

Open the `DeleteConfirmation.jsx` file. This component is responsible for
rendering the content of the modal. We want to add a feature to **automatically
close the modal and delete the place** after 3 seconds. We can achieve this with
the help of the `setTimeout` function built into the browser.

Refactor the `DeleteConfirmation` code as follows:

```jsx
export default function DeleteConfirmation({ onConfirm, onCancel }) {
  setTimeout(() => {
    onConfirm();
  }, 3000);

  return (
    <div id="delete-confirmation">
      <h2>Are you sure?</h2>
      <p>Do you really want to remove this place?</p>
      <div id="confirmation-actions">
        <button onClick={onCancel} className="button-text">
          No
        </button>
        <button onClick={onConfirm} className="button">
          Yes
        </button>
      </div>
    </div>
  );
}
```

If we do this, we create a problem. The `DeleteConfirmation` code is **always
part of the DOM**, but not always visible. Therefore the timer will be set
immediately when the `App` component is rendered. Also, the timer is never
**stopped** once the component is no longer visible in the DOM!
