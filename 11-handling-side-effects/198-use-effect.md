# using `useEffect` for Handling (Some) Side Effects

We can use `useEffect` to handle side effects in a better way. We import it from
`react` and use it **inside our component function**. `useEffect` takes two
arguments:

- a **function** that wraps your side-effect producing code

- an **array of dependencies** of that effect function

Add the following to `App.js`:

```jsx
import { useRef, useState, useEffect } from 'react';

import Places from './components/Places.jsx';
import { AVAILABLE_PLACES } from './data.js';
import { sortPlacesByDistance } from './loc.js';
import Modal from './components/Modal.jsx';
import DeleteConfirmation from './components/DeleteConfirmation.jsx';
import logoImg from './assets/logo.png';

function App() {
  const modal = useRef();
  const selectedPlace = useRef();
  const [pickedPlaces, setPickedPlaces] = useState([]);
  const [availablePlaces, setAvailablePlaces] = useState([]);

  // Wrap location-fetching and state-setting code in `useEffect()`
  useEffect(() => {
    navigator.geolocation.getCurrentPosition((position) => {
      const {
        coords: { latitude, longitude },
      } = position;
      const sortedPlaces = sortPlacesByDistance(
        AVAILABLE_PLACES,
        latitude,
        longitude
      );
      setAvailablePlaces(sortedPlaces);
    });
  }, []);

  // Omitted...
}
```

If we do this, _we will avoid the infinite re-render issues_ we saw in the
previous lecture!

## How Does `useEffect` Work?

The function that is passed as the first argument to `useEffect` is guaranteed
to be executed **after** the **component function executes**, and thereafter
**only if the dependencies in the dependencies array change**.

Note that you **must** include the dependenices array argument! Omitting it will
create the same infinite loop problem we saw previously!

Save the changes to `App.jsx` and refresh. You should see a pop-up dialog asking
permission to access your location, and the available places should appear
shortly thereafter:

![available places useEffect](./screenshots/available-places-use-effect.png)
