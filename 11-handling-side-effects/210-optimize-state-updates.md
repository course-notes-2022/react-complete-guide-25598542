# Optimizing State Updates

At the moment, we are managing the interval in `DeleteConfirmation`. This means
that the entire `DeleteConfirmation` component function must be re-executed
every 10ms. We can optimize this by refactoring the progress bar to a
**separate, smaller** component.

Create a new file, `src/components/ProgressBar.jsx`, and add the following code:

```jsx
import { useState, useEffect } from 'react';

const ProgressBar = ({ timer }) => {
  const [remainingTime, setRemainingTime] = useState(timer);

  useEffect(() => {
    const interval = setInterval(() => {
      console.log('INTERVAL');
      setRemainingTime((prevTime) => prevTime - 10);
    }, 10);

    return () => {
      clearInterval(interval);
    };
  }, []);

  return <progress value={remainingTime} max={timer} />;
};

export default ProgressBar;
```

Refactor `DeleteConfirmation` to use the new `ProgressBar` component:

```jsx
import { useEffect } from 'react';
import ProgressBar from './ProgressBar';

export const TIMER = 3000;

export default function DeleteConfirmation({ onConfirm, onCancel }) {
  useEffect(() => {
    console.log('TIMER SET');
    const timer = setTimeout(() => {
      onConfirm();
    }, TIMER);

    return () => {
      console.log('Cleaning up timer');
      clearTimeout(timer);
    };
  }, [onConfirm]);

  return (
    <div id="delete-confirmation">
      <h2>Are you sure?</h2>
      <p>Do you really want to remove this place?</p>
      <div id="confirmation-actions">
        <button onClick={onCancel} className="button-text">
          No
        </button>
        <button onClick={onConfirm} className="button">
          Yes
        </button>
      </div>
      <ProgressBar timer={TIMER} />
    </div>
  );
}
```
