# The Problem with Object and Function Dependencies

Let's take a closer look at the `onConfirm` prop in `DeleteConfirmation`:

```jsx
import { useEffect } from 'react';
import ProgressBar from './ProgressBar';

export const TIMER = 3000;

// Receiving the `onConfirm` prop
export default function DeleteConfirmation({ onConfirm, onCancel }) {
  useEffect(() => {
    console.log('TIMER SET');
    const timer = setTimeout(() => {
      onConfirm();
    }, TIMER);

    return () => {
      console.log('Cleaning up timer');
      clearTimeout(timer);
    };
  }, [onConfirm]); // adding `onConfirm` to `useEffect` dependencies array

  return (
    <div id="delete-confirmation">
      <h2>Are you sure?</h2>
      <p>Do you really want to remove this place?</p>
      <div id="confirmation-actions">
        <button onClick={onCancel} className="button-text">
          No
        </button>
        <button onClick={onConfirm} className="button">
          Yes
        </button>
      </div>
      <ProgressBar timer={TIMER} />
    </div>
  );
}
```

If we're using `prop` or `state` values in our `useEffect` function, we should
add them as dependencies to our dependency array.

There is a problem we should be aware of when adding **functions** as
dependencies. There is a danger of creating an infinite loop.

When we add dependencies, we're telling React to re-execute the callback
function passed to `useEffect` when the dependency changes. When the dependency
is a **function**, this becomes trickier. Although the **code** in a function
definition doesn't change, functions are **objects** and are **recreated every
time the component function that contains the function definition is
re-executed**. As objects, when a function is recreated it **points to a
different location in memory** and is **not equal to the previous function
object**, even though they have the same code. This can lead to the same
infinite loop problem we saw before.

A safer way to avoid this problem is to use a special React hook that we'll look
at in the next lecture.
