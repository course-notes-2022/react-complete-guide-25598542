# Introducing `useEffect()`'s Cleanup Function

To be clear, our problem is not **setting the timer**, but **cleaning it up**
when the component is removed from the DOM.

`useEffect` allows us to define and return a **cleanup function** that will be
executed by React in the following situations:

1. Before the function runs **again**

2. After the component is **removed from the DOM**

Refactor `DeleteConfirmation` as follows:

```jsx
import { useEffect } from 'react';

export default function DeleteConfirmation({ onConfirm, onCancel }) {
  useEffect(() => {
    const timer = setTimeout(() => {
      onConfirm();
    }, 3000);

    return () => {
      clearTimeout(timer);
    };
  }, []);

  return (
    <div id="delete-confirmation">
      <h2>Are you sure?</h2>
      <p>Do you really want to remove this place?</p>
      <div id="confirmation-actions">
        <button onClick={onCancel} className="button-text">
          No
        </button>
        <button onClick={onConfirm} className="button">
          Yes
        </button>
      </div>
    </div>
  );
}
```

Note that the cleanup function does _not_ run on _initial execution_ of the
component function.
