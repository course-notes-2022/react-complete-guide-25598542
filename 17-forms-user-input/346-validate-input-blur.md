# Validating Input on Blur

Let's look at how to validate a form input on `blur`.

```jsx
import { useState } from 'react';

export default function Login() {
  const [enteredValues, setEnteredValues] = useState({
    email: '',
    password: '',
  });

  // Create a state value to track whether each input has been edited (similar to "touched")
  const [didEdit, setDidEdit] = useState({
    email: false,
    password: false,
  });

  // Use the new `didEdit` property to avoid displaying the error message on initial load
  const emailIsInvalid = didEdit.email && !enteredValues.email.includes('@');

  function handleSubmit(e) {
    e.preventDefault();
    console.log('Values: ', enteredValues);
  }

  function handleInputChange(identifier, event) {
    setEnteredValues((prevValues) => {
      return {
        ...prevValues,
        [identifier]: event.target.value,
      };
    });
  }

  function handleInputBlur(identifier) {
    setDidEdit((prevEdit) => ({
      ...prevEdit,
      [identifier]: true,
    }));
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input
            onBlur={() => {
              handleInputBlur('email');
            }}
            onChange={(e) => {
              handleInputChange('email', e);
            }}
            id="email"
            type="email"
            name="email"
            value={enteredValues.email}
          />
          {
            // Error message displays only when form field has been touched and blurred, and email does not contain an `@`
          }
          <div className="control-error">
            {emailIsInvalid && <p>Please enter a valid email address.</p>}
          </div>
        </div>

        {
          // password input omitted...
        }
      </div>

      {
        // submit/reset button omitted...
      }
    </form>
  );
}
```

This is arguably a better experience, but note that once the error message is
_displayed_, it **continues to display** until the user enters a valid email
with an `@`. We _may_ want to _hide the message again_ as soon as the user
starts typing **until the input loses focus again**. We could accomplish this by
updating `didEdit` on every keystroke and **resetting it to false** `onChange`.
We can do this easily by modifying the `handleInputChange` function:

```jsx
function handleInputChange(identifier, event) {
  setEnteredValues((prevValues) => {
    return {
      ...prevValues,
      [identifier]: event.target.value,
    };
  });
  // Set the input's edit status to false on input change
  setDidEdit((prevEdit) => {
    return {
      ...prevEdit,
      [identifier]: false,
    };
  });
}
```

With this, we have **comgined** validation on **keystroke _and_ validation**.
