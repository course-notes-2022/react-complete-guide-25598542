# Validate Input on Form Submission

Another way to validate form input is **on form submission**. We can switch back
to the ref-based `Login` component to see how.

We can now keep track of our `emailIsValid` property _inside_ our submit handler
function, since with a ref-based form we _typically_ want to validate form
fields on submission<sup>1</sup>. Make the following updates to `Login.jsx`:

```jsx
import { useRef, useState } from 'react';

export default function Login() {
  const [emailIsInvalid, setEmailIsInvalid] = useState(false);
  const emailRef = useRef();
  const passwordRef = useRef();

  function handleSubmit(e) {
    e.preventDefault();
    const enteredEmail = emailRef.current.value;
    const enteredPassword = passwordRef.current.value;

    // Create the `emailIsValid` property
    const emailIsValid = enteredEmail.includes('@');

    if (!emailIsValid) {
      setEmailIsInvalid(true); // Update the `emailIsInvalid` state to false if invalid, and return to prevent further evaluation
      return;
    }

    setEmailIsInvalid(false); // Email is valid; update `emailIsInvalid` state
    console.log('sending http request...');
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input id="email" type="email" name="email" ref={emailRef} />
          <div className="control-error">
            {
              // Display error message if email is invalid
            }
            {emailIsInvalid && <p>Please enter a valid email address.</p>}
          </div>
        </div>

        <div className="control no-margin">
          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            name="password"
            ref={passwordRef}
          />
        </div>
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button" onClick={handleSubmit}>
          Login
        </button>
      </p>
    </form>
  );
}
```

In the end, it's up to us as developers to decide what experience we want to
provide to our users. We may want to provide validation on **both** blur **and**
submission. For example, if we only rely on keystroke and blur validation, we
give the user a nice experience while **editing** the form, but we give no
output that the form is invalid on **submission**. It's **always** a good idea
to validate our forms **on submit**, even if we're already validating user input
on every keystroke.

<sup>1</sup>Note that we don't _have_ to validate ref-based forms on submission
_only_. We can validate each field on change as we did with state-based forms.
It's just less efficient and requires more code to accomplish.
