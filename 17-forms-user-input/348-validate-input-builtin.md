# Validating Input vis Built-in Validation Props

We can validate input on form submission by adding our own code, but we can
_also do it in an even easier way_ using the browser's **built-in** validation
properties.

Refactor the `Signup.jsx` component to add the `required` attribute to the
`email` input field:

```html
<div className="control">
  <label htmlFor="email">Email</label>
  <input id="email" type="email" name="email" required />
</div>
```

Refactor the `App` component to use the `Signup` component again. Attempt to
submit the form with an empty `email` input, and note the following output in
the browser:

![email required](./screenshots/email-required.png)

The error output is generated **by the browser**, without any code written by
us!
