# Validating Input on Every Keystroke via State

Let's begin to look at how we can **validate our user's input**.

As we discussed earlier, we have several options for input validation in our
forms. In this section, we'll discuss validating input on **keystroke**.

If we want to validate input on **keystroke**, we need to **listen to the
keystroke event**. That is what we're doing when we manage our form input via
state. When using `ref`s, we only have access to the data **once the form is
submitted**. Therefore if keystroke validation is a requirement, we need a
**stateful** form.

Let's refactor the `App` component to use the `StateLogin` form. Update
`StateLogin` as follows:

```jsx
import { useState } from 'react';

export default function Login() {
  const [enteredValues, setEnteredValues] = useState({
    email: '',
    password: '',
  });

  // Create the `emailIsInvalid` boolean value
  const emailIsInvalid = !enteredValues.email.includes('@');

  function handleSubmit(e) {
    e.preventDefault();
    console.log('Values: ', enteredValues);
  }

  function handleInputChange(identifier, event) {
    setEnteredValues((prevValues) => {
      return {
        ...prevValues,
        [identifier]: event.target.value,
      };
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input
            onChange={(e) => {
              handleInputChange('email', e);
            }}
            id="email"
            type="email"
            name="email"
            value={enteredValues.email}
          />
          {
            // Add the div with the class name 'control-error' below the email input field
          }
          <div className="control-error">
            {emailIsInvalid && <p>Please enter a valid email address.</p>}
          </div>
        </div>

        <div className="control no-margin">
          <label htmlFor="password">Password</label>
          <input
            onChange={(e) => {
              handleInputChange('password', e);
            }}
            id="password"
            type="password"
            name="password"
          />
        </div>
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button" onClick={handleSubmit}>
          Login
        </button>
      </p>
    </form>
  );
}
```

Refresh the browser. Note that we now have an error message appearing below the
email input field whenever the field does not contain an `@` symbol.

Note also that this message appears on **initial render**, which may not be the
experience we want for our users. Therefore, we might want to make sure that
`emailIsInvalid` is true only if there is a value in the field

```jsx
const emailIsInvlid =
  enteredValues.email !== '' && !enteredValues.email.includes('@');
```

This hides the message on initial render (when there is no content in the
input), but then displays it as soon as the user begins typing _until the user
enters an `@` symbol_.

This approach still has problems, however. If the user enters a value and _then
deletes the entire value_, we will not show the error message because the check
`entereedValues.email !== ''` now **fails**. A second problem is that we see an
error message as soon as the user starts typing, so as mentioned we **show the
error message too early**. Let's look at validating on **blur** can help with
this.
