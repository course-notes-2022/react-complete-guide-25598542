# Getting User Input via Refs

Another way to get access to our form field values is to use `ref`s. We'll learn
about that approach in this lesson.

To start, let's remove the previous state-management code from 'Login.jsx'. We
can then refactor it to make use of `ref`s:

```jsx
import { useRef } from 'react';

export default function Login() {
  const emailRef = useRef();
  const passwordRef = useRef();

  function handleSubmit(e) {
    e.preventDefault();
    const enteredEmail = emailRef.current.value;
    const enteredPassword = passwordRef.current.value;
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input
            id="email"
            type="email"
            name="email"
            value={enteredValues.email}
            ref={emailRef}
          />
        </div>

        <div className="control no-margin">
          <label htmlFor="password">Password</label>
          <input
            id="password"
            type="password"
            name="password"
            ref={passwordRef}
          />
        </div>
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button" onClick={handleSubmit}>
          Login
        </button>
      </p>
    </form>
  );
}
```

Recall that `ref`s give us a **reference** to a DOM element. Since we're
capturing refs to **input** fields, we can access the input field's `value`
property on `ref.current.value`.

This approach typically requires **less code** than when using state; all we had
to do is create the `ref`s, connect them with the `ref` props and extract the
value. The **downside** is that resetting the values is more difficult because
it is **not recommended to _set_ the DOM using refs**.
