# Build and Use a Reusable Input Component

Now that we've learned different ways of extracting and validating form inputs,
let's look at how to create a more **reusable** input component.

Our `Login` component has the most complex validation logic so far. We have
quite a bit of code, and quite a bit of _repeated_ code for just two inputs.
This is a great use case for a **custom reusable component**.

Create a new component file, `Input.jsx` and add the following:

```jsx
export default function Input({ label, id, error, ...props }) {
  return (
    <div className="control no-margin">
      <label htmlFor={id}>{label}</label>
      <input onChange={handleEmailChange} {...props} id={id} />
      <div className="control-error">{error && <p>{error}</p>}</div>
    </div>
  );
}
```

Note the use of the `...props` spread operator passed to the `Input` function.
This contains all the _additional_ props passed to the component _not explicitly
destructured_. We can use this property on the input to **destructure** all
those props with the line:

> `<input {...props}/>

We can now refactor our `StateLogin` component to use the new `Input` component:

```jsx
return (
  <form onSubmit={handleSubmit}>
    <h2>Login</h2>

    <div className="control-row">
      <Input
        label="Email"
        type="email"
        name="email"
        id="email"
        onBlur={() => {
          handleInputBlur('email');
        }}
        onChange={(e) => {
          handleInputChange('email', e);
        }}
        value={enteredValues.email}
      />
    </div>
    <div className="control no-margin">
      <Input
        label="Password"
        type="password"
        name="password"
        id="password"
        onBlur={() => {
          handleInputBlur('password');
        }}
        onChange={(e) => {
          handleInputChange('password', e);
        }}
        value={enteredValues.password}
      />
    </div>

    <p className="form-actions">
      <button className="button button-flat">Reset</button>
      <button className="button" onClick={handleSubmit}>
        Login
      </button>
    </p>
  </form>
);
```

See below for the complet updated `StateLogin` component:

```jsx
import { useState } from 'react';
import Input from './Input';

export default function Login() {
  const [enteredValues, setEnteredValues] = useState({
    email: '',
    password: '',
  });

  const [didEdit, setDidEdit] = useState({
    email: false,
    password: false,
  });

  const emailIsInvalid = didEdit.email && !enteredValues.email.includes('@');
  const passwordIsInvalid =
    didEdit.password && !enteredValues.password.trim().length < 6;

  function handleSubmit(e) {
    e.preventDefault();
    console.log('Values: ', enteredValues);
  }

  function handleInputChange(identifier, event) {
    setEnteredValues((prevValues) => {
      return {
        ...prevValues,
        [identifier]: event.target.value,
      };
    });
    setDidEdit((prevEdit) => {
      return {
        ...prevEdit,
        [identifier]: false,
      };
    });
  }

  function handleInputBlur(identifier) {
    setDidEdit((prevEdit) => ({
      ...prevEdit,
      [identifier]: true,
    }));
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Login</h2>

      <div className="control-row">
        <Input
          label="Email"
          type="email"
          name="email"
          id="email"
          onBlur={() => {
            handleInputBlur('email');
          }}
          onChange={(e) => {
            handleInputChange('email', e);
          }}
          value={enteredValues.email}
          error={emailIsInvalid && 'Please enter a valid email.'}
        />
      </div>
      <div className="control no-margin">
        <Input
          label="Password"
          type="password"
          name="password"
          id="password"
          onBlur={() => {
            handleInputBlur('password');
          }}
          onChange={(e) => {
            handleInputChange('password', e);
          }}
          value={enteredValues.password}
          error={passwordIsInvalid && 'Please enter a valid password.'}
        />
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button" onClick={handleSubmit}>
          Login
        </button>
      </p>
    </form>
  );
}
```
