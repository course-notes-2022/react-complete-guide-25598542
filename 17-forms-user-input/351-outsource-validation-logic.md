# Outsourcing Validation Logic

Reusing code doesn't stop at the custom component level! We can also outsource
our **validation logic** to reusable functions/services that we can leverage in
**multiple components**.

The `util` folder contains a `validation.js` file with a couple of sample
validation functions we can leverage in our project. We can use these functions
in place of the previous hard-coded checks to validate our `email` and
`password` input in `StateLogin`:

```jsx
import { useState } from 'react';
import Input from './Input';
import { isEmail, isNotEmpty, hasMinLength } from '../util/validation';

export default function Login() {
  const [enteredValues, setEnteredValues] = useState({
    email: '',
    password: '',
  });

  const [didEdit, setDidEdit] = useState({
    email: false,
    password: false,
  });

  const emailIsInvalid =
    didEdit.email &&
    !isEmail(enteredValues.email) &&
    !isNotEmpty(enteredValues.email);
  const passwordIsInvalid =
    didEdit.password && !hasMinLength(enteredValues.password, 6);

  // omitted...
}
```
