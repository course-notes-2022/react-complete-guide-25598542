# What's So Difficult About Forms?

A **form** is a collection of **input fields**. Typically the fields are used in
conjunction with `labels`, and are wrapped by the `form` element.

We want to do two things with forms:

- Handle **form submission**, and
- **Validate user input**

| Form Submission                                 | Input Validation                                                         |
| ----------------------------------------------- | ------------------------------------------------------------------------ |
| Handling submission is relatively `easy`        | Providing a good user experience is tricky                               |
| Entered values can be managed via `state`       | You can `validate` on every `keystroke`; errors may be shown `too early` |
| Alternatively, they can be extracted via `refs` | You can validate on `lost focus`; errors may be hown `too long`          |
| Or via `FormData` and native browser features   | You can validate on `form submission`; errors may be shown `too late`    |
