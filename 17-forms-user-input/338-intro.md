# Intro and Starting Project

At some point in time, you'll need to deal with **user input** in your
applications. Working with forms is trickier than it may seem initially, but
we'll learn:

- What's difficult about forms
- Handling form submission and validation
- Using built-in form features
- Building custom solutions

## Starting Project

[Download the starting project zip file from the Github repo](https://github.com/academind/react-complete-guide-course-resources/blob/main/attachments/17%20Forms%20User%20Input/01-starting-project.zip)
and run `npm install` to get the sample project for this chapter started.
