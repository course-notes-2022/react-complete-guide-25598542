# Managing & Getting User Input via State & Generic Handlers

Now that we've ensured our form can be submitted, let's learn how to **extract
the user-entered data** from our form. There are multiple ways to achieve this.

## Using the `useState` Hook

We can utilize `useState` to keep track of our form state, either as:

- **Multiple** state pieces representing each **input**
- An **object** representing the **entire form**

Refactor `Login.jsx` as follows:

```jsx
import { useState } from 'react';

export default function Login() {
  const [enteredEmail, setEnteredEmail] = useState('');
  const [enteredPassword, setEnteredPassword] = useState('');

  function handleSubmit(e) {
    e.preventDefault();
    console.log('User email: ', enteredEmail);
  }

  function handleEmailChange(event) {
    setEnteredEmail(event.target.value);
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input
            onChange={handleEmailChange}
            id="email"
            type="email"
            name="email"
            value={enteredEmail}
          />
        </div>

        <div className="control no-margin">
          <label htmlFor="password">Password</label>
          <input id="password" type="password" name="password" />
        </div>
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button" onClick={handleSubmit}>
          Login
        </button>
      </p>
    </form>
  );
}
```

Note that we've created two new state variables for the user-entered **email**
and **password**. We've also created a new function, `handleEmailChange` and
passed it as a handler to the email's `onChange` event. Finally, we've passed
the `enteredEmail` state value to the email input. Logging the value to the
console on submit produces the expected output.

Note that this _works_, but creating **separate handlers** for each piece of
state, and creating a **separate piece of state** for each input, can quickly
cause our code to become **bloated**.

One possible solution would be to create a more **generic** function for
handling input changes, and refactoring our form state to an **object**:

```jsx
import { useState } from 'react';

export default function Login() {
    // Set up initial state as an OBJECT
  const [enteredValues, setEnteredValues] = useState({
    email: '',
    password: '',
  });

  function handleSubmit(e) {
    e.preventDefault();
    console.log('User email: ', enteredEmail);
  }

  function handleInputChange(identifier, event) {
    setEnteredValues((prevValues) => { // Pass a function to get access to previous state
      return {
        ...prevValues,
        [identifier]: event.target.value,
      };
    });
  }

  return (
    <form onSubmit={handleSubmit}>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input
          {
            /*
            We now need to pass an anonymous function to pass to the `onChange` handler to call `handleInputChange` with the identifier
             */
          }
            onChange={(e) => {handleInputChange('email', e)}}
            id="email"
            type="email"
            name="email"
            value={enteredValues.email}
          />
        </div>

        <div className="control no-margin">
          <label htmlFor="password">Password</label>
          <input id="password" type="password" name="password" />
        </div>
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button" onClick={handleSubmit}>
          Login
        </button>
      </p>
    </form>
  );
}
```

This allows us to get **rid of** the separate handling functions in the previous
solution.
