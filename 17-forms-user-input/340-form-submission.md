# Handling Form Submission

Let's start by exploring how to handle **form submission**. We'll start with the
beginning state of the `Login` component:

```jsx
export default function Login() {
  return (
    <form>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input id="email" type="email" name="email" />
        </div>

        <div className="control no-margin">
          <label htmlFor="password">Password</label>
          <input id="password" type="password" name="password" />
        </div>
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button">Login</button>
      </p>
    </form>
  );
}
```

To handle form submission, we want to listen to the `click` event on the
**Login** button. Let's add a new function to listen to the event:

```jsx
export default function Login() {
  function handleSubmit() {
    console.log('submitted');
  }
  return (
    <form>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input id="email" type="email" name="email" />
        </div>

        <div className="control no-margin">
          <label htmlFor="password">Password</label>
          <input id="password" type="password" name="password" />
        </div>
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button" type="button" onClick={handleSubmit}>
          Login
        </button>
      </p>
    </form>
  );
}
```

**Note that we _must_ add the `type="button"` attribute to the button**. The
**default** browser behavior is to create a `button` element in a `form` as a
`type=submit`. This creates a **new request** to the server that is serving the
application.

**Another way** is to **keep** the `type=submit` attribute on the _button_ and
add an **onSubmit** listener to the **form**:

```jsx
export default function Login() {
  function handleSubmit(e) {
    e.preventDefault();
    console.log('submitted');
  }
  return (
    <form onSubmit={handleSubmit}>
      <h2>Login</h2>

      <div className="control-row">
        <div className="control no-margin">
          <label htmlFor="email">Email</label>
          <input id="email" type="email" name="email" />
        </div>

        <div className="control no-margin">
          <label htmlFor="password">Password</label>
          <input id="password" type="password" name="password" />
        </div>
      </div>

      <p className="form-actions">
        <button className="button button-flat">Reset</button>
        <button className="button" onClick={handleSubmit}>
          Login
        </button>
      </p>
    </form>
  );
}
```

This is probably the more common approach.
