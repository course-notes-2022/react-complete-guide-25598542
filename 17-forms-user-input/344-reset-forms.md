# Resetting Forms

Let's now learn how to **reset** our form field values.

Note that we currently have a "Reset" button on our form, that **does** work
because the button `type` attribute to `reset`. We can also **programmatically**
reset our form.

If we're using **state** in our form, we can simply use the setter functions to
set the input values to `''` (or whatever the initial value was).

If we're using **refs**, we _can_ reset the inputs by _manually_ setting
`field.current.value = ''`, but this is not best practice because we're manually
updating the DOM. A **better way** would be to call the `event.target.reset()`
method (in your submit handler function, for example). This is also imperative
code, but it's less code to write than manually resetting all the `ref` values.
