# Creating a React + Typescript Project

Let's now dive into React & Typescript. We need to create a new project with a
development server and build process, but one that is configured to use
**Typescript**.

_Note: As of this writing, the React team no longer recommends
`create-react-app` for creating a new application in the official documentation.
We will use it here for learning purposes only_

Run the following command to use `create-react-app` to create a React project
with **built-in support for Typescript**:

> `npx create-react-app my-app --template typescript`

`cd` into the project directory, and start the project. We still have the same
starter project, but note that the file extensions are now `.tsx` because we are
using **Typescript** (with JSX).

Note that the `dev` server is now performing an extra step of compiling our
Typescript code into Javascript _for us_, so we don't need to compile our code
_manually_. This also happens when we _build_ our code using `npm run build`.

A note on **dependencies**:

```json
"dependencies": {
    "@testing-library/jest-dom": "^5.17.0",
    "@testing-library/react": "^13.4.0",
    "@testing-library/user-event": "^13.5.0",
    "@types/jest": "^27.5.2",
    "@types/node": "^16.18.68",
    "@types/react": "^18.2.45",
    "@types/react-dom": "^18.2.17",
    "react": "^18.2.0",
    "react-dom": "^18.2.0",
    "react-scripts": "5.0.1",
    "typescript": "^4.9.5",
    "web-vitals": "^2.1.4"
}
```

Observe that, in addition to Typescript, we have several `@types` dependencies
as well. `typescript` is the language/compiler that we need to be able to use
Typescript in this project. The `@types` packages act as **translation bridges**
between vanilla JS libraries and Typescript projects. For example, `react` and
`react-dom` work with Javascript _only_; in order to work with them in
Typescript (auto-completion, type-checking etc.), we need extra type
annotations.
