# Using Union Types

So far, all of our variables have taken a **single type**. Often, this is all we
need. Other times, we want to be able to store **multiple types** in a variable.

**Union types** allow us to use multiple different types when defining types for
our variables:

```ts
const course: string | string[];
```

We use the `|` syntax for defining a union type.
