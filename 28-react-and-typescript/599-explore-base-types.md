# Exploring the Base Types

Let's dig deeper into Typescript and explore the core basics. We'll start with
the basic types we can work with.

Create a new file, `basics.ts` with the following code:

```ts
// Primitives: number, string, boolean
// More complex types: arrays, objects
// Function types, parameters

// Primitives:

let age: number;
age = 12;

let userName: string; // Note that the data types are LOWERCASE for primitive types

userName = 'foo';

let isInstructor: boolean;

isInstructor = true;
```
