# What and Why?

**Typescript** is a **superset** of Javascript. It is a programming language
that builds upon and **extends** Javascript.

Unlike React, Typescript is not a Javascript **library** and adds **static
typing** to it. Javascript alone is **dynamically typed** Javascript already
_has_ data types, but it is not statically-typed like Java, for example, where
the data types of variables **must be specified at creation time**.

Consider the following function:

```js
function add(a, b) {
  return a + b;
}

console.log(add(2, 5)); // 7
console.log(add('a', 'b')); // 'ab'
```

`add` does not know what types of arguments it will receive when called, so it
does its best to try and "add" the passed arguments (whatever they are). In this
case, it will return the **sum** of two **integers**, however it will
**concatenate two strings**. For other types of arguments it may fail
altogether!

This is not ideal for us as programmers, because it makes our programs
**unpredicatable** and prone to **bugs**. We get no indication of what type of
parameters the function is expecting, and no alerts when we add the **wrong
types**.

This is where Typescript comes in!

With Typescript, we can do this:

```ts
function add(a: number, b: number) {
  return a + b;
}

const result = add('b', 'c'); // Typescript will complain about this, alerting us to the error!
```
