# Bonus: Exploring `tsconfig.json`

`tsconfig.json` is a file you can add to **any project** in which you're using
Typescript. It configures your Typescript compiler.

We have various options in this file. See the official documentation for
detailed explanations, but hovering over a property displays a link to the
documentation for that property.

Let's look at a few important properties:

```json
{
  "compilerOptions": {
    "target": "es5",
    "lib": ["dom", "dom.iterable", "esnext"],
    "allowJs": true,
    "skipLibCheck": true,
    "esModuleInterop": true,
    "allowSyntheticDefaultImports": true,
    "strict": true,
    "forceConsistentCasingInFileNames": true,
    "noFallthroughCasesInSwitch": true,
    "module": "esnext",
    "moduleResolution": "node",
    "resolveJsonModule": true,
    "isolatedModules": true,
    "noEmit": true,
    "jsx": "react-jsx"
  },
  "include": ["src"]
}
```

- `target`: Controls the target JS version to which your Typescript code will be
  transpiled

- `lib`: Typescript libraries that influence which kind of types Typescript
  recognizes:

  - `dom`: Ensures default DOM types are understood, e.g. `HTMLInputElement` is
    unlocked by `dom`

- `allowJs`: Allows us to import files from files with plain `.js` extensions

- `strict`: Sets "strict" settings when `true`, e.g. enforcing no implicit `any`
  type rule

In general, the default settings should work, and you should change them **only
if you know what you're doing**. However you **can** configure these settings if
you need to.
