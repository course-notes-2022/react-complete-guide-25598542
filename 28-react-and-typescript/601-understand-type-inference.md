# Type Inference

So far, we've declared a variable in **one step**, and assigned a value in a
**second step**.

Typescript can **infer the type of a variable** when it is initialized to a
value:

```ts
// Primitives: number, string, boolean
// More complex types: arrays, objects
// Function types, parameters

// Primitives:

let age: number;
age = 12;

let userName: string; // Note that the data types are LOWERCASE for primitive types

userName = 'foo';

let isInstructor: boolean;

isInstructor = true;

// Complex types

let hobbies: string[]; // store an ARRAY of strings in `hobbies`

hobbies = ['Sports', 'Cooking'];

let person: { name: string; age: number };

person = {
  name: 'Johnny Bravo',
  age: 32,
};

/*
We could do this:

let person: any;

person = {
    name: 'Johnny Bravo',
    age: 32
}

But the use of `any` as a type defeats
the purpose of Typescript, and should not
be used!
*/

let people: { name: string; age: number }[]; // Store an array of `person` objects

let course = 'React - The Complete Guide'; // Typescript infers that `course` is a string type
course = 2; // NO
```

It is recommended to use type inference when possible, to avoid redundancies in
your code.
