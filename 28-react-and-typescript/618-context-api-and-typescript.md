# The Context API and Typescript

Let's wrap-up by exploring the Context API with Typescript.

Add a new folder, `store`, with a new file `todos-context.tsx`:

```tsx
import React, { useState } from 'react';
import Todo from '../models/todo';

type TodosContextObject = {
  items: Todo[];
  addTodo: (text: string) => void;
  removeTodo: (id: string) => void;
};

type Props = { children: React.ReactNode };

const initialContext: TodosContextObject = {
  items: [],
  addTodo: () => {},
  removeTodo: () => {},
};

export const TodosContext =
  React.createContext<TodosContextObject>(initialContext);

const TodosContextProvider: React.FC<Props> = (props) => {
  const [todos, setTodos] = useState<Todo[]>([]);

  const addTodoHandler = (text: string) => {
    const newTodo = new Todo(text);
    console.log('newTodo:', newTodo);
    setTodos((prevState) => [newTodo, ...prevState]);
  };

  const removeTodoHandler = (id: string) => {
    setTodos((prevState) => prevState.filter((todo: Todo) => todo.id !== id));
  };

  const contextValue: TodosContextObject = {
    items: todos,
    addTodo: addTodoHandler,
    removeTodo: removeTodoHandler,
  };

  return (
    <TodosContext.Provider value={contextValue}>
      {props.children}
    </TodosContext.Provider>
  );
};

export default TodosContextProvider;
```

Refactor `Todos` to leverage the Context API:

```tsx
import React, { useContext } from 'react';
import TodoItem from './TodoItem';
import { TodosContext } from '../store/todos-context';
import classes from './Todos.module.css';

const Todos: React.FC = () => {
  const todosCtx = useContext(TodosContext);

  return (
    <ul className={classes.items}>
      {todosCtx.items.map((item) => (
        <TodoItem
          key={item.id}
          item={item}
          onRemoveTodo={todosCtx.removeTodo}
        />
      ))}
    </ul>
  );
};

export default Todos;
```

Refactor `NewTodo` to leverage the Context API:

```tsx
import { useContext, useRef } from 'react';
import { TodosContext } from '../store/todos-context';
import classes from './NewTodo.module.css';

const NewTodo: React.FC = () => {
  const todosCtx = useContext(TodosContext);
  const todoTextInputRef = useRef<HTMLInputElement>(null);

  const submitHandler = (event: React.FormEvent) => {
    event.preventDefault();
    const enteredText = todoTextInputRef.current!.value;
    if (enteredText.trim().length === 0) {
      // throw an error
      return;
    }
    todosCtx.addTodo(enteredText);
  };

  return (
    <form onSubmit={submitHandler} className={classes.form}>
      <label htmlFor="text">Todo Text</label>
      <input type="text" id="text" ref={todoTextInputRef} />
      <button>Add Todo</button>
    </form>
  );
};

export default NewTodo;
```

Refactor `App.tsx` to wrap all the components that require access ot the Context
in `ContextProvider`:

```tsx
import TodosContextProvider from './store/todos-context';
import Todos from './components/Todos';
import NewTodo from './components/NewTodo';

function App() {
  return (
    <TodosContextProvider>
      <NewTodo />
      <Todos />
    </TodosContextProvider>
  );
}

export default App;
```

Save changes and refresh. Observe that we still get the same behavior, but we're
now using the Context API, properly-typed with Typescript.
