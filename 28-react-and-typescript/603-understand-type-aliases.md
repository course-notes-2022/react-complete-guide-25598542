# Understanding Type Aliases

At some point, you'll probably repeat some type definitions. We're already doing
it for the `people` variable: the object type is the same as the `person` object
we defined earlier. For this scenario, we can define a **type alias**:

```ts
// Define a type alias
type Person = {
  name: string;
  age: number;
};

// We can now use our type alias wherever needed
let person: Person;

person = {
  name: 'Johnny Bravo',
  age: 32,
};

let people: Person[];
```
