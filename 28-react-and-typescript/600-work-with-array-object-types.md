# Working with Array and Object Types

```ts
// Primitives: number, string, boolean
// More complex types: arrays, objects
// Function types, parameters

// Primitives:

let age: number;
age = 12;

let userName: string; // Note that the data types are LOWERCASE for primitive types

userName = 'foo';

let isInstructor: boolean;

isInstructor = true;

// Complex types

let hobbies: string[]; // store an ARRAY of strings in `hobbies`

hobbies = ['Sports', 'Cooking'];

let person: { name: string; age: number };

person = {
  name: 'Johnny Bravo',
  age: 32,
};

/*
We could do this:

let person: any;

person = {
    name: 'Johnny Bravo',
    age: 32
}

But the use of `any` as a type defeats
the purpose of Typescript, and should not
be used!
*/

let people: { name: string; age: number }[]; // Store an array of `person` objects
```
