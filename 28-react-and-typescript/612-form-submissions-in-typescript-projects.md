# Form Submissions in Typescript Projects

We've now built some components dealing with **outputting** events. Let's build
a component dealing with **inputting** data.

Create a new component, `NewTodo.tsx` as follows:

```tsx
const NewTodo = () => {
  const submitHandler = (event: React.FormEvent) => {
    event.preventDefault();
  };

  return (
    <form onSubmit={submitHandler}>
      <label htmlFor="text">Todo Text</label>
      <input type="text" id="text" />
      <button>Add Todo</button>
    </form>
  );
};

export default NewTodo;
```

This is a simple form with a single text input. We'll add the ability to **get
user input** to our form shortly. Recall that we have **two basic strategies**
for getting user input:

- Listen to **every keystroke** using `useState`
- Use a `ref` to get the value of the input once the form is **submitted**

We select a strategy **depending upon the _validation_ strategy** we want to
use: validating on _keystroke_ or on _submission of the form_.

## Typing our Event Object

Note that the event object we're passing to the `submitHandler` function is
**typed**: a `React.FormEvent` object. This is a special type provided by the
React package that is **automatically passed to the event handler** by
`onSubmit`. There are other events in the `React` package (`MouseEvent`,
`InputEvent`, etc.). With this, we get the same type checks that Typescript
gives us for variables.
