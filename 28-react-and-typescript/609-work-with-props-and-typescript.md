# Working with Props and Typescript

Add the following to `Todos.tsx`:

```tsx
export default function Todos(props) {
  return (
    <ul>
      <li>Learn React</li>
      <li>Learn Typescript</li>
    </ul>
  );
}
```

Note that when we do so, our IDE begins complaining about the `props` being of
type `any`:

![typescript complaining about props](./screenshots/typescript-complaining-about-props.png)

This is Typescript in action. We didn't assign a type to `props`, and Typescript
is alerting us to that fact.

Let's make it clear what kind of props we're expecting.

We _could_ define our props like this:

```tsx
export default function Todos(props: { items: string[] }) {
  return (
    <ul>
      <li>Learn React</li>
      <li>Learn Typescript</li>
    </ul>
  );
}
```

But that would be **incorrect**. Recall that `props` in React is an **object**
that contains multiple properties, including `children`. It would be cumbersome
to try and add type definitions for **all** the properties of `props`.
Therefore, we can use an alternate method.

Refactor `Todos.tsx` as follows:

```tsx
import React from 'react';

const Todos: React.FC<{ items: string[] }> = (props) => {
  return (
    <ul>
      {props.items.map((item) => (
        <li key={item}>{item}</li>
      ))}
    </ul>
  );
};

export default Todos;
```

### Describing the Shape of our Props

What have we done in this code?

React and Typescript give us a way to turn our functional components into a
**generic function**. Our functional component will be configured so that we
make clear that it will be a React component function with all the **built-in
`props`** (such as `children`), **as well as our own custom `props`**.

We assign a **type** to the `Todos` constant: `React.FC`. This type is defined
in `@types/react`, and is a type definition as we saw before. `React.FC` makes
clear the the function to which it's applied is a **functional component**.

By adding this annotation, Typescript understands that this is a function that
receives a `props` argument that contains `children`.

`React.FC` is a **generic type** that **allows us to merge our _own custom
props_ with the built-in `props` object**. For this, we add the `<>`, between
which we add the definition for our custom props:

```tsx
const Todos: React.FC<{ items: string[] }> = (props) => {
  // ...
};
```

### What Do We Get?

Our Typescript code now **knows the shape of our custom props**. We get
autocompletion, type safety checks, etc. with our **custom props**. For example,
we have an error in `App.tsx` because Typescript understands that our `Todos`
component **requires an `items` property**:

![todos props error](./screenshots/todos-props-err.png)

If we refactor `App` to pass the expected `props` to `Todos`:

```tsx
import Todos from './components/Todos';

function App() {
  return (
    <div>
      <Todos items={['Learn React', 'Learn Typescript']} />
    </div>
  );
}

export default App;
```

The error goes away.
