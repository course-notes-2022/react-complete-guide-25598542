# Time to Practice

Here's a challenge for you: Create a new `Todo` **component**. The JSX should be
the same as the `li` currently being rendered in the `Todos` component, but
outsourced to a separate component. Use what we've learned so far about
Typescript and React components to add the proper type safety to the new `Todo`
component.

## MY Solution

`TodoItem.tsx`:

```tsx
import React from 'react';
import Todo from '../models/todo';

const TodoItem: React.FC<{ item: Todo }> = (props) => {
  const { item } = props;

  return <li>{item.text}</li>;
};

export default TodoItem;
```

`Todos.tsx`:

```tsx
import React from 'react';
import TodoItem from './Todo';
import Todo from '../models/todo';

const Todos: React.FC<{ items: Todo[] }> = (props) => {
  return (
    <ul>
      {props.items.map((item) => (
        <TodoItem key={item.id} item={item} />
      ))}
    </ul>
  );
};

export default Todos;
```

`App.tsx`:

```tsx
import Todos from './components/Todos';
import Todo from './models/todo';

function App() {
  const todos = [new Todo('Bake bread'), new Todo('Buy Milk')];
  return (
    <div>
      <Todos items={todos} />
    </div>
  );
}

export default App;
```
