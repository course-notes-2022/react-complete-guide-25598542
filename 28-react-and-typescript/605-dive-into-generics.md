# Diving Into Generics

Let's conclude our basic Typescript introduction with **Generics**. Imagine the
following scenario:

```ts
function insertAtBeginning<T>(array: T[], value: T) {
  const newArray = [value, ...array];
  return newArray;
}

const demoArray = [1, 2, 3];
const updatedArray = insertAtBeginning(demoArray, -1); // [-1, 1, 2, 3]
console.log(updatedArray[0].toFixed(2)); // We can call Number class methods on members of the NUMERICAL `updatedArray`

const demoStringArray = ['foo', 'bar', 'baz'];
const updatedStrings = insertAtBeginning(demoStringArray, 'quuz');
console.log(updatedStrings[0].length); // We can call String class members of a STRING `updatedStrings`
```

**Generics** allow us to make our functions more..._generic_. We have the
flexibility to pass **any type of argument we want** to `insertAtBeginning`,
**yet still maintain our type-safety**!
