# Time to Pactice: Removing a Todo

Let's now ensure that we can **remove** a todo by clicking on it.

This is another challenge for you! Add the code to remove a todo, using the
things you've learned about React and Typescript so far.

## MY Solution

`App.tsx`:

```tsx
import { useState } from 'react';
import Todos from './components/Todos';
import NewTodo from './components/NewTodo';
import Todo from './models/todo';

function App() {
  const [todos, setTodos] = useState<Todo[]>([]);

  const addTodoHandler = (text: string) => {
    const newTodo = new Todo(text);
    setTodos((prevState) => [newTodo, ...prevState]);
  };

  const removeTodoHandler = (id: string) => {
    setTodos((prevState) => prevState.filter((todo: Todo) => todo.id !== id));
  };

  return (
    <div>
      <NewTodo onAddTodo={addTodoHandler} />
      <Todos items={todos} onRemoveTodo={removeTodoHandler} />
    </div>
  );
}

export default App;
```

`Todos.tsx`:

```tsx
import React from 'react';
import TodoItem from './TodoItem';
import Todo from '../models/todo';
import classes from './Todos.module.css';

const Todos: React.FC<{ items: Todo[]; onRemoveTodo: (id: string) => void }> = (
  props
) => {
  return (
    <ul className={classes.items}>
      {props.items.map((item) => (
        <TodoItem key={item.id} item={item} onRemoveTodo={props.onRemoveTodo} />
      ))}
    </ul>
  );
};

export default Todos;
```

`TodoItem.tsx`:

```tsx
import React from 'react';
import Todo from '../models/todo';
import classes from './TodoItem.module.css';

const TodoItem: React.FC<{ item: Todo; onRemoveTodo: (id: string) => void }> = (
  props
) => {
  const { item } = props;

  return (
    <li
      className={classes.item}
      key={item.id}
      onClick={() => props.onRemoveTodo(item.id)}
    >
      {item.text}
    </li>
  );
};

export default TodoItem;
```
