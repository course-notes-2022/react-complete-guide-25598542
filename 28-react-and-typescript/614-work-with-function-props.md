# Working with Function Props

Let's now use our `NewTodo` component in `App.tsx`. We want to be able to add a
new item to the `todos` array every time a new todo is submitted via the
`NewTodo` component.

In order to achieve this, we need to:

1. Create a new property on the `App` component state to **contain the todos**
2. Re-render `App` when the state changes to trigger a re-render of `Todos`
3. Create a method of communicating new todos from the `NewTodo` to its parent,
   `App`.

We'll focus on #3 first.

As we've done many times before, we will pass a **function** from `App` to
`NewTodos` as a prop. This function will handle updating the parent component
when a todo is added

## Defining Function Props

Refactor `NewTodos.tsx` as follows:

```tsx
import { useRef } from 'react';

const NewTodo: React.FC<{ onAddTodo: (text: string) => void }> = (props) => {
  const todoTextInputRef = useRef<HTMLInputElement>(null);

  const submitHandler = (event: React.FormEvent) => {
    event.preventDefault();
    const enteredText = todoTextInputRef.current!.value;
    if (enteredText.trim().length === 0) {
      // throw an error
      return;
    }
    props.onAddTodo(enteredText);
  };

  return (
    <form onSubmit={submitHandler}>
      <label htmlFor="text">Todo Text</label>
      <input type="text" id="text" ref={todoTextInputRef} />
      <button>Add Todo</button>
    </form>
  );
};

export default NewTodo;
```

Note that we've again used `React.FC` with a generic type to inform Typescript
about the **shape** of our **custom props**. This time, our props contain a
**function**. We define function types as follows in Typescript:

```ts
onAddTodo: (text: string) => void
```

This indicates to Typescript that `onAddTodo` takes a single `text` property of
type `string`, and returns **nothing**.

Refactor `App.tsx` to create a new function of the expected shape and pass to
`NewTodos` as `props`:

```tsx
import { useState } from 'react';
import Todos from './components/Todos';
import NewTodo from './components/NewTodo';
import Todo from './models/todo';

function App() {
  const addTodoHandler = (text: string) => {};

  const todos = [new Todo('Bake bread'), new Todo('Buy Milk')];
  return (
    <div>
      <NewTodo onAddTodo={addTodoHandler} />
      <Todos items={todos} />
    </div>
  );
}

export default App;
```
