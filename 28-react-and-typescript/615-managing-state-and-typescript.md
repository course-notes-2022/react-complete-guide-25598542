# Managing State and Typescript

Let's now turn to managing our `App` component `todos` state with Typescript.

We can use the `useState` hook to define a piece of internal component state as
we've done before:

```tsx
import { useState } from 'react';
import Todos from './components/Todos';
import NewTodo from './components/NewTodo';
import Todo from './models/todo';

function App() {
  const [todos, setTodos] = useState([]);
  const addTodoHandler = (text: string) => {};

  return (
    <div>
      <NewTodo onAddTodo={addTodoHandler} />
      <Todos items={todos} />
    </div>
  );
}

export default App;
```

We _could_ define our state like this, but once again we're missing out on the
type-safety checks that Typescript provides for us. If we hover over the `todos`
destructured variable above, Typescript tells us that it is of type `never[]`.
This basically means that the array can _never_ hold **any values**. This is
definitely not what we want!

To inform Typescript that the array is **initially empty** but **will eventually
hold an array of `Todo` objects**, we can use generics as follows:

```tsx
import { useState } from 'react';
import Todos from './components/Todos';
import NewTodo from './components/NewTodo';
import Todo from './models/todo';

function App() {
  const [todos, setTodos] = useState<Todo[]>([]);
  const addTodoHandler = (text: string) => {};

  return (
    <div>
      <NewTodo onAddTodo={addTodoHandler} />
      <Todos items={todos} />
    </div>
  );
}

export default App;
```

Now we can properly update our state:

```tsx
import { useState } from 'react';
import Todos from './components/Todos';
import NewTodo from './components/NewTodo';
import Todo from './models/todo';

function App() {
  const [todos, setTodos] = useState<Todo[]>([]);
  const addTodoHandler = (text: string) => {
    const newTodo = new Todo(text);
    setTodos((prevState) => [newTodo, ...prevState]);
  };

  return (
    <div>
      <NewTodo onAddTodo={addTodoHandler} />
      <Todos items={todos} />
    </div>
  );
}

export default App;
```

Save changes and refresh. Add a new todo using the input field. Observe that the
new todo text is displayed in an unordered list in the browser.
