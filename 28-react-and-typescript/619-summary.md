# Summary

We've learned a lot about React and Typescript. However, we're only scratching
the surface of Typescript and React.

You can use the official Typescript docs for learning more about Typescript in
great detail.

For React and Typescript, you can see the "Create React App docs", or the
Typescript course at Academind.
