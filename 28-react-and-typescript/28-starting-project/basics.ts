// Primitives: number, string, boolean
// More complex types: arrays, objects
// Function types, parameters

// Primitives:

let age: number;
age = 12;

let userName: string; // Note that the data types are LOWERCASE for primitive types

userName = 'foo';

let isInstructor: boolean;

isInstructor = true;

// Complex types

let hobbies: string[]; // store an ARRAY of strings in `hobbies`

hobbies = ['Sports', 'Cooking'];

// Define a type alias
type Person = {
  name: string;
  age: number;
};

let person: Person;

person = {
  name: 'Johnny Bravo',
  age: 32,
};

/*
We could do this:

let person: any;

person = {
    name: 'Johnny Bravo',
    age: 32
}

But the use of `any` as a type defeats
the purpose of Typescript, and should not
be used!
*/

let people: Person[]; // Store an array of `person` objects

let course = 'React - The Complete Guide'; // Typescript infers that `course` is a string type
// course = 2; // NO

let multiType: string | number = 'React - The Complete Guide'; // Union Type

// Functions and Types

// Specify the function **return type**
function add(a: number, b: number): number {
  return a + b;
}

// Add `void` as the return type of functions
// with no explicit return value
function printOutput(value: any): void {
  console.log(value);
}

// Generics

function insertAtBeginning<T>(array: T[], value: T) {
  const newArray = [value, ...array];
  return newArray;
}

const demoArray = [1, 2, 3];
const updatedArray = insertAtBeginning(demoArray, -1); // [-1, 1, 2, 3]
console.log(updatedArray[0].toFixed(2)); // We can call Number class methods on members of the NUMERICAL `updatedArray`

const demoStringArray = ['foo', 'bar', 'baz'];
const updatedStrings = insertAtBeginning(demoStringArray, 'quuz');
console.log(updatedStrings[0].length); // We can call String class members of a STRING `updatedStrings`
