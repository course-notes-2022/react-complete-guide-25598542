import React, { useState } from 'react';
import Todo from '../models/todo';

type TodosContextObject = {
  items: Todo[];
  addTodo: (text: string) => void;
  removeTodo: (id: string) => void;
};

type Props = { children: React.ReactNode };

const initialContext: TodosContextObject = {
  items: [],
  addTodo: () => {},
  removeTodo: () => {},
};

export const TodosContext =
  React.createContext<TodosContextObject>(initialContext);

const TodosContextProvider: React.FC<Props> = (props) => {
  const [todos, setTodos] = useState<Todo[]>([]);

  const addTodoHandler = (text: string) => {
    const newTodo = new Todo(text);
    console.log('newTodo:', newTodo);
    setTodos((prevState) => [newTodo, ...prevState]);
  };

  const removeTodoHandler = (id: string) => {
    setTodos((prevState) => prevState.filter((todo: Todo) => todo.id !== id));
  };

  const contextValue: TodosContextObject = {
    items: todos,
    addTodo: addTodoHandler,
    removeTodo: removeTodoHandler,
  };

  return (
    <TodosContext.Provider value={contextValue}>
      {props.children}
    </TodosContext.Provider>
  );
};

export default TodosContextProvider;
