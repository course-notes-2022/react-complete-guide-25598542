import React from 'react';
import Todo from '../models/todo';
import classes from './TodoItem.module.css';

const TodoItem: React.FC<{ item: Todo; onRemoveTodo: (id: string) => void }> = (
  props
) => {
  const { item } = props;

  return (
    <li
      className={classes.item}
      key={item.id}
      onClick={() => props.onRemoveTodo(item.id)}
    >
      {item.text}
    </li>
  );
};

export default TodoItem;
