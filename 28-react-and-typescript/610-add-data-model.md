# Adding a Data Model

Let's refine our app and add a **data model**. At the moment, a `Todo` is a
string. This may be what you need, but often a `Todo` will be an `Object` with
multiple properties.

Add a new folder, `src/models`. In this folder we'll describe our data models.
Add a new file, `Todos.ts`:

```ts
class Todo {
  id: string;
  text: string;

  constructor(todoText: string) {
    this.text = todoText;
    this.id = new Date().toISOString();
  }
}

export default Todo;
```

Refactor `App.js` to pass a list of `Todo` objects to `Todos`:

```tsx
import Todos from './components/Todos';
import Todo from './models/todo';

function App() {
  const todos = [new Todo('Bake bread'), new Todo('Buy Milk')];
  return (
    <div>
      <Todos items={todos} />
    </div>
  );
}

export default App;
```

Refactor `Todos` to use the `Todo` class:

```tsx
import React from 'react';
import Todo from '../models/todo';

const Todos: React.FC<{ items: Todo[] }> = (props) => {
  return (
    <ul>
      {props.items.map((item) => (
        <li key={item.id}>{item.text}</li>
      ))}
    </ul>
  );
};

export default Todos;
```

We are making it **very clear** what shape our data and components should have!
Its cleaner, more structured, and more robust for our development experience!
