# Functions and Function Types

Let's look at one more feature: Functions and types.

Consider the following function:

```ts
function add(a: number, b: number): number {
  return a + b;
}
```

Although Typescript will **infer** the return type of a function, we can also
**set it explicitly** using the `:` syntax.

Functions with no return value can be decorated with the `void` return type:

```ts
// Add `void` as the return type of functions
// with no explicit return value
function printOutput(value: any): void {
  console.log(value);
}
```
