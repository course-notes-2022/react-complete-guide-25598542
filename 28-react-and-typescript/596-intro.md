# Intro

We've learned a lot about the core features that make up React. So far, we've
used React with **plain JavaScript**. Actually, we can use React with
**Typescript** as well. In this section, we'll learn:

- What is Typescript? Why might we want to use it?
- Typescript Basics
- Combining React & Typescript
