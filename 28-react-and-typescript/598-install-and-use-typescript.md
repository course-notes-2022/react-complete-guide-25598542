# Installing and Using Typescript

Let's now install and use Typescript.

[See the Typescript documentation for more information.](https://www.typescriptlang.org/docs/)

Initialize a new npm project:

> `npm init -y`

To install typescript locally, run the following command:

> `npm install typescript`

This will install Typescript in a **specific project** (use the `-g` flag to
install globally).

With this, we can invoke the Typescript **compiler**. Typescript code **does not
run in the browser**, therefore we must **compile Typescript to Javascript**
before we can run the code in the browser. It is in the **compilation step**
where we'll be notified of any errors.

To invoke the compiler, run:

> `npx tsc`

Note that if we run it now, we'll get an error. Typescript expects a **config
file** that tells it which files to compile. We'll create that configuration
later; for now, run:

> `npx tsc with-typescript.ts`

Add the following code to `with-typescript.ts`, and note the output below:

```ts
function add(x: number, y: number) {
  return x + y;
}

const sum = add('2', '4');
```

![typescript compile error](./screenshots/typescript-compile-err.png)

Note that we also get a Javascript output file of the same name; Typescript will
still output the file even if there are compilation errors. The warnings in the
terminal are just that: **warnings**.
