# Working with `refs` and `useRef`

Let's create a new `ref` and attach it to our `input` field. Add the following
to `NewTodo.tsx`:

```tsx
import { useRef } from 'react';

const NewTodo = () => {
  const todoTextInputRef = useRef();

  const submitHandler = (event: React.FormEvent) => {
    event.preventDefault();
  };

  return (
    <form onSubmit={submitHandler}>
      <label htmlFor="text">Todo Text</label>
      <input type="text" id="text" ref={todoTextInputRef} />
      <button>Add Todo</button>
    </form>
  );
};

export default NewTodo;
```

Note that this produces an error in our IDE:

![ref error](./screenshots/ref-error.png)

When we **create** the ref in this line:

```tsx
const todoTextInputRef = useRef();
```

Typescript has no idea that we want to attach the ref to an `input` element. We
could theoretically be attaching to **any type** of HTML element, which is why
Typescript is complaining. We must be more explicit about the **type** of data
we'll store in the `ref`:

```tsx
import { useRef } from 'react';

const NewTodo = () => {
  const todoTextInputRef = useRef<HTMLInputElement>(null);

  const submitHandler = (event: React.FormEvent) => {
    event.preventDefault();
    // We can use the `!` operator here because we
    // are certain that when `submitHandler` is invoked
    // `todoTextInputRef` will already be connected to
    // the `input` element and therefore `current` will
    // not be undefined
    const enteredText = todoTextInputRef.current!.value;
    if (enteredText.trim().length === 0) {
      // throw an error
      return;
    }
  };

  return (
    <form onSubmit={submitHandler}>
      <label htmlFor="text">Todo Text</label>
      <input type="text" id="text" ref={todoTextInputRef} />
      <button>Add Todo</button>
    </form>
  );
};

export default NewTodo;
```

Note that we are decorating `useRef` with the `HTMLInputElement` generic _and_
passing an initial value of `null` to it. We must do this to inform Typescript
of the **type** of element to which this ref will refer, and that it will
_eventually_ be connected to an HTML `input` element but _initially_ has no
value.
