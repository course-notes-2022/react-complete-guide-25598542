# Working with Components and Typescript

Let's now begin to work with components and Typescript. We'll build a basic todo
app where we can add/delete todos. This simple app will allow us to explore
basic Typescript concepts.

Create a new file, `src/components/Todos.tsx`, and add the following:

```tsx
export default function Todos() {
  return (
    <ul>
      <li>Learn React</li>
      <li>Learn Typescript</li>
    </ul>
  );
}
```

Use the `Todos` component in the `App` component as we always have:

```tsx
import Todos from './components/Todos';

function App() {
  return (
    <div>
      <Todos />
    </div>
  );
}

export default App;
```

Nothing new here. It gets more interesting when our component receives todos
through `props`.
