# Preparing the App for Data Fetching

Let's establish a connection between the React app and our backend server.
Ensure that the server is **running in a terminal** at all times while
developing the frontend app. Start the backend server by running:

> `node app.js`

Let's start by fetching some **available places** from our backend. We need to
send an `HTTP` request from our React app to our backend.

Open the `src/components/AvailablePlaces.jsx` file. This is where we'll fetch
the available places. Previously, we fetched data locally from `localStorage`.
The good thing about doing this is that `localStorage` is **synchronous**.
Fetching data from a backend server is **_asynchronous_**: we cannot predict
when or _how long_ the request/response cycle will take to complete!

This is a problem for our component function, as the component function executes
pretty much immediately. If it's dependent upon some externally-fetched data,
and that data is _available_, then the component can't render properly and will
error out.

We need to set the data to an **intial value** for the component to use, and
then **update the data** once the fetch operation is complete.

Start by setting an initial value. Use the `useState` hook and make the
following changes to `AvailablePlaces`:

```jsx
import { useState } from 'react';
import Places from './Places.jsx';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```
