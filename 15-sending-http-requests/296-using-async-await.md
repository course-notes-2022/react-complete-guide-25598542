# Using `async/await`

We're now successfully fetching data. Let's improve our code a bit. Many
developers prefer the `async/await` syntax, as it's a little leaner. Let's look
at how to use `async/await` **properly** in a `useEffect`.

## DON'T Do THIS

We must **not** attempt to make the **callback function** we pass to `useEffect`
asynchronous:

```jsx
import { useEffect, useState } from 'react';
import Places from './Places.jsx';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);

  useEffect(async () => {
    // Do NOT put `async` HERE
    const response = (await fetch('http://localhost:3000/places')).json();
    setAvailablePlaces(response.places);
  }, []);

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

Effect callbacks are synchronous to prevent race conditions. Instead, put the
async function **inside the callback**:

```jsx
import { useEffect, useState } from 'react';
import Places from './Places.jsx';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);

  useEffect(() => {
    const fetchPlaces = async () => {
      const response = await fetch('http://localhost:3000/places');
      const data = await response.json();
      setAvailablePlaces(data.places);
    };

    fetchPlaces();
  }, []);

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```
