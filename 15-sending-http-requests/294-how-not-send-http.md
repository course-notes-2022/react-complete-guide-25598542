# How NOT to Send HTTP Requests (and Why It's Wrong)

An `HTTP` request can be sent from a client in different ways. One way is to use
the built-in `fetch` function.

In its simplest form, `fetch` takes the URL of the route to which you want to
send the request:

```jsx
import { useState } from 'react';
import Places from './Places.jsx';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);

  fetch('http://localhost:3000/places');

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

`fetch` returns a `Promise`, a JS object representing a value that will
**eventually** become available. To access that value, you can **chain methods**
to the `fetch` function:

```jsx
import { useState } from 'react';
import Places from './Places.jsx';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);

  fetch('http://localhost:3000/places').then((response) => {
    // This function will execute
    // when the response SUCCEEDS
  });

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

You can also use an **alternative syntax** to `.then()`, the `async/await`
syntax:

```jsx
import { useState } from 'react';
import Places from './Places.jsx';

export default async function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);

  const response = await fetch('http://localhost:3000/places');

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

**Note however that the `async` keyword is _not allowed_ on component
functions**. This is a restriction applied by React. We _can_ use `async/await`
elsewhere; we'll learn about that later.

For now, let's refactor `AvailablePlaces` as follows to fetch the places data
and set `availablePlaces` accordingly:

```jsx
import { useState } from 'react';
import Places from './Places.jsx';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);

  fetch('http://localhost:3000/places')
    .then((response) => {
      return response.json(); // response.json() returns ANOTHER Promise; need to chain another `.then()`
    })
    .then((resData) => {
      setAvailablePlaces(resData.places);
    });

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

Looks good... _right_?

**Actually, there's a big problem with this code**. Calling `fetch` in this way
directly in the component function will execute this code **every time the
component executes**. But because we update the state, that will trigger
_another_ component function execution, which will cause the `fetch` function to
_execute again_, which will update the state and cause _another component
execution_... leading to an **infinite loop**!
