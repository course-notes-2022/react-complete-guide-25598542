# Sending HTTP Requests (`GET` Request) via `useEffect`

Thankfully, React provides a solution that we've already seen: the `useEffect`
hook.

Update `AvailablePlaces.jsx` as follows:

```jsx
import { useEffect, useState } from 'react';
import Places from './Places.jsx';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);

  /*
    useEffect ensures that the function executes AFTER
    the component function executes initially, and again only if the dependencies in the dependency array change
*/
  useEffect(() => {
    fetch('http://localhost:3000/places')
      .then((response) => {
        return response.json();
      })
      .then((resData) => {
        setAvailablePlaces(resData.places);
      });
  }, []);

  return (
    <Places
      title="Available Places"
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

Save all changes and refresh. Note that we are now fetching our places once
again.
