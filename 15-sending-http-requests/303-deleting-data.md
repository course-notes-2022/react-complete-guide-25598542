# Deleting Data (via `DELETE` HTTP Requests)

We can also **get rid of selected places** by clicking on them. Currently, this
functionality does not persist to the **backend**. Let's change this now.

Update the `handleRemovePlace` in `App.js`. Refactor the function as follows:

```jsx
const handleRemovePlace = useCallback(
  async function handleRemovePlace() {
    setUserPlaces((prevPickedPlaces) =>
      prevPickedPlaces.filter((place) => place.id !== selectedPlace.current.id)
    );

    try {
      await updateUserPlaces(
        userPlaces.filter((place) => place.id !== selectedPlace.current.id)
      );
    } catch (err) {
      setUserPlaces(userPlaces);
      setErrorUpdatingPlaces({
        message: err.message || 'Failed to delete place',
      });
    }

    setModalIsOpen(false);
  },
  [userPlaces]
);
```
