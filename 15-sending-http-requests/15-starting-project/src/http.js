export async function fetchAvailablePlaces() {
  const response = await fetch('http://localhost:3000/places');
  if (!response.ok) {
    throw new Error('Failed to fetch places');
  }

  const data = await response.json();
  return data.places;
}

export async function updateUserPlaces(places) {
  const response = await fetch('http://localhost:3000/user-places', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ places }),
  });

  const resData = await response.json();

  if (!response.ok) {
    throw new Error('Failed to update user data');
  }

  return resData.message;
}

export async function fetchUserPlaces() {
  const response = await fetch('http://localhost:3000/user-places', {
    method: 'GET',
  });

  if (!response.ok) {
    throw new Error('Failed to fetch user places');
  }
  const userPlaces = await response.json();
  return userPlaces;
}
