import { useEffect, useState } from 'react';
import Places from './Places.jsx';
import Error from './Error.jsx';
import { sortPlacesByDistance } from '../loc.js';
import { fetchAvailablePlaces } from '../http.js';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);
  const [isFetching, setIsFetching] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    const fetchPlaces = async () => {
      try {
        const places = await fetchAvailablePlaces();
        navigator.geolocation.getCurrentPosition((position) => {
          const sortedPlaces = sortPlacesByDistance(
            places,
            position.coords.latitude,
            position.coords.longitude
          );
          setAvailablePlaces(sortedPlaces);
          setIsFetching(false); // Call this HERE
        });
      } catch (err) {
        setError(err);
        setIsFetching(false); // AND HERE
      }
      // setIsFetching(false); // Cannot call this here anymore; JS will not wait until the getCurrentPosition callback is done
    };

    fetchPlaces();
  }, []);

  if (error) {
    return (
      <Error
        title="Failed to Fetch Places"
        message={error.message}
        onConfirm={() => {}}
      />
    );
  }
  return (
    <Places
      title="Available Places"
      isLoading={isFetching}
      loadingText="Loading places..."
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
