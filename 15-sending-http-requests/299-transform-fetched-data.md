# Transforming Fetched Data

Now that we're fetching the places, we would like to sort these places by
distance to the user's location. We have the `loc.js` file with some utility
functions to perform the calculations. We therefore need to fetch the user's
**location** in `AvailablePlaces`.

We can use the browser's built-in `navigator.geolocation.getCurrentPosition()`
method to get the user's location. It takes a **function** that will be executed
in the browser once the user's location is known. Note that we **cannot** use
`async/await` because `getCurrentPostion()` does not return a `Promise`, thus we
use the callback pattern to define the code once the location is there.

Update `AvailablePlaces.jsx` as follows:

```jsx
import { useEffect, useState } from 'react';
import Places from './Places.jsx';
import Error from './Error.jsx';
import { sortPlacesByDistance } from '../loc.js';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);
  const [isFetching, setIsFetching] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    const fetchPlaces = async () => {
      try {
        const response = await fetch('http://localhost:3000/places');
        if (!response.ok) {
          throw new Error('Failed to fetch places');
        }

        const data = await response.json();
        navigator.geolocation.getCurrentPosition((position) => {
          const sortedPlaces = sortPlacesByDistance(
            data.places,
            position.coords.latitude,
            position.coords.longitude
          );
          setAvailablePlaces(sortedPlaces);
          setIsFetching(false); // Call this HERE
        });
      } catch (err) {
        setError(err);
        setIsFetching(false); // AND HERE
      }
      // setIsFetching(false); // Cannot call this here anymore; JS will not wait until the getCurrentPosition callback is done
    };

    fetchPlaces();
  }, []);

  if (error) {
    return (
      <Error
        title="Failed to Fetch Places"
        message={error.message}
        onConfirm={() => {}}
      />
    );
  }
  return (
    <Places
      title="Available Places"
      isLoading={isFetching}
      loadingText="Loading places..."
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

Save changes and refresh. Note that we are now asked for permission to share our
location, and the sorted places load after a moment.
