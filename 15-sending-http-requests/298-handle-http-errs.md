# Handling `HTTP` Errors

A common situation we face when sending HTTP request is an **error**. Errors can
arise for many reasons: a bad network connection, an invalid user request, etc.
We should be prepared to handle HTTP errors in our frontend code!

There are two main ways a request may fail:

- The request may fail to send in the first place (e.g. network crash)

- The request may send successfully, but something goes wrong on the backend

## The Error Response

When using `fetch`, we can check if the response is an **error response** by
checking the `ok` property. `ok` will be `true` if the response is successful
(`200` - `300` series status code), and `false` otherwise (`400` - `500` status
code).

Update `AvailablePlaces.jsx` as follows:

```jsx
import { useEffect, useState } from 'react';
import Places from './Places.jsx';
import Error from './Error.jsx';

export default function AvailablePlaces({ onSelectPlace }) {
  /*
    A common state pattern for data-fetching components:
    1. DATA state
    2. FETCHING state
    3. ERROR state
    */
  const [availablePlaces, setAvailablePlaces] = useState([]);
  const [isFetching, setIsFetching] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    const fetchPlaces = async () => {
      // Move data fetching into `try-catch`
      // `fetch` throws an error, thus we need to handle
      // it in a try-catch block
      try {
        const response = await fetch('http://localhost:3001/places');
        if (!response.ok) {
          throw new Error('Failed to fetch places');
        }

        const data = await response.json();
        setAvailablePlaces(data.places);
      } catch (err) {
        setError(err);
      }
      setIsFetching(false);
    };

    fetchPlaces();
  }, []);

  if (error) {
    // Display UI feedback if an error occurs
    return (
      <Error
        title="Failed to Fetch Places"
        message={error.message}
        onConfirm={() => {}}
      />
    );
  }
  return (
    <Places
      title="Available Places"
      isLoading={isFetching}
      loadingText="Loading places..."
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```

Simulate an error by replacing the `3000` in the URL with an invalid port
number. Note the output in the UI:

![ui error output](./screenshots/ui-error-output.png)
