# Sending Data with `POST` Requests

We want to be able to add places to our selection and store those selections on
the backend.

Our backend already has two endpoints for storing the places and fetching the
selected places respectively. Let's start by ensuring that we send the updated
selection of places to the backend when the user selects a place.

Let's create a new `http` utility function for sending the selected places to
the backend:

```jsx
export async function updateUserPlaces(places) {
  const response = await fetch('http://localhost:3000/user-places', {
    method: 'PUT',
    headers: {
      'Content-Type': 'application/json',
    },
    body: JSON.stringify({ places }),
  });

  const resData = await response.json();

  if (!response.ok) {
    throw new Error('Failed to update user data');
  }

  return resData.message;
}
```

In `App.jsx`, update the `handleSelectPlace` function to send the places to the
backend with the `updateUserPlaces` function:

```jsx

```

```jsx

```
