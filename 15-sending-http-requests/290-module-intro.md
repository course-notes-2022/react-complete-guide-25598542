# Intro

Up until now, we've worked with demo projects where all the data required for
the project was **part of the (frontend) React project**. You _might_ be
building projects like this in the real world, but more commonly you'll need to
**connect with a server** to fetch and send the **data** required for your
application. In the end, a React app runs in the **browser** of each individual
user. It would be impossible to store and manage a **centralized datastore** on
so many different clients!

In this course section, we'll look at **data fetching** in React apps. We'll
learn:

- How to connect to a **backend/database**
- How to **fetch** data
- How to **send data**
