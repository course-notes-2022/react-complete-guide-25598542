# How (Not) to Connect to a Database

Some data must be managed **centrally** in our applications:

![centralized database](./screenshots/sharing-data.png)

> How do you connect your React app to a database?

The answer is: **you don't**! At least, not directly. Attempting to connect to a
database **directly** from a **front-end** application is a **security risk**.

**Always keep in mind**:

> Your React code runs in the **browsers of your users**. Your visitors can
> access and view that code via the browser devtools!

If your frontend code contains **secrets** (database login credentials, etc.),
your users can **easily see this information** in the devtools and **exploit
it**.

Additionally, **not all operations can be performed in the browser**, dut to
technical restrictions and constraints. For example, you can't easily access a
centrally managed file system.

Instead of directly accessing a database, you communicate with a backend server
that acts as a "middleman" (e.g. a REST API server) that _can_ interact with
databases, etc. which is **inaccessible** to your users. You connect to this
middleman via **HTTP requests**. The client can send only the HTTP requests that
are **accepted by the middleman**.

![how to connect: summary](./screenshots/how-to-connect-summary.png)
