# Practice: Fetching Data

Let's practice! We need to **fetch the places stored by the user**. We want to
do that immediately when the `App` component renders for the first time. We want
to avoid an infinite loop, so we'll do this in a `useEffect` hook.

First, let's create a new `http` utility function, `fetchUserPlaces`:

```js
export async function fetchUserPlaces() {
  const response = await fetch('http://localhost:3000/user-places', {
    method: 'GET',
  });

  if (!response.ok) {
    throw new Error('Failed to fetch user places');
  }
  const userPlaces = await response.json();
  return userPlaces;
}
```

Update `App.jsx` as follows:

```jsx
import { useEffect, useRef, useState, useCallback } from 'react';

import Places from './components/Places.jsx';
import Modal from './components/Modal.jsx';
import DeleteConfirmation from './components/DeleteConfirmation.jsx';
import logoImg from './assets/logo.png';
import AvailablePlaces from './components/AvailablePlaces.jsx';
import { fetchUserPlaces, updateUserPlaces } from './http.js';

function App() {
  const selectedPlace = useRef();

  const [userPlaces, setUserPlaces] = useState([]);

  const [modalIsOpen, setModalIsOpen] = useState(false);

  const [isFetching, setIsFetching] = useState(false); // Add 'isFetching' state

  const [errorFetchingUserPlaces, setErrorFetchingUserPlaces] = useState(null); // Add `errorFetchingUserPlaces` state

  const [errorUpdatingPlaces, setErrorUpdatingPlaces] = useState(null);

  // Fetch user places on component load in `useEffect`
  useEffect(() => {
    const getUserPlaces = async () => {
      setIsFetching(true);
      try {
        const userPlacesData = await fetchUserPlaces();
        setUserPlaces(userPlacesData.places);
        setIsFetching(false);
      } catch (err) {
        // ...
        console.log(err.message);
        setErrorFetchingUserPlaces(
          err.message || 'Failed to fetch user places'
        );
      }
      setIsFetching(false);
    };
    getUserPlaces();
  }, []);

  function handleStartRemovePlace(place) {
    setModalIsOpen(true);
    selectedPlace.current = place;
  }

  function handleStopRemovePlace() {
    setModalIsOpen(false);
  }

  async function handleSelectPlace(selectedPlace) {
    setUserPlaces((prevPickedPlaces) => {
      if (!prevPickedPlaces) {
        prevPickedPlaces = [];
      }
      if (prevPickedPlaces.some((place) => place.id === selectedPlace.id)) {
        return prevPickedPlaces;
      }
      return [selectedPlace, ...prevPickedPlaces];
    });

    try {
      await updateUserPlaces([selectedPlace, ...userPlaces]);
    } catch (err) {
      setUserPlaces(userPlaces);
      setErrorUpdatingPlaces({
        message: error.message || 'Failed to update places.',
      });
    }
  }

  const handleRemovePlace = useCallback(
    async function handleRemovePlace() {
      setUserPlaces((prevPickedPlaces) =>
        prevPickedPlaces.filter(
          (place) => place.id !== selectedPlace.current.id
        )
      );

      try {
        await updateUserPlaces(
          userPlaces.filter((place) => place.id !== selectedPlace.current.id)
        );
      } catch (err) {
        setUserPlaces(userPlaces);
        setErrorUpdatingPlaces({
          message: err.message || 'Failed to delete place',
        });
      }

      setModalIsOpen(false);
    },
    [userPlaces]
  );

  function handleError() {
    setErrorUpdatingPlaces(null);
  }

  return (
    <>
      <Modal open={errorUpdatingPlaces} onClose={handleError}>
        {errorUpdatingPlaces && (
          <Error
            title="An Error Occurred"
            message={errorUpdatingPlaces.message}
            onConfirm={handleError}
          />
        )}
      </Modal>
      <Modal open={modalIsOpen} onClose={handleStopRemovePlace}>
        <DeleteConfirmation
          onCancel={handleStopRemovePlace}
          onConfirm={handleRemovePlace}
        />
      </Modal>

      <header>
        <img src={logoImg} alt="Stylized globe" />
        <h1>PlacePicker</h1>
        <p>
          Create your personal collection of places you would like to visit or
          you have visited.
        </p>
      </header>
      <main>
        {
          // Display error output to UI if fetching user places fails
        }
        {errorFetchingUserPlaces && (
          <Error
            title="An Error Occurred"
            message={errorFetchingUserPlaces.message}
          />
        )}
        {!errorFetchingUserPlaces && (
          <Places
            title="I'd like to visit ..."
            fallbackText="Select the places you would like to visit below."
            places={userPlaces}
            onSelectPlace={handleStartRemovePlace}
            isLoading={isFetching}
            loadingText="Fetching your places..."
          />
        )}

        <AvailablePlaces onSelectPlace={handleSelectPlace} />
      </main>
    </>
  );
}

export default App;
```
