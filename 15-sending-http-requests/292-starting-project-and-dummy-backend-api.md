# Starting Project & Dummy Backend API

Download and install the starter project attached to this lesson. Once started,
you should see a non-function "placepicker" application. This will be similar to
the application we used in a previous module. The difference is the **data** for
this application will be stored on a **backend**. The backend API is included in
the starter project.

The backend project specifies the actions that the client can take on the
backend by defining **routes**.
