# Extracting Code and Improving Code Structure

Let's clean up our `AvailablePlaces` code a bit. This is **optional**, but
recommended.

Create a new file next to the `loc.js` file, `http.js`. This is where we will
place the code for making the `HTTP` requests:

```js
export async function fetchAvailablePlaces() {
  const response = await fetch('http://localhost:3000/places');
  if (!response.ok) {
    throw new Error('Failed to fetch places');
  }

  const data = await response.json();
  return data.places;
}
```

We can now leverage the `fetchAvailablePlaces` function anywhere in the
application where the places are needed.

Refactor `AvailablePlaces.jsx` to use the new `fetchAvailablePlaces` function:

```jsx
import { useEffect, useState } from 'react';
import Places from './Places.jsx';
import Error from './Error.jsx';
import { sortPlacesByDistance } from '../loc.js';
import { fetchAvailablePlaces } from '../http.js';

export default function AvailablePlaces({ onSelectPlace }) {
  const [availablePlaces, setAvailablePlaces] = useState([]);
  const [isFetching, setIsFetching] = useState(true);
  const [error, setError] = useState();

  useEffect(() => {
    const fetchPlaces = async () => {
      try {
        const places = await fetchAvailablePlaces();
        navigator.geolocation.getCurrentPosition((position) => {
          const sortedPlaces = sortPlacesByDistance(
            places,
            position.coords.latitude,
            position.coords.longitude
          );
          setAvailablePlaces(sortedPlaces);
          setIsFetching(false); // Call this HERE
        });
      } catch (err) {
        setError(err);
        setIsFetching(false); // AND HERE
      }
      // setIsFetching(false); // Cannot call this here anymore; JS will not wait until the getCurrentPosition callback is done
    };

    fetchPlaces();
  }, []);

  if (error) {
    return (
      <Error
        title="Failed to Fetch Places"
        message={error.message}
        onConfirm={() => {}}
      />
    );
  }
  return (
    <Places
      title="Available Places"
      isLoading={isFetching}
      loadingText="Loading places..."
      places={availablePlaces}
      fallbackText="No places available."
      onSelectPlace={onSelectPlace}
    />
  );
}
```
