// api/new-meetup
import { MongoClient } from 'mongodb';

const handler = async (req, res) => {
  if (req.method === 'POST') {
    const data = req.body;

    const client = await MongoClient.connect(
      `mongodb+srv://dev01:vOppMUzp0A7eTpdc@meetupsappcluster.oy0ecri.mongodb.net/meetups?retryWrites=true&w=majority`
    );

    const db = client.db('meetups');

    const result = await db.collection('meetups').insertOne(data);
    // console.log(result);
    client.close();
    res.status(201).json({ body: 'Meetup inserted' });
  }
};

export default handler;
