import MeetupDetails from '../../components/meetups/MeetupDetails';

const MeetupDetailsPage = (props) => {
  return (
    <MeetupDetails
      title={props.meetupData.title}
      description={props.meetupData.description}
      image={props.meetupData.image}
      alt={props.meetupData.title}
      address={props.meetupData.address}
    />
  );
};

export async function getStaticPaths() {
  return {
    paths: [
      {
        params: {
          meetupId: 'm1',
        },
      },
      {
        params: {
          meetupId: 'm2',
        },
      },
    ],
    fallback: false,
  };
}

export async function getStaticProps(context) {
  const meetupId = context.params.meetupId;

  return {
    props: {
      meetupData: {
        image: '',
        id: meetupId,
        title: 'First Meetup',
        description: 'A first meetup',
        address: '1234 Pine Street',
      },
    },
  };
}

export default MeetupDetailsPage;
