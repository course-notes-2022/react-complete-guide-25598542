import MeetupList from '../components/meetups/MeetupList';

const DUMMY_MEETUPS = [
  {
    id: 'm1',
    title: 'First Meetup',
    image: '',
    address: '1234 Main Street',
    description: 'this is a first meetup!',
  },
  {
    id: 'm2',
    title: 'Second Meetup',
    image: '',
    address: '1235 Main Street',
    description: 'this is a second meetup!',
  },
  {
    id: 'm3',
    title: 'Third Meetup',
    image: '',
    address: '1236 Main Street',
    description: 'this is a third meetup!',
  },
];
const HomePage = (props) => {
  return <MeetupList meetups={props.meetups} />;
};

// export async function getServerSideProps(context) {
//   const req = context.req;
//   const res = context.res;
//   return {
//     props: {
//       meetups: DUMMY_MEETUPS,
//     },
//   };
// }

export async function getStaticProps() {
  return {
    props: {
      meetups: DUMMY_MEETUPS,
    },
    revalidate: 10,
  };
}
export default HomePage;
