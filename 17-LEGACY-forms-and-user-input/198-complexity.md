# What's Complex About Forms?

Forms and inputs can assume differents states. One or more inputs can be
**valid**, **invalid**, or **undetermined**. These states can apply to **each
individual input**, as well as the **overall form state**.

## When to Validate?

Another complexity involves **when to validate** a form input:

- on submission?
- on blur?
- on every keystroke?

![form complexity](./screenshots/form-complexity.png)
