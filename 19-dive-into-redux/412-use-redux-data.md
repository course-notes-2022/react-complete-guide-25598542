# using Redux Data in React Components

Imagine we want to use our new Redux store in the `Counter` component.

To use the store in a component, we need to import the `useSelector` hook from
`react-redux` (note that we could also use `useStore`, but `useSelector` is more
convenient in that it gives us a **specific piece** of the store). `useSelector`
**automatically sets up a subscription to the store**. Changes to the store will
automatically cause the **component function to be re-executed**, so that our
component will continuously receive the **latest version** of the piece of store
state in which it's interested!

Update the `Counter.js` file as follows:

```ts
import { useSelector } from 'react-redux';
import classes from './Counter.module.css';

const Counter = () => {
  const counter = useSelector((state) => state.counter); // `useSelector` takes a function that receives the state, and returns the piece of state we need
  const toggleCounterHandler = () => {};

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      <div className={classes.value}>-- {counter} --</div>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
```

Save changes and view in the browser. Note that we now see `0` in the component
UI.
