# Creating a Redux Store for React

Let's create a new folder in the `src` folder called `store`. Note that this is
not required, but common convention. We'll store all our Redux store logic here.

Create an `index.js` file inside `src/store`. Add the following to the
`index.js` file:

```js
import { createStore } from 'redux';

const initialState = {
  counter: 0,
};

const counterReducer = (state = initialState, action) => {
  if (action.type === increment.type) {
    return {
      counter: state.counter + 1,
    };
  }

  if (action.type === decrement.type) {
    return {
      counter: state.counter - 1,
    };
  }
  return state;
};

const store = createStore(counterReducer);

export const increment = { type: 'increment' };
export const decrement = { type: 'decrement' };

export default store;
```

We've created our store and a reducer function as we did previously. Let's learn
to **connect our React application to our Redux store** in the next lesson.
