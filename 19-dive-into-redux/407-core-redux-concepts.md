# Exploring the Core Redux Concepts

Let's explore Redux basics with a brand-new, _non-React_ project. Create a new
folder with a simple JS file called `redux-demo.js`. Note that you **will** need
node/npm installed.

Initialize a new `npm` project in the directory containing your JS file. Install
Redux with the `npm install redux` command. Add the following code to
`redux-demo.js`:

```js
const redux = require('redux');

// Create reducer function
// A reducer function is a standard JS function
// that:
// 1. Receives the OLD STATE plus (optionally) an ACTION
// 2. Returns the NEW STATE
// 3. Is a **pure function**; no side-effects, same input produces same output
const counterReducer = (
  state = { counter: 0 }, // pass default state to set INITIAL STATE value
  action
) => {
  return {
    counter: state.counter + 1,
  };
};

// Create store
// Pass the reducer function to the store
// to inform the store which functions will
// be updating it
const store = redux.createStore(counterReducer);

// Create a subscriber to the store
// Subscriber is a function that Redux
// will execute whenever the store changes
const counterSubscriber = () => {
  const latestState = store.getState(); // `getState` gives us the latest state snapshot
  console.log(latestState);
};

// Pass a REFERENCE to the store.subscribe method
// (not an INVOCATION)
store.subscribe(counterSubscriber);

// Create and dispatch an action
store.dispatch({ type: 'increment' });
```

Note that we get the following output in the console:

```js
{
  counter: 2;
}
```
