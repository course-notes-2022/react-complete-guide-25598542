# How Redux Works

Now that we know what Redux is, let's learn **how it works**.

## Core Redux Concepts

1. **One**, _central data store_ for your **entire application**.

2. Components **subscribe to the store**. The store **notifies components** when
   state changes.

3. Components **_never_** directly manipulate **store data**.

4. Reducer functions are functions that take an **input** and produce an
   **output**. **Reducer functions** are responsible for **mutating the state**.

5. Components **dispatch "actions"** to **reducer functions** to kick off
   **updates to the state**.

![how redux works](./screenshots/how-redux-works-diagram.png)
