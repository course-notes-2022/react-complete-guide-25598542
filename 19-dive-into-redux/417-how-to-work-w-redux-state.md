# How to Work With Redux State Correctly

Let's talk about the objects we are **returning from our reducers**. We
**always** return a brand-new state snapshot from our reducers that will
**_NOT_** be _merged_ with the existing state object, but will **_REPLACE_** it
(or overwrite it).

**YOU MUST _NEVER_ MUTATE THE STATE OBJECT DIRECTLY IN REDUX!** Doing so can
lead to bugs, unwanted behavior inside your application where the state becomes
unsynced, and make your application hard to debug. **ALWAYS** create a
**BRAND-NEW OBJECT**, i.e. a **COPY** of the state whenever you need to update
data!
