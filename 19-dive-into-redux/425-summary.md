# Summary

In this section, we learned:

- How to set up Redux without React and Redux Toolkit

- Redux Toolkit is the **recommended way** to manage Redux state

- The core redux concepts: Actions, Reducers and the Central Data Store

- We create the store with teh Redux `configureStore` method and a single
  reducer or a map of reducers

- `useSelector` allows us to **read** Redux state

- `useDispatch` allows us to **dispatch actions** to our Redux state

- Redux is an amazing library that **can** replace Context API, but it doesn't
  **have to**

- Context API can lead to deeply-nested `Providers` and hard-to-manage state

- Redux is a 3rd-party library that **does** add an additional dependency and
  therefore makes your application code **larger**
