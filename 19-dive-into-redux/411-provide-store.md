# Providing the Store

To provide the Redux store, we typically go to the **highest level in the
component tree** where we render the **root component**. Make the following
changes to `src/index.js`:

```js
import React from 'react';
import ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';

import './index.css';
import App from './App';
import store from './store';

const root = ReactDOM.createRoot(document.getElementById('root'));
root.render(
  <Provider store={store}>
    <App />
  </Provider>
);
```

Now, our store is available to all components in our application. They can read
the store and dispatch actions.

You do not _have_ to wrap the root component in the `Provider`, but note that
only those components wrapped by `Provider` will have access to the Redux store.
