# Dispatching Actions from Inside Components

Let's learn to leverage the `useDispatch` hook from `react-redux` to dispatch
actions from inside our components. Make the following updates to `Counter.js`:

```js
import { useDispatch, useSelector } from 'react-redux';
import { increment, decrement } from '../store';
import classes from './Counter.module.css';

const Counter = () => {
  const counter = useSelector((state) => state.counter); // `useSelector` takes a function that receives the state, and returns the piece of state we need

  const dispatch = useDispatch(); // `useDispatch` gives us a function we can call to dispatch an action against our Redux store

  const toggleCounterHandler = () => {};

  const incrementCounter = () => {
    dispatch(increment);
  };

  const decrementCounter = () => {
    dispatch(decrement);
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      <div className={classes.value}>-- {counter} --</div>
      <button onClick={incrementCounter}>Increment</button>
      <button onClick={decrementCounter}>Decrement</button>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
```

Save changes and refresh. Note that we can now **increment and decrement** the
value of `counter` by clicking the buttons in the UI!
