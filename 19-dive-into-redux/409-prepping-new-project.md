# Preparing a New Project

Let's now apply what we've learned about Redux to a React project. _Note:_ You
can use Redux in _any_ Javascript project; it's not limited to React!

Unzip the file attached to the course lecture and install the project
dependencies. Install the following dependencies as well:

> `npm install redux react-redux`

`react-redux` simplifies connecting React components to Redux.
