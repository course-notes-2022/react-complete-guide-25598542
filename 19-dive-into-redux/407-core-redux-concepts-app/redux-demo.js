const redux = require('redux');

// Create reducer function
// A reducer function is a standard JS function
// that:
// 1. Receives the OLD STATE plus (optionally) an ACTION
// 2. Returns the NEW STATE
// 3. Is a **pure function**; no side-effects, same input produces same output
const counterReducer = (
  state = { counter: 0 }, // pass default state to set INITIAL STATE value
  action
) => {
  if (action.type === 'increment') {
    return {
      counter: state.counter + 1,
    };
  }
  if (action.type === 'decrement') {
    return {
      counter: state.counter - 1,
    };
  }
};

// Create store
// Pass the reducer function to the store
// to inform the store which functions will
// be updating it
const store = redux.createStore(counterReducer);

// Create a subscriber to the store
// Subscriber is a function that Redux
// will execute whenever the store changes
const counterSubscriber = () => {
  const latestState = store.getState(); // `getState` gives us the latest state snapshot
  console.log(latestState);
};

// Pass a REFERENCE to the store.subscribe method
// (not an INVOCATION)
store.subscribe(counterSubscriber);

// Create and dispatch an action
store.dispatch({ type: 'increment' });
store.dispatch({ type: 'decrement' });
