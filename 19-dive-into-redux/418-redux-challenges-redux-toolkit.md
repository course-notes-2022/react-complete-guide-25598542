# Redux Challenges and Intro to Redux Toolkit

The more complex our projects become, the more difficult it can become to use
Redux correctly. There is a (slightly) easier way of using Redux!

Let's look at a few potential problems that we may face if our application
continues to grow:

## Problem 1: Action Type Typos

Currently, we have to be very careful when typing our action identifiers. We're
using plain strings, and a **typo** in the identifier could lead to
unpredicatable results in our application.

## Problem 2: The Amount of Data

The bigger our state objects get, we must copy and keep **larger objects** in
our reducer and the \*\*larger our reducer function gets.

## Problem 3: Respecting State Immutability

We have to ensure that we are not **mutating state directly**. As our state
object grows, it becomes easier to violate this rule by mistake (especially with
nested objects or arrays).

We _could_ implement solutions to **all these problems** on our own:

- Using **constants** for action types
- Split our reducer function into multiple reducers
- Use a 3rd-party package to assist with copying state
- etc.

However, the **Redux Toolkit** library is available as an extra package that
makes working with Redux simpler. We'll learn about Redux Toolkit starting in
the next lecture.
