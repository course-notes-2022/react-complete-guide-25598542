# Attaching Payloads to Actions

Often we want to dispatch actions that carry some **data**. Redux provides us
the ability to do so easily.

Refactor `src/store/index.js` to define a new `increase` action, and to handle
that action in the `counterReducer` function:

```js
import { createStore } from 'redux';

const initialState = {
  counter: 0,
};

export const increment = () => {
  return { type: 'increment' };
};
export const decrement = () => {
  return { type: 'decrement' };
};
export const increase = (amount = 0) => ({ type: 'increase', value: amount });

const counterReducer = (state = initialState, action) => {
  if (action.type === 'increment') {
    return {
      counter: state.counter + 1,
    };
  }

  if (action.type === 'decrement') {
    return {
      counter: state.counter - 1,
    };
  }

  if (action.type === 'increase') {
    return {
      counter: state.counter + action.value,
    };
  }
  return state;
};

const store = createStore(counterReducer);

export default store;
```

Update the `Counter.js` component to now dispatch the new action on click:

```js
import { useDispatch, useSelector } from 'react-redux';
import { increment, decrement, increase } from '../store';
import classes from './Counter.module.css';

const Counter = () => {
  const counter = useSelector((state) => state.counter); // `useSelector` takes a function that receives the state, and returns the piece of state we need

  const dispatch = useDispatch(); // `useDispatch` gives us a function we can call to dispatch an action against our Redux store

  const toggleCounterHandler = () => {};

  const incrementCounter = () => {
    dispatch(increment());
  };

  const decrementCounter = () => {
    dispatch(decrement());
  };

  const increaseCounterBy = (amount) => {
    dispatch(increase(amount));
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      <div className={classes.value}>-- {counter} --</div>
      <button onClick={incrementCounter}>Increment</button>
      <button onClick={() => increaseCounterBy(5)}>Increment by 5</button>
      <button onClick={decrementCounter}>Decrement</button>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
```
