# Another Look at State in React Apps

**Redux** is a state-management system for cross-component or app-wide state.

We've already used state a lot in this course, with `useState` or `useReducer`.
These hooks exist to allow us to manage data that changes, and where changes to
that data lead to the **UI being updated**.

We can **split the definition** of "state" into 3 broad categories:

1. **Local** state: State that belongs to a single component, e.g. listening to
   user input on an input field or toggling a "show more details" field. Should
   be managed **inside the component** via `useState` or `useReducer`.

2. **Cross-component** state: State that might affect multiple components, e.g.
   open/closed state of a modal overlay. Requires "prop drilling", i.e. passing
   props and/or functions across multiple components.

3. **App-wide** state: State affecting the **entire app**, e.g. user
   authentication status or chosen theme. We _can_ manage this using `useState`
   and `useReducer`, and passing around props, but this can become
   **cumbersome**. We learned how to use the Context API to make this simpler.

**Redux** solves the same problem as Context.
