# Connecting Redux Toolkit State

Let's learn how to connect to our new Redux Toolkit-enabled store.

Update `src/store/index.js` as follows:

```js
import {
  createSlice,
  configureStore, // `configureStore` creates a store and makes merging multiple reducers easier
} from '@reduxjs/toolkit';

const initialState = {
  counter: 0,
  showCounter: true,
};

const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    increment(state) {
      state.counter++;
    },
    decrement(state) {
      state.counter--;
    },
    increase(state, action) {
      state.counter += action.payload;
    },
    toggleCounter(state) {
      state.showCounter = !state.showCounter;
    },
  },
});

const store = configureStore({
  // configureStore takes a config object
  reducer: counterSlice.reducer, // `reducer` can be a single reducer function OR a MAP of reducer functions: { counter: counterSlice.reducer}
});

export default store;
```

Note the following:

- We can remove the custom reducers that we wrote previously, as we won't be
  needing them any longer.

- To use our slice, we need to get the return value of `createSlice`.

- We are using the `configureStore` method of Redux Toolkit in place of the
  previous `createStore` method. `configureStore` takes a configuration object
  that contains a `reducer` (note the singular) attribute. `reducer` can be a
  **single reducer function**, or an **object** in the case that we have
  **multiple state slices** we want to manage.
