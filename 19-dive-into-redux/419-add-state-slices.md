# Adding State Slices

Let's get started with Redux Toolkit. Install the following `npm` package:

> `npm install @reduxjs/toolkit`

After this command, we can **unistall** Redux itself, as it is **included** in
Redux Toolkit. Remove `redux` from the `package.json` file and restart the dev
server.

## Using Redux Toolkit

As mentioned, Redux Toolkit **simplifies working with Redux stores**. Let's look
at how it does this.

Open the `src/store/index.js` file. Add the following import:

```js
import { createSlice } from '@reduxjs/toolkit';
```

`createSlice` allows us to create different "slices" of global state. We could
potentially create these slices in **different files** to make our state more
maintainable.

Update `src/store/index.js` as follows:

```js
import { createStore } from 'redux';
import { createSlice } from '@reduxjs/toolkit';

const initialState = {
  counter: 0,
  showCounter: true,
};

createSlice(
  // Prepare a "slice" of global state
  {
    name: 'counter', // Every slice needs a name
    initialState, // set the initial state object
    reducers: {
      // map of all reducer functions required by this state slice
      increment(state) {
        state.counter++; // We CANNOT accidentally mutate direct state in Redux Toolkit
      },
      decrement(state) {
        state.counter--;
      },
      increase(state, action) {
        state.counter += action.payload;
      },
      toggleCounter(state) {
        state.showCounter = !state.showCounter;
      },
    },
  }
);

// Omitted...
```

**Note the following**:

- Every method in the `reducers` property of the `counter` slice is a **reducer
  function** that will be invoked by Redux in response to a **dispatched
  action**.

- The reducers receive the `state` and an optional `action`, just like the
  custom reducers we wrote previously.

- Although it **appears that we are _directly mutating the state_** in our
  reducer methods, **_that is not the case_**! Redux Toolkit leverages a library
  called `immer` that copies our state for us behind the scenes and **ensures we
  are still respecting the rules of immutatability**.
