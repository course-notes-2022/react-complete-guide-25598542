# Working with Multiple Slices

We have some additional components outside of the `Counter` component. Let's use
them now.

Modify `App.js` as follows:

```js
import { Fragment } from 'react';
import Auth from './components/Auth';
import Counter from './components/Counter';
import Header from './components/Header';

function App() {
  return (
    <Fragment>
      <Header />
      <Auth />
      <Counter />
    </Fragment>
  );
}

export default App;
```

We want to add functionality to the `Auth` component that switches us to a
"login" mode when the user clicks "Login". We also want to swap the `Login`
component for the `UserProfile` component and update the `Header` button. All of
this requires **application-wide state** that matters to **multiple
components**. This is a perfect example of state that we can manage with Redux.

## Where Do We Add our Authentication Data?

The authentication status has nothing to do with the counter status. It's best
to keep the state slices **separate**.

Let's now refactor our store to define a **separate slice** for our
authentication state, as well as the required **reducers and actions**:

```js
import { createSlice, configureStore } from '@reduxjs/toolkit';

const initialCounterState = {
  counter: 0,
  showCounter: true,
};

const counterSlice = createSlice({
  name: 'counter',
  initialState: initialCounterState, // MUST refactor to `initialState: initStateName`
  reducers: {
    increment(state) {
      state.counter++;
    },
    decrement(state) {
      state.counter--;
    },
    increase(state, action) {
      state.counter += action.payload;
    },
    toggleCounter(state) {
      state.showCounter = !state.showCounter;
    },
  },
});

export const counterActions = counterSlice.actions;

// Add new state slice for AUTHENTICATION STATE

const initialAuthState = {
  isAuthenticated: false,
};

const authSlice = createSlice({
  name: 'authentication',
  initialState: initialAuthState, // MUST refactor to `initialState: initStateName`
  reducers: {
    login(state) {
      state.isAuthenticated = true;
    },

    logout(state) {
      state.isAuthenticated = false;
    },
  },
});

export const authActions = authSlice.actions;

const store = configureStore({
  reducer: {
    counter: counterSlice.reducer,
    auth: authSlice.reducer,
  },
});

export default store;
```

Make the following changes in `Counter.js`:

```js
import { useDispatch, useSelector } from 'react-redux';
import { counterActions } from '../store';
import classes from './Counter.module.css';

const Counter = () => {
  // Refactor to return `state.counter.counter`
  // to reflect new structure of state object
  const counter = useSelector((state) => state.counter.counter);

  // Refactor to return `state.counter.showCounter`
  // to reflect new structure of state object
  const showCounter = useSelector((state) => state.counter.showCounter);

  // Omitted...
};

export default Counter;
```

Save changes and refresh the browser.

## Challenge

Try to implement the following functionality on your own:

1. Use the `authState` to conditionally show the `Auth` or `UserProfile` in `App

2. Use the `authState` to conditionally show the "My Products", "My Sales", and
   "Logout" buttons in the `Header`

3. Dispatch `login` and `logout` actions in the appropriate places
