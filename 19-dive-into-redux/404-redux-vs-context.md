# Redux vs. React Context API

Do we need **both** Redux **and** the Context API?

## Potential Disadvantages of Context API

Context API has some potential **disadvantages**, that _may or may not be
relevant for your application_:

1. Context can have a complex setup and management cost, depending on the
   application size. Large enterprise apps can become cumbersome, with multiple
   nested providers:

```ts
return (
  <AuthContextProvider>
    <ThemeContextProvider>
      <UIInteractionContextProvider>
        <MultiStepFormContextProvider>
          <UserRegistration />
        </MultiStepFormContextProvider>
      </UIInteractionContextProvider>
    </ThemeContextProvider>
  </AuthContextProvider>
);
```

_or_, a **large context object that cares about multiple pieces of state**,
which can itself become complex.

2. Performance. Context API is recommended for low-frequency updates (like
   locale, theme, authentication, etc.), _not optimized_ for data that **changes
   frequently**.

_Note: You **can** use Redux **and** Context API at the same time_.
