# More Redux Basics

We're currently dispatching an action with the `type` property equal to
"increment". Typically, we want to do **different state changes** based on the
**different `type` of the action**.

Refactor `counterReducer` as follows:

```js
const counterReducer = (state = { counter: 0 }, action) => {
  // Add conditional statement for `increment` action type
  if (action.type === 'increment') {
    return {
      counter: state.counter + 1,
    };
  }
  return state;
};
```

Note that if we run the code again now, we get:

```js
{
  counter: 1;
}
```

This is because the counter is now incremented **only if the `increment` action
is dispatched**. Previously it was being incremented **both at store creation
and when we dispatched the `increment` action explicitly**. Redux invokes all
reducers for the state once at the time of store creation.
