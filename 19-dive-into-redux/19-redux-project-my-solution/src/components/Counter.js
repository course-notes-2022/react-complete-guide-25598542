import { useDispatch, useSelector } from 'react-redux';
import { counterActions } from '../store/counter';
import classes from './Counter.module.css';

const Counter = () => {
  const counter = useSelector((state) => state.counter.counter); // `useSelector` takes a function that receives the state, and returns the piece of state we need

  const showCounter = useSelector((state) => state.counter.showCounter);

  const dispatch = useDispatch(); // `useDispatch` gives us a function we can call to dispatch an action against our Redux store

  const toggleCounterHandler = () => {
    dispatch(counterActions.toggleCounter());
  };

  const incrementCounter = () => {
    dispatch(counterActions.increment());
  };

  const decrementCounter = () => {
    dispatch(counterActions.decrement());
  };

  const increaseCounterBy = (amount) => {
    dispatch(counterActions.increase(amount));
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      {showCounter && <div className={classes.value}>-- {counter} --</div>}
      <button onClick={incrementCounter}>Increment</button>
      <button onClick={() => increaseCounterBy(5)}>Increment by 5</button>
      <button onClick={decrementCounter}>Decrement</button>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
