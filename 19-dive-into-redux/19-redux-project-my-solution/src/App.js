import { useSelector } from 'react-redux';
import { Fragment } from 'react';
import Auth from './components/Auth';
// import Counter from './components/Counter';
import Header from './components/Header';
import UserProfle from './components/UserProfile';

function App() {
  const isAuthenticated = useSelector((state) => state.auth.isAuthenticated);
  return (
    <Fragment>
      <Header />
      {!isAuthenticated && <Auth />}
      {isAuthenticated && <UserProfle />}
      {/* <Counter /> */}
    </Fragment>
  );
}

export default App;
