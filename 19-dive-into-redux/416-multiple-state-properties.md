# Working with Multiple State Properties

Until now, we've learned to work with a **single value** on the store using
Redux. Let's imagine that the counter is of interest to **other components** as
well, and that we want to modify a Redux store property that controls the
**display of the counter** whenever a button in the `Counter` component is
clicked.

To do this, we need to add a new piece of state to our Redux store. Modify the
`/src/store/index.js` file as follows:

```js
import { createStore } from 'redux';

const initialState = {
  counter: 0,
  showCounter: true, // add the `showCounter` attr to initial state
};

export const increment = () => {
  return { type: 'increment' };
};
export const decrement = () => {
  return { type: 'decrement' };
};
export const increase = (amount = 0) => ({ type: 'increase', value: amount });

export const toggle = () => ({ type: 'toggle' }); // Add new `toggle` action

const counterReducer = (state = initialState, action) => {
  if (action.type === 'increment') {
    return {
      ...state, // Use the spread operator to spread ALL the current state properties
      // to the state object returned by reducers
      counter: state.counter + 1,
    };
  }

  if (action.type === 'decrement') {
    return {
      ...state,
      counter: state.counter - 1,
    };
  }

  if (action.type === 'increase') {
    return {
      ...state,
      counter: state.counter + action.value,
    };
  }

  if (action.type === 'toggle') {
    // Add new acttion for toggling display of counter
    return {
      ...state,
      showCounter: !state.showCounter,
    };
  }

  return state;
};

const store = createStore(counterReducer);

export default store;
```

Note that we **still must set the `showCounter` property**, because our reducers
are returning the **entire state object**.

Refactor `Counter.js` as follows:

```js
import { useDispatch, useSelector } from 'react-redux';
import { increment, decrement, increase, toggle } from '../store';
import classes from './Counter.module.css';

const Counter = () => {
  const counter = useSelector((state) => state.counter);

  const showCounter = useSelector((state) => state.showCounter); // Select `showCounter` piece of state

  const dispatch = useDispatch();

  const toggleCounterHandler = () => {
    dispatch(toggle()); // dispatch the 'toggle' action in `toggleCounterHandler`
  };

  const incrementCounter = () => {
    dispatch(increment());
  };

  const decrementCounter = () => {
    dispatch(decrement());
  };

  const increaseCounterBy = (amount) => {
    dispatch(increase(amount));
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      {
        // Display counter conditionally based on
        // value of `showCounter` state attribute
      }
      {showCounter && <div className={classes.value}> {counter}</div>}
      <button onClick={incrementCounter}>Increment</button>
      <button onClick={() => increaseCounterBy(5)}>Increment by 5</button>
      <button onClick={decrementCounter}>Decrement</button>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
```
