# Splitting our Code

To finish our module, let's split up the `src/store/index.js` file. In a typical
React app, our store can become pretty large. Since we are using Redux Toolkit,
and it's so easy to create **slices** of our state, it may make sense to put
every store slice into its **own file**.

Let's make a new file in `src/store/counter.js`. We'll keep our counter-related
store slices in there:

```js
import { createSlice } from '@reduxjs/toolkit';

const initialCounterState = {
  counter: 0,
  showCounter: true,
};

const counterSlice = createSlice({
  name: 'counter',
  initialState: initialCounterState,
  reducers: {
    increment(state) {
      state.counter++;
    },
    decrement(state) {
      state.counter--;
    },
    increase(state, action) {
      state.counter += action.payload; // { type: SOME_UNIQUE_ID, payload: 10 }
    },
    toggleCounter(state) {
      state.showCounter = !state.showCounter;
    },
  },
});

export const counterActions = counterSlice.actions;
export default counterSlice.reducer;
```

Create a new file `src/store/auth.js` and add the following:

```js
import { createSlice } from '@reduxjs/toolkit';

const initialAuthState = {
  isAuthenticated: false,
};

const authSlice = createSlice({
  name: 'auth',
  initialState: initialAuthState,
  reducers: {
    login(state) {
      state.isAuthenticated = true;
    },

    logout(state) {
      state.isAuthenticated = false;
    },
  },
});

export const authActions = authSlice.actions;
export default authSlice.reducer;
```

Now we can refactor `index.js` as follows:

```js
import { configureStore } from '@reduxjs/toolkit';
import authReducer from './auth';
import counterReducer from './counter';

const store = configureStore({
  reducer: {
    counter: counterReducer,
    auth: authReducer,
  },
});

export default store;
```

Fix the necessary import statements and refresh. The application should work as
before. In larger applications, splitting our code in this way makes our state
management files **leaner and easier to maintain**.
