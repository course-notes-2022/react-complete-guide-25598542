# Migrating Everything to Redux Toolkit

For **dispatching actions** `createSlice` **automatically creates properties
with unique identifiers** for our actions.

These properties are called **action creators**. They are **functions that we
can call** to generate the **action objects** with a structure as below:

```json
{
  "type": "some-unique-identifier"
}
```

Refactor `src/store/index.js` as follows:

```js
import { createSlice, configureStore } from '@reduxjs/toolkit';

const initialState = {
  counter: 0,
  showCounter: true,
};

const counterSlice = createSlice({
  name: 'counter',
  initialState,
  reducers: {
    // Add remaining reducers for counter slice
    increment(state) {
      state.counter++;
    },
    decrement(state) {
      state.counter--;
    },
    increase(state, action) {
      state.counter += action.payload; // { type: SOME_UNIQUE_ID, payload: 10 }
    },
    toggleCounter(state) {
      state.showCounter = !state.showCounter;
    },
  },
});

// Export the counterActions object
export const counterActions = counterSlice.actions;

const store = configureStore({
  reducer: counterSlice.reducer,
});

export default store;
```

Note the following:

- For actions that receive **data**, the `payload` property is **required**:

```js
{ type: SOME_UNIQUE_ID, payload: 10 }
```

Update `Counter.js`:

```js
import { useDispatch, useSelector } from 'react-redux';
import { counterActions } from '../store'; // Import `counterActions`
import classes from './Counter.module.css';

const Counter = () => {
  const counter = useSelector((state) => state.counter);

  const showCounter = useSelector((state) => state.showCounter);

  const dispatch = useDispatch();

  // Refactor handler methods to use the
  // action creators provided by `counterActions`
  const toggleCounterHandler = () => {
    dispatch(counterActions.toggleCounter());
  };

  const incrementCounter = () => {
    dispatch(counterActions.increment());
  };

  const decrementCounter = () => {
    dispatch(counterActions.decrement());
  };

  const increaseCounterBy = (amount) => {
    dispatch(counterActions.increase(5));
  };

  return (
    <main className={classes.counter}>
      <h1>Redux Counter</h1>
      {showCounter && <div className={classes.value}>-- {counter} --</div>}
      <button onClick={incrementCounter}>Increment</button>
      <button onClick={() => increaseCounterBy(5)}>Increment by 5</button>
      <button onClick={decrementCounter}>Decrement</button>
      <button onClick={toggleCounterHandler}>Toggle Counter</button>
    </main>
  );
};

export default Counter;
```

Save changes and refresh. Note that we still get the expected behavior in the
browser.
