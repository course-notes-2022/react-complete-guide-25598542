# Module Intro

In this section we'll learn a popular 3rd-party React library: the **Redux**
library. We'll learn:

- What is Redux? Why would you use it?
- Redux basics and Using Redux with React
- Working with **Redux Toolkit**
