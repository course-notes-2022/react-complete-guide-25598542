import React from 'react';

const AuthContext = React.createContext({
  isLoggedIn: true,
  logoutHandler: () => {},
});

export default AuthContext;
