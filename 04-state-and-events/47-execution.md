# How Component Functions are Executed

To reiterate, components in React are **just functions that return JSX**. By
using our components in other components, we are **making React aware** of those
functions. React calls all component functions until there are no more functions
left. It then evaluates the overall **result**, and translates that into
instructions that render **elements** into the DOM.

What's the problem? **React only does this once**, when the application first
loads. We need a way of telling React when something **changes**, and the
instructions need to be **re-evaluated**. That's where **state** comes in!
