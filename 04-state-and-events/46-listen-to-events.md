# Listening to Events and Working with Event Handlers

In React, we have **full access to all HTML DOM events** exposed by Javascript
for a given element. There is a React prop equivalent we can add to the element
in the template, and attach **listeners** to that prop:

```javascript
const ExpenseItem = (props) => {
  const clickHandler = () => {
    console.log('clicked!!!');
  };
  return (
    <Card className="expense-item">
      <ExpenseDate date={props.date} />
      <div className="expense-item__description">
        <h2>{props.title}</h2>
        <div className="expense-item__price">${props.amount}</div>
      </div>
      <button
        {/* onClick={() => {
          console.log('clicked');
        }} */}
        onClick={clickHandler}
      >
        Change Title
      </button>
    </Card>
  );
};
```
