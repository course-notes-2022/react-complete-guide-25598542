# Intro

In this section, we'll look at **user interactions** and handling events, as
well as **state** in React:

- Handling Events
- Updating the UI and working with "State"
- A closer look at components and state
