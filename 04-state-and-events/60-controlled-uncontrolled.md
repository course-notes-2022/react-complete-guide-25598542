# Controlled vs. Uncontrolled Components & Stateless vs. Stateful Components

**Controlled components** are components in which **one or more values** are
both **passed _to_ a parent component**, and **received _from_ a parent
component** via `props`. Both the value and changes to the value are handled in
the **parent** component.

**Stateful** components manage some state. **Stateless** (also called
"presentational" or "dumb") components do **not** manage any state. They are
simply there to present data. Typically you will want to have **more** stateless
components than stateful components in your React applications.
