# A Closer Look at the `useState` Hook

`useState` registers a state value for **a specific component instance**.
Component instances do **not** share state in React.

The initial value we give to a state variable is considered **only on the intial
component render**. React manages the value for us and tracks changes when the
mutation function is called, and the component is re-rendered.
