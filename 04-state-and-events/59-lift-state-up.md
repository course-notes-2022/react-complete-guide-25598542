# Lifting State Up

It often happens that we generate data in one React component, but have to use
it in another component. Consider the following example where we generate some
state in the `<NewExpense/>` component, but need that state in `<Expenses/>`:

![lift state up 1](./screenshots/lift-state-up1.png)

The two component **can't communicate** because they are **siblings**. We can
communicate only between **parents** and **children**.

The solution is to **lift state up**, i.e. pass the state from child to parent,
to the **closest component that is a parent of both**, in this case the `<App/>`
component:

![lift state up 2](./screenshots/lift-state-up2.png)

We're already doing this by calling the parent function **inside the child** to
pass the child data **up to the parent** and **down again to where it's
needed**:

![lift state up 3](./screenshots/lift-state-up3.png)
