# Listening to User Input

Let's listen to input events and respond to user input:

```javascript
import './ExpenseForm.css';

const ExpenseForm = () => {
  const titleChangeHandler = (event) => {
    console.log(event);
  };
  return (
    <form>
        <div className="new-expense__control">
          <label>Title</label>
          <input type="text" onChange={titleChangeHandler} />
        </div>
      </div>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Amount</label>
          <input type="number" min="0.01" step="0.01" />
        </div>
      </div>
      <div className="new-expense__controls">
        <div className="new-expense__control">
          <label>Date</label>
          <input type="date" min="2019-01-01" max="2022-12-31" />
        </div>
      </div>
      <div className="new-expense__actions">
        <button type="submit">Submit</button>
      </div>
    </form>
  );
};
```

Note that you don't **have** to call an anonymous function to invoke
`titleChangeHandler` and get an event object (passing the **reference** is
enough).
