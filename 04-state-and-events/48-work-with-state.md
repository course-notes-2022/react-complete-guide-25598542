# Working with State

To tell React that an update has occurred and the component function should run
again, we use the `useState` hook:

```javascript
import { useState } from 'react';
// ...

const ExpenseItem = (props) => {
  const [title, setTitle] = useState(props.title);

  return (
    <Card className="expense-item">
      <ExpenseDate date={props.date} />
      <div className="expense-item__description">
        <h2>{title}</h2>
        <div className="expense-item__price">${props.amount}</div>
      </div>
      <button
        onClick={() => {
          setTitle('Updated!');
        }}
      >
        Change Title
      </button>
    </Card>
  );
};
```

`useState` returns **two** values:

- a **pointer** to a value managed by React
- a **function** that mutates the value

When the mutating function is invoked, **the component function is also invoked
again**. Any changes it detects are **drawn onto the screen**, thus updating the
DOM.

## React Hooks

Hooks are functions provided by the React library. Hooks have certain rules:

- Hooks must be called **inside** component functions
- Hooks must start with the word `use`
- Hooks must not be called inside any **nested** functions

**Bottom line: If you have changes that need to be reflected on screen/in the
DOM, use `useState`!**
