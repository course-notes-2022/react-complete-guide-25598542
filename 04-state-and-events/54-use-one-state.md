# Using One State Instead (and Which is Better?)

There **is** an alternative to having multiple slices of state in a React
component. We can **also** use **one state** by calling `useState` **once** with
an **object** representing the **entire** component state:

```javascript
const ExpenseForm = () => {
  const [userInput, setUserInput] = useState({
    enteredTitle: '',
    enteredAmount: '',
    enteredDate: '',
  });
  // ...

  const titleChangeHandler = (event) => {
    setUserInput({
      ...userInput,
      enteredTitle: event.target.value,
    });
  };
};
```

Note that if you do this, you **must** ensure to update the **entire object**
whenever **any attribute changes**. One way to do this is to use the **spread
operator**.
