# Child-to-Parent Communication

We need to **pass** the data generated in `ExpenseForm` to its **parent**,
`AppComponent`. How can we pass data **up** to a parent from a child?

## Passing a Function as `props`

We've already seen how to pass `props` from a parent to a child component. A
prop can be of any data type, **including a function**. By passing a function as
a prop from a parent to a child, we can **allow the child to call its parent
function**, and **pass any data we wish from child to parent**:

```javascript
// App
const App = () => {
  const addExpensehandler = (expense) => {
    console.log('in App.js');
    console.log(expense);
  };
  // ...

  return (
    <div>
      <NewExpense onAddExpense={addExpenseHandler} />
      <Expenses items={expenses} />
    </div>
  );

// ExpenseForm
const submitHandler = (event) => {
  event.preventDefault();

  const expenseData = {
    title: enteredTitle,
    amount: enteredAmount,
    date: new Date(enteredDate),
  };

  props.onSaveExpenseData(expenseData); // call onSaveExpenseData prop

  setEnteredTitle('');
  setEnteredAmount('');
  setEnteredDate('');
};

// NewExpense
const NewExpense = (props;) => {
  const saveExpenseDataHandler = (enteredExpenseData) => {
    const expenseData = { ...enteredExpenseData, id: Math.random().toString() };
    props.onAddExpense(expenseData)
  };

  return (
    <div className="new-expense">
      <ExpenseForm onSaveExpenseData={saveExpenseDataHandler} />
    </div>
  );
};
```
