# Updating State that Depends on Previous State

It's a bad practice to update state that depends on a previous state like this:

```javascript
const ExpenseForm = () => {
  const [userInput, setUserInput] = useState({
    enteredTitle: '',
    enteredAmount: '',
    enteredDate: '',
  });
  // ...

  const titleChangeHandler = (event) => {
    setUserInput({
      ...userInput, // NO!
      enteredTitle: event.target.value,
    });
  };
};
```

Whenever you update state the **depends on previous state**, you should use an
alternative form of `useState` that accepts a **function**:

```javascript
const ExpenseForm = () => {
  const [userInput, setUserInput] = useState({
    enteredTitle: '',
    enteredAmount: '',
    enteredDate: '',
  });
  // ...

  const titleChangeHandler = (event) => {
    // YES!
    setUserInput((prevState) => {
      return { ...prevState, enteredTitle: event.target.value };
    });
  };
};
```

**Why?** If you use the function method, React **guarantees** that the state
used will always be the latest snapshot of the state, _after_ any scheduled
updates have completed.
