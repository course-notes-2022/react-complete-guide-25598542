// `useLoaderData` is a hook provided by
// react-router-dom that we can use to get
// access to the "closest loader data"
import { Await, defer, useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';
import { Suspense } from 'react';

async function loadEvents() {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    throw new Response(JSON.stringify({ message: 'Could not fetch events.' }), {
      status: 500,
    });
  } else {
    const responseData = await response.json();
    return responseData.events;
  }
}

export const loader = () => {
  return defer(
    // `defer` accepts an argument that is an OBJECT
    // that contains all the HTTP request functions
    // that take place on this page
    {
      events: loadEvents(), // EXECUTE the function and store the return value on `events`
    }
  );
};

function EventsPage() {
  const { events } = useLoaderData();

  /*
  The `Await` component will wait until the data passed by
  the `resolve` prop
  */
  return (
    <Suspense fallback={<p>Loading...</p>}>
      <Await resolve={events}>
        {(loadedEvents) => <EventsList events={loadedEvents} />}
      </Await>
    </Suspense>
  );
}

export default EventsPage;
