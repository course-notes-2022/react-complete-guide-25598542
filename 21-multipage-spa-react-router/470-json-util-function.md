# The `json()` Utility Function

We often want to construct `Response` objects in our `loader` functions as we've
done in the `Events` component:

```js
export const loader = async () => {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    throw new Response(JSON.stringify({ message: 'Could not fetch events.' }), {
      status: 500,
    });
  } else {
    return response;
  }
};
```

We _could_ continue to do it manually, but `react-router-dom` provides us with a
utility method to simplify this process: the `json()` utility:

```js
export const loader = async () => {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    return json({ message: 'Could not fetch events' }, { status: 500 });
  } else {
    return response;
  }
};
```

`json` automatically constructs and returns a `Response` object. It's less
verbose, and also allows us to use the response as an **object** without having
to parse it from `json` using `JSON.parse()`:

```js
import { useRouteError } from 'react-router-dom';
import MainNavigation from '../components/MainNavigation';
import PageContent from '../components/PageContent';

function ErrorPage() {
  const err = useRouteError();

  let title = 'An error occurred!';
  let message = 'Something went wrong';

  if (err.status && err.status === 500) {
    message = err.data.message; // No need to JSON.parse anymore
  }

  if (err.status === 404) {
    title = 'Not Found!';
    message = 'Could not find resource or page';
  }
  return (
    <>
      <MainNavigation />
      <PageContent title={title}>
        <p>{message}</p>
      </PageContent>
    </>
  );
}

export default ErrorPage;
```
