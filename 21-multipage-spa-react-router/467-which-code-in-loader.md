# Which Kind of Code Goes into `loader()`s?

Note that the code defined in `loader` executes in the **browser**, not on the
**server**. This means that we can access **any API that exists on the
browser**: `fetch()`, local/session storage, etc.

We **cannot** execute **hooks** inside `loader`! The rules of hooks still apply:
they must be executed at the top-level of React components **only**.

That's the only limitation of using `loader` functions!
