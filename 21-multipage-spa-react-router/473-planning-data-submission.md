# Planning Data Submission

Let's learn to **send data to the backend**.

Refactor the `NewEvent.js` file to display the `EventForm`:

```js
import EventForm from '../components/EventForm';
function NewEventPage() {
  return <EventForm />;
}

export default NewEventPage;
```

How can we send data to the backend from our form?

Just as we can add `loaders` to **load** data, we can use **actions** to **send
data** with `react-router-dom`. Let's learn how to do that in the upcoming
lessons!
