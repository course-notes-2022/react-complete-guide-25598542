# Error-Handling with Custom Errors

Let's learn how to handle errors in our `loader` functions. One simple way would
be to return a **custom error object** from our `loader` in the event of an
error, and then check for that error object in the calling function and handle
it there:

```js
import { useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';

export const loader = async () => {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    // Return error object
    return { isError: true, message: 'Could not fetch events' };
  } else {
    return response;
  }
};

function EventsPage() {
  const data = useLoaderData();
  // Handle custom error object
  if (data.isError) {
    return <p>{data.message}</p>;
  }
  const events = data.events;

  return <EventsList events={events} />;
}

export default EventsPage;
```

Alternatively, we could **throw** an error from our `loader`:

```js

```

When an error gets thrown in a `loader`, `react-router-dom` renders the closest
`errorElement` defined in the `router`. We saw `errorElement` in action when we
displayed a **fallback page for invalid/non-existent routes** earlier in this
module, but that is only **one** of its use cases. `errorElement` will be shown
to the screen **whenever an error occurs in any route-relate code**, _including
loaders_.

Create a new `src/pages/ErrorPage.js` file, with the following content:

```js
function ErrorPage() {
  return <h1>An error occurred!</h1>;
}

export default ErrorPage;
```

Refactor `Events.js` to throw an `Error` inside the loader if the data fetch
fails:

```js
import { useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';

export const loader = async () => {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    throw new Error('could not fetch event data');
  } else {
    return response;
  }
};

function EventsPage() {
  const data = useLoaderData();
  const events = data.events;

  return <EventsList events={events} />;
}

export default EventsPage;
```

Refactor `App.js` to add an `errorElement` property equal to `<ErrorPage/>` on
the root route:

```js
// imports omitted...

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />, // Add `errorElement`
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader,
          },
          { path: ':eventId', element: <EventDetailPage /> },
          { path: 'new', element: <NewEventPage /> },
          { path: ':id/edit', element: <EditEventPage /> },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

Make a change to the `fetch` operation that will force the request to fail, such
as changing the `port` to an invalid port number. Save all changes and attempt
to visit the `Events` page. Note the following output:

![throw from loader](./screenshots/throw-from-loader.png)

Our application is now showing the **closest** `errorElement` in the route tree.
