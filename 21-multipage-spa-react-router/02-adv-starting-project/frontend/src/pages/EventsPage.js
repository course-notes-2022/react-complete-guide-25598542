import { Link } from 'react-router-dom';
import classes from './EventsPage.module.css';

const EVENTS = [
  {
    id: 'e-1',
    title: "Everyone's Favorite Event",
    date: '2024-01-01',
    description: 'The best event of the year!',
    image: '',
  },
  {
    id: 'e-2',
    title: 'My Birthday Party',
    date: '2024-01-02',
    description: 'My 99th birthday blowout!',
    image: '',
  },
  {
    id: 'e-3',
    title: 'A Boring Event',
    date: '2024-01-03',
    description: 'Not much happening at this event, to be honest...',
    image: '',
  },
];

const EventsPage = () => {
  return (
    <>
      <h1>Events Page</h1>
      <ul>
        {EVENTS.map((event) => (
          <li key={event.id}>
            <Link to={`/events/${event.id}`}>{event.title}</Link>
          </li>
        ))}
      </ul>
    </>
  );
};

export default EventsPage;
