import { createBrowserRouter } from 'react-router-dom';
import EditEventPage from '../pages/EditEventPage';
import EventDetailPage from '../pages/EventDetailPage';
import EventsPage from '../pages/EventsPage';
import HomePage from '../pages/HomePage';
import NewEventPage from '../pages/NewEventPage';
import RootLayout from '../layouts/RootLayout';
import EventsLayout from '../layouts/EventsLayout';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      { path: '', element: <HomePage /> },
      {
        path: '/events',
        element: <EventsLayout />,
        children: [
          { path: '', element: <EventsPage /> },
          { path: '/events/:id', element: <EventDetailPage /> },
          { path: '/events/new', element: <NewEventPage /> },
          { path: '/events/:id/edit', element: <EditEventPage /> },
        ],
      },
    ],
  },
]);

export default router;
