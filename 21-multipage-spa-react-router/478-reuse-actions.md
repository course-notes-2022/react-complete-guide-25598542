# Reusing Actions via Request Methods

Let's look at our event editing functionality. Currently, we're not actually
supporting submitting an event. We render the `EditEvent` form, but we haven't
registered any **actions** for our `/events/edit` route:

```js
const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader,
          },
          {
            path: ':eventId',
            id: 'event-detail',
            loader: eventDetailLoader,
            children: [
              {
                index: true,
                element: <EventDetailPage />,
                loader: eventDetailLoader,
                action: deleteEventAction,
              },
              { path: 'edit', element: <EditEventPage /> }, // No action to handle this route yet!
            ],
          },
          { path: 'new', element: <NewEventPage />, action: newEventAction },
        ],
      },
    ],
  },
```

The action we want to dispatch is _almost_ the same as the one to the
`NewEventPage` route. It would be great if we could **reuse** this action
somehow...

And we can!

## Reusing an Action

Cut the `action` function from `NewEvent` and add it to `EventForm.js`. Note
that we could also create a **utility file** to house it as well.

`EventForm` is already receiving a `method` prop. We can pass the `method` to
the `method` property of the `Form` component we're using from
`react-router-dom`, **and** refactor `action` to create a **dynamic request**
based on the value of `method`:

```js
import {
  Form,
  useNavigate,
  useNavigation,
  useActionData,
} from 'react-router-dom';

import classes from './EventForm.module.css';

function EventForm({ method, event }) {
  const data = useActionData();
  const navigation = useNavigation();
  const isSubmitting = navigation.state === 'submitting';

  const navigate = useNavigate();
  function cancelHandler() {
    navigate('..');
  }

  return (
    <Form method={method} className={classes.form}>
      {data && data.errors && (
        <ul>
          {Object.values(data.errors).map((err) => (
            <li>{err}</li>
          ))}
        </ul>
      )}
      <p>
        <label htmlFor="title">Title</label>
        <input
          id="title"
          type="text"
          name="title"
          required
          defaultValue={event ? event.title : ''}
        />
      </p>
      <p>
        <label htmlFor="image">Image</label>
        <input
          id="image"
          type="url"
          name="image"
          required
          defaultValue={event ? event.image : ''}
        />
      </p>
      <p>
        <label htmlFor="date">Date</label>
        <input
          id="date"
          type="date"
          name="date"
          required
          defaultValue={event ? event.date : ''}
        />
      </p>
      <p>
        <label htmlFor="description">Description</label>
        <textarea
          id="description"
          name="description"
          rows="5"
          required
          defaultValue={event ? event.description : ''}
        />
      </p>
      <div className={classes.actions}>
        <button type="button" onClick={cancelHandler} disabled={isSubmitting}>
          Cancel
        </button>
        <button disabled={isSubmitting}>
          {isSubmitting ? 'Submitting' : 'Save'}
        </button>
      </div>
    </Form>
  );
}

export default EventForm;

// Refactor `action` to make code dynamic enough
// to support adding a NEW event,
// AND editing an EXISTING event
export async function action({ request }) {
  const data = await request.formData();
  const method = request.method;
  const eventData = {
    title: data.get('title'),
    image: data.get('image'),
    date: data.get('date'),
    description: data.get('description'),
  };

  let url = 'http://localhost:8080/events';
  if (method === 'PATCH') {
    const eventId = params.eventId;
    url = `${url}/${eventId}`;
  }
  const response = await fetch(url, {
    method,
    body: JSON.stringify(eventData),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (response.status === 422) {
    return response;
  }

  if (!response.ok) {
    throw json({ message: 'Could not save event.' }, { status: 500 });
  }
  return redirect('/events');
}
```

Edit our event forms to make sure we're passing the appropriate `method` prop to
the `Form` components: `post` for creating an event, and `put` for editing an
existing event:

```js
import { useRouteLoaderData } from 'react-router-dom';
import EventForm from '../components/EventForm';

function EditEventPage() {
  const data = useRouteLoaderData('event-detail');

  return <EventForm method="patch" event={data.event} />;
}

export default EditEventPage;
```

```js
import EventForm from '../components/EventForm';

function NewEventPage() {
  return <EventForm method="post" />;
}

export default NewEventPage;
```

Update `App.js` to use the new action on **both** the Edit and New Event routes:

```js
// imports omitted...

import NewEventPage, {
  action as manipulateEventAction,
} from './pages/NewEvent';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader,
          },
          {
            path: ':eventId',
            id: 'event-detail',
            loader: eventDetailLoader,
            children: [
              {
                index: true,
                element: <EventDetailPage />,
                loader: eventDetailLoader,
                action: deleteEventAction,
              },
              {
                path: 'edit',
                element: <EditEventPage />,
                action: manipulateEventAction, // new action for edit event
              },
            ],
          },
          {
            path: 'new',
            element: <NewEventPage />,
            action: manipulateEventAction, // new action for manipulate event
          },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```
