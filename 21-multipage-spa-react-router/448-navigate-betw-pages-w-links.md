# Navigating Between Pages with Links

let's learn some more routing features that apply to both types of routing
definitions.

Currently, we have to **manually edit the URL** in order to navigate between our
two pages. This is not ideal for our users. Typically, we want to provide
**links** to our application routes.

We _could_ use the built-in HTML `anchor` tags, but there is a problem: `<a>`
creates a **new HTTP request** to the server serving the website. The server
will serve the single page that renders our application, but this creates
unnecessary overhead (as the React app is **already loaded**), and our
application state will be reset.

To avoid these drawbacks, we use the `Link` component provided by
`react-router-dom`.

## The `Link` Component

The `Link` component renders an `anchor` element, listens for clicks on the
element, and prevents the default browser behavior while loading the content
associated with the route.

Update `HomePage` to use the `Link` component to navigate to the `ProductsPage`:

```js
import { Link } from 'react-router-dom';

export default function HomePage() {
  return (
    <>
      <h1>Home Page</h1>
      <p>
        Go to <Link to="/products">the list of products.</Link>
      </p>
    </>
  );
}
```
