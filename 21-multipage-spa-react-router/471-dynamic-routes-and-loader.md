# Dynamic Routes and `loader()`s

If we click on an event, we probably want to load the **data** for that event in
the `EventDetail` page.

First, let's make sure we have a `Link` component in the `EventsList` component
that will navigate to the correct URL:

```js
import { Link } from 'react-router-dom';
import classes from './EventsList.module.css';

function EventsList({ events }) {
  return (
    <div className={classes.events}>
      <h1>All Events</h1>
      <ul className={classes.list}>
        {events.map((event) => (
          <li key={event.id} className={classes.item}>
            {
              // Add the `Link` here, with a relative
              // URL that will append the event ID
              // to the currently-active route
            }
            <Link to={event.id}>
              <img src={event.image} alt={event.title} />
              <div className={classes.content}>
                <h2>{event.title}</h2>
                <time>{event.date}</time>
              </div>
            </Link>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default EventsList;
```

To get the event detail data, we _could_ create a new `useEffect` in the
`EventDetail` component that will fetch the data on component load, but instead
we'll create a new `loader` function **inside `EventDetail`**.

Inside the `loader`, we'll create and send a `fetch` request to the backend to
fetch the data for this particular event. However, to do so we'll need the `id`
of the event itself. How can we get access to the `id`?

We can't use the `useParams` hook, because we're not calling it **inside a
component**.

`react-router-dom` automatically passes an argument to each `loader` function.
The argument is an **object** with the following structure:

```js
{
  request, // contains the request data
    params; // contains the request parameters
}
```

Refactor `EventDetail.js` to create and use a new `loader` function:

```js
import { json, useLoaderData } from 'react-router-dom';
import EventItem from '../components/EventItem';

function EventDetailPage() {
  const data = useLoaderData();

  return <EventItem event={data.event} />;
}

export default EventDetailPage;

export async function loader({ request, params }) {
  const id = params.eventId;
  const response = await fetch(`http://loaclhost:8080/events/${id}`);
  if (!response.ok) {
    throw json(
      { message: 'Could not fetch details for selected event.' },
      { status: 500 }
    );
  } else {
    return response;
  }
}
```

Import the event detail `loader` and add it to the route for the event detail
view in `App.js`:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/Home';
import EventsPage, { loader as eventsLoader } from './pages/Events';
import NewEventPage from './pages/NewEvent';
import EventDetailPage, {
  loader as eventDetailLoader,
} from './pages/EventDetail';
import EditEventPage from './pages/EditEvent';
import ErrorPage from './pages/ErrorPage';
import RootLayout from './pages/Root';
import EventsRootLayout from './pages/EventsRoot';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader,
          },
          {
            path: ':eventId',
            element: <EventDetailPage />,
            loader: eventDetailLoader,
          },
          { path: 'new', element: <NewEventPage /> },
          { path: ':id/edit', element: <EditEventPage /> },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```
