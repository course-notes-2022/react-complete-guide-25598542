# Extracting Error Data and Throwing Responses

The error page works, but it's not pretty and not very specific. Let's work on
fixing both in this lesson.

Create two new files in `src/components`, `PageContent.js` and
`PageContent.module.css` with the following content respectively:

```js
import classes from './PageContent.module.css';

function PageContent({ title, children }) {
  return (
    <div className={classes.content}>
      <h1>{title}</h1>
      {children}
    </div>
  );
}

export default PageContent;
```

```css
.content {
  text-align: center;
}
```

We can **throw** a `Response` object, and extract the data contained on it.
Refactor `loader` to throw the `Response` object:

```js
import { useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';

export const loader = async () => {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    // Throw `Response` object
    throw new Response(JSON.stringify({ message: 'Could not fetch events.' }), {
      status: 500,
    });
  } else {
    return response;
  }
};

function EventsPage() {
  const data = useLoaderData();
  const events = data.events;

  return <EventsList events={events} />;
}

export default EventsPage;
```

`react-router-dom` provides another hook, `useRouteError`, that allows us to
extract the data being thrown, inside the component being _rendered as an
`errorElement`_.

The shape of that data depends on whether we throw a `Response`, or any other
kind of data. If we throw a `Response`, the object returned by `useRouteError`
contains a `status` property that equals the value of the `status` property on
the `Response`. If throwing any **other** kind of object, the object returned is
simply the thrown object.

Refactor `ErrorPage` as follows:

```js
import { useRouteError } from 'react-router-dom';
import PageContent from '../components/PageContent';

function ErrorPage() {
  const err = useRouteError();

  let title = 'An error occurred!';
  let message = 'Something went wrong';

  if (err.status && err.status === 500) {
    message = JSON.parse(err.data).message;
  }

  if (err.status === 404) {
    title = 'Not Found!';
    message = 'Could not find resource or page';
  }
  return (
    <PageContent title={title}>
      <p>{message}</p>
    </PageContent>
  );
}

export default ErrorPage;
```

Save all changes and attempt to visit a non-existent route in the browser. Note
that we are getting our **custom `404` output**:

![error handling with response](./screenshots/err-handling-w-response.png)
