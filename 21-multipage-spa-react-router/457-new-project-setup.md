# Onwards to a New Project Setup

We've learned a lot about routing and the key routing features in
`react-router-dom`. Let's now practice what we've learned. This project includes
a backend API server and frontend React application. We'll dive deeper into
routing using these two projects for the rest of this section.

Run `npm install` in both the frontend and backend projects. Start the backend
server every time you work on the frontend application, as it is dependent.
