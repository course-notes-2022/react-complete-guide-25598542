# Returning Responses in `loader()`s

`loader()` is an important feature of `react-router`. One important aspect of
`loader` is to understand that you can return **any type of data** from a
`loader` function.

We can also return a **`Response`** object of the `Response` type built-in to
the browser:

```js
import { useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';

export const loader = async () => {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    // ...
  } else {
    const resData = await response.json();
    // return resData.events;

    const res = new Response('any data', { status: 201 }); // We can return a `Response` object
    return res;
  }
};

function EventsPage() {
  const events = useLoaderData();

  return <EventsList events={events} />;
}

export default EventsPage;
```

Whenever we return a `Response` in `loader`s, react-router will
**automatically** extract the data from the response when calling
`useLoaderData`. This feature exist because we commonly use `fetch` to reach out
to the backend for data. Since `fetch` **returns a `Response` object by
default**, we can simply **return the response** from `loader`:

```js
import { useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';

export const loader = async () => {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    // ...
  } else {
    return response;
  }
};

function EventsPage() {
  const data = useLoaderData();
  const events = data.events;

  return <EventsList events={events} />;
}

export default EventsPage;
```
