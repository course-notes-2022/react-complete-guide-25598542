import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import RootLayout from './pages/Root';
import HomePage from './pages/HomePage';
import ProductsPage from './pages/Products';
import ErrorPage from './pages/ErrorPage';
import ProductDetailPage from './pages/ProductDetails';

// `createBrowserRouter` allows us to define
// our application routes. It takes an array
// of route definition objects.
const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: '', element: <HomePage /> },
      { path: 'products', element: <ProductsPage /> },
      { path: 'products/:productId', element: <ProductDetailPage /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />; // Render our router on the screen
}

export default App;
