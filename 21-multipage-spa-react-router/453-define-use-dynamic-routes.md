# Defining and Using Dynamic Routes

To conclude the basics of routing, let's look at a **common and crucial**
feature of `react-router-dom`: **dynamic routes**.

Consider our `Products` page. We might want to have a **list** of products
visible on our products page. Each product in the list would be clickable, and
lead to a **details page** with the specific data for that product.

What would be the **path** for the **route associated with the details page**?
We always want to load the same **component**, but we want different **data**
for each product. We want our path to look something like:

```js
{path: '/products/product-id-1', element: <ProductDetailPage/>}
```

**Hard-coding** the product id attributes is not a realistic option. Instead,
`react-router-dom` provides the ability to create **dynamic paths**.

You can create a dynamic path by simply preceding the dynamic path segment with
a `:`, as in the following:

```js
{path: '/products/:productId', element: <ProductDetailPage/>}
```

The `:` signals to `react-router-dom` that this particular portion of the path
is **dynamic**.

Refactor `App.js` to define a new dynamic route:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import RootLayout from './pages/Root';
import HomePage from './pages/HomePage';
import ProductsPage from './pages/Products';
import ErrorPage from './pages/ErrorPage';
import ProductDetailPage from './pages/ProductDetails';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: '/', element: <HomePage /> },
      { path: '/products', element: <ProductsPage /> },
      { path: '/products/:productId', element: <ProductDetailPage /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

Notice we are now directed to the `ProductDetailsComponent` when entering a path
matching `/products/{anything}`:

![dynamic route](./screenshots/dynamic-route.png)

## Obtaining the Path Parameter in our Code

`react-router-dom` provides the `useParams` hook to allow us access to the
current **path parameters** in our code. Update `ProductDetails` as follows:

```js
import { useParams } from 'react-router-dom';

export default function ProductDetailPage() {
  const params = useParams();

  return (
    <>
      <h1>Product Details!</h1>
      {params.productId}
    </>
  );
}
```

Save changes and refresh. Note that the `productId` param is now visible in the
DOM.
