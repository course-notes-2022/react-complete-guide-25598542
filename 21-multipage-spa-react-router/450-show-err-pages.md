# Showing Error Pages with `errorElement`

We might want to prepare a **default error page** to be shown when a user
attempts to navigate to a page that does not exist.

Create a new file `src/pages/Error.js`, and add the following:

```js
import MainNavigation from '../components/MainNavigation';

export default function ErrorPage() {
  return (
    <>
      <MainNavigation />
      <main>
        <h1>An error occurred!</h1>
        <p>Page not found</p>
      </main>
    </>
  );
}
```

`react-router-dom` gives us the `errorElement` prop that allows us to define the
behavior that should occur when an error happens:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import RootLayout from './pages/Root';
import HomePage from './pages/HomePage';
import ProductsPage from './pages/Products';
import ErrorPage from './pages/ErrorPage';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />, // Show `ErrorPage` component when error occurs
    children: [
      { path: '/', element: <HomePage /> },
      { path: '/products', element: <ProductsPage /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />; // Render our router on the screen
}

export default App;
```

Save changes and refresh. Attempt to visit a non-existent route such as `/abc`,
and note that our error page is displayed:

![error element](./screenshots/react-router-dom-error-element.png)
