# Validating User Input & Outputting Validation Errors

We should always be **validating our user input** on the **frontend** as well as
the backend, to provide a good user experience. Let's leverage the fact that
we're sending an error response of `422` if some validation **fails**.

For example, in the `NewEvent` component, we would like to display the
validation errors **on that page**, and _not_ redirecting to an **error page**
as we're doing currently.

We can easily do this in `action` functions by **returning the response** we get
from the backend. Returning the response from an `action` allows us to use the
response in code, and is common in validation when we don't want to show an
error page.

Open the `NewEvent` component, and make the following adjustments:

```js
import { json, redirect } from 'react-router-dom';
import EventForm from '../components/EventForm';

function NewEventPage() {
  return <EventForm />;
}

export default NewEventPage;

export async function action({ request, params }) {
  const data = await request.formData();
  const eventData = {
    title: data.get('title'),
    image: data.get('image'),
    date: data.get('date'),
    description: data.get('description'),
  };

  const response = await fetch('http://localhost:8080/events', {
    method: 'POST',
    body: JSON.stringify(eventData),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  // Return the backend response if validation errors
  if (response.status === 422) {
    return response;
  }

  if (!response.ok) {
    throw json({ message: 'Could not save event.' }, { status: 500 });
  }
  return redirect('/events');
}
```

We can use the data in the EventFrom. Refactor `EventForm.js` as follows:

```js
import {
  Form,
  useNavigate,
  useNavigation,
  useActionData,
} from 'react-router-dom';

import classes from './EventForm.module.css';

function EventForm({ method, event }) {
  /*
    `useActionData()` allows us to get access to the data
    returned by the **closest action**. Just as with `useLoaderData()`
    the data is already parsed as a JS object
  */
  const data = useActionData();

  /*
  `useNavigation()` gives us access to a navigation object
  that gives us various data, including the
  **current state of the transition** and the **current state
 of the action**
  */
  const navigation = useNavigation();
  const isSubmitting = navigation.state === 'submitting';

  const navigate = useNavigate();
  function cancelHandler() {
    navigate('..');
  }

  return (
    <Form method="post" className={classes.form}>
      {data && data.errors && (
        <ul>
          {Object.values(data.errors).map((err) => (
            <li>{err}</li>
          ))}
        </ul>
      )}
      <p>
        <label htmlFor="title">Title</label>
        <input
          id="title"
          type="text"
          name="title"
          required
          defaultValue={event ? event.title : ''}
        />
      </p>
      <p>
        <label htmlFor="image">Image</label>
        <input
          id="image"
          type="url"
          name="image"
          required
          defaultValue={event ? event.image : ''}
        />
      </p>
      <p>
        <label htmlFor="date">Date</label>
        <input
          id="date"
          type="date"
          name="date"
          required
          defaultValue={event ? event.date : ''}
        />
      </p>
      <p>
        <label htmlFor="description">Description</label>
        <textarea
          id="description"
          name="description"
          rows="5"
          required
          defaultValue={event ? event.description : ''}
        />
      </p>
      <div className={classes.actions}>
        <button type="button" onClick={cancelHandler} disabled={isSubmitting}>
          Cancel
        </button>
        <button disabled={isSubmitting}>
          {isSubmitting ? 'Submitting' : 'Save'}
        </button>
      </div>
    </Form>
  );
}

export default EventForm;
```

We get access to the data returned from the action with the `useActionData`. We
then show some error output using that data:

![use action data](./screenshots/use-action-data.png)
