# Adding Links for Dynamic Routes

Let's now look at how to add links with dynamic routes. Refactor
`ProductsPage.js` as follows:

```js
import { Link } from 'react-router-dom';

const PRODUCTS = [
  { id: 'p1', title: 'Product 1' },
  { id: 'p2', title: 'Product 2' },
  { id: 'p3', title: 'Product 3' },
];
export default function ProductsPage() {
  return (
    <>
      <h1>The Products Page</h1>
      <ul>
        {PRODUCTS.map((product) => (
          <li key={product.id}>
            <Link to={`/products/product-${product.id}`}>{product.tile}</Link>
          </li>
        ))}
      </ul>
    </>
  );
}
```

Simply create a JS template string with the `id` (or whatever unique identifier
is associated with the product) and provide it to the `to` property on the
`Link`.
