# Routing: Multiple Pages in Single-Page Applications

**Routing** refers to the loading of **different content on the screen** when
the **URL** changes. Traditionally, we'd implement routing by loading different
`HTML` files for different paths. The disadavantage is that a **new request**
must be generated for **each route**.

With SPAs, only **one initial HTML request is generated**. Page (URL) changes
are then handled by **client-side** React code that triggers whenever the URL
changes and then displays different content.
