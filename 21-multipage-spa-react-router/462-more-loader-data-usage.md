# More `loader()` Data Usage

Where else can we use `loader()`?

One place we could use it is in the `EventsList` component itself. We could
refactor it as follows:

```js
import { useLoaderData } from 'react-router-dom';
import classes from './EventsList.module.css';

function EventsList() {
  const events = useLoaderData();

  return (
    <div className={classes.events}>
      <h1>All Events</h1>
      <ul className={classes.list}>
        {events.map((event) => (
          <li key={event.id} className={classes.item}>
            <a href="...">
              <img src={event.image} alt={event.title} />
              <div className={classes.content}>
                <h2>{event.title}</h2>
                <time>{event.date}</time>
              </div>
            </a>
          </li>
        ))}
      </ul>
    </div>
  );
}

export default EventsList;
```

thus eliminating the need to pass a list of `events` as props to the component:

```js
import EventsList from '../components/EventsList';

function EventsPage() {
  return <EventsList />; // No more props!
}

export default EventsPage;
```

and therefore we can remove `useLoaderData` from `EventsPage`.

We _cannot_ get the events in a **higher** component, such as the `RootLayout`
component. Attempting to do so will return `undefined`. We cannot get data
defined at a **lower-level** (child) component in a **higher-level** (parent)
component.
