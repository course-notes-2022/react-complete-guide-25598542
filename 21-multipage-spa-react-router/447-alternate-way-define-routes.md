# Exploring an Alternative Way of Defining Routes

We've added two routes to our router so far. Let's take a look at an alternative
way of defining routes.

Older versions of `react-router-dom` defined routes as follows:

```js
import {
  createBrowserRouter,
  RouterProvider,
  createRoutesFromElements,
  Route,
} from 'react-router-dom';
import HomePage from './pages/HomePage';
import ProductsPage from './pages/Products';

const routerDefinitions = createRoutesFromElements(
  <Route>
    <Route path="/" element={<HomePage />} />
    <Route path="/products" element={<ProductsPage />} />
  </Route>
);

const router = createBrowserRouter(routerDefinitions);

function App() {
  return <RouterProvider router={router} />; //
}

export default App;
```

This is **equivalent** to calling `createBrowserRouter` and passing an array of
route **objects**. We'll stick to the previous syntax in this course, as it's a
bit more intuitive to understand.
