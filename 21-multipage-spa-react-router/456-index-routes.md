# Working with Index Routes

We can add a special property to some route definitions, such as the `HomePage`
definition. The HomePage `path` is an empty string, because `HomePage` should be
loaded for the **same path as the parent definition**, i.e. `/`. We have defined
separate routes for the parent and the child because we want the wrapping
behavior.

As an alternative to the current definition:

```js
const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: '', element: <HomePage /> },
      { path: 'products', element: <ProductsPage /> },
      { path: 'products/:productId', element: <ProductDetailPage /> },
    ],
  },
]);
```

We could use the `index` property on the `<HomePage>` route:

```js
const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      { path: 'products', element: <ProductsPage /> },
      { path: 'products/:productId', element: <ProductDetailPage /> },
    ],
  },
]);
```

`index` allows us to define a **default route** for when the **paren route is
active**. The application works as it did before.
