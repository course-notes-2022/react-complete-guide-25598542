# Working with `action()` Functions

Just as with `loader`, `action` is a property that we add to our **route
definitions**. `action` is a **function**, and it's best to define our `action`s
**closest to the components** that will use them.

## Submitting Forms with `react-router`

`react-router` makes form submission **easy**. First, you should ensure that all
your form fields have a `name` attribute. Those `name`s will be used to extract
the form field values.

Next, you should replace the `<form></form>` tags with the `react-router-dom`'s
`<Form></Form>` component. This will ensure that the browser default of sending
a request to the backend will be prevented, but it will **take the request and
give it to your action**, along with all the data submitted as part of the form.

Refactor `EventForm` as follows:

```js
import { Form, useNavigate } from 'react-router-dom';

import classes from './EventForm.module.css';

function EventForm({ method, event }) {
  const navigate = useNavigate();
  function cancelHandler() {
    navigate('..');
  }

  return (
    <Form method="post" className={classes.form}>
      <p>
        <label htmlFor="title">Title</label>
        <input
          id="title"
          type="text"
          name="title"
          required
          defaultValue={event ? event.title : ''}
        />
      </p>
      <p>
        <label htmlFor="image">Image</label>
        <input
          id="image"
          type="url"
          name="image"
          required
          defaultValue={event ? event.image : ''}
        />
      </p>
      <p>
        <label htmlFor="date">Date</label>
        <input
          id="date"
          type="date"
          name="date"
          required
          defaultValue={event ? event.date : ''}
        />
      </p>
      <p>
        <label htmlFor="description">Description</label>
        <textarea
          id="description"
          name="description"
          rows="5"
          required
          defaultValue={event ? event.description : ''}
        />
      </p>
      <div className={classes.actions}>
        <button type="button" onClick={cancelHandler}>
          Cancel
        </button>
        <button>Save</button>
      </div>
    </Form>
  );
}

export default EventForm;
```

### Extracting Form Data in `NewEventPage`

Refactor `NewEventPage` to define and export a new `action` function:

```js
import { json, redirect } from 'react-router-dom';
import EventForm from '../components/EventForm';

function NewEventPage() {
  return <EventForm />;
}

export default NewEventPage;

export async function action({
  request, // `action` receives a request and
  params, // params object from react-router, just like `loader`
}) {
  const data = await request.formData();
  const eventData = {
    title: data.get('title'),
    image: data.get('image'),
    date: data.get('date'),
    description: data.get('description'),
  };

  const response = await fetch('http://localhost:8080/events', {
    method: 'POST',
    body: JSON.stringify(eventData),
    headers: {
      'Content-Type': 'application/json',
    },
  });

  if (!response.ok) {
    throw json({ message: 'Could not save event.' }, { status: 500 });
  }
  return redirect('/events'); // Redirect to the `/events` page on successful save
}
```

Import and use the `action` function in our route definition, just as we did
with our `loader` functions:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/Home';
import EventsPage, { loader as eventsLoader } from './pages/Events';
import NewEventPage, { action as newEventAction } from './pages/NewEvent';
import EventDetailPage, {
  loader as eventDetailLoader,
} from './pages/EventDetail';
import EditEventPage from './pages/EditEvent';
import ErrorPage from './pages/ErrorPage';
import RootLayout from './pages/Root';
import EventsRootLayout from './pages/EventsRoot';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader,
          },
          {
            path: ':eventId',
            id: 'event-detail',
            loader: eventDetailLoader,
            children: [
              {
                index: true,
                element: <EventDetailPage />,
                loader: eventDetailLoader,
              },
              { path: 'edit', element: <EditEventPage /> },
            ],
          },
          {
            path: 'new',
            element: <NewEventPage />,
            action: newEventAction, // add new action to `NewEventPage` route
          },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```
