# Step 1: Defining Routes

Lets' begin defining our application routes. Add the following to `App.js`:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/Home';

// `createBrowserRouter` allows us to define
// our application routes. It takes an array
// of route definition objects.
const router = createBrowserRouter([
  {
    // `path` defines the path for which this route is active
    // the path is the part AFTER the domain
    path: '/',
    // `element` defines which component should be loaded when
    // this path is active
    element: <HomePage />,
  },
]);

function App() {
  return <RouterProvider router={router} />; // Render our router on the screen
}

export default App;
```

Create a new `src/pages/Home.js` file, and add the following content:

```js
export default function HomePage() {
  return <h1>Home Page</h1>;
}
```

Save all changes and refresh. If we visit the `/` route in our browser, we
should see the `HomePage` component being rendered to the screen.
