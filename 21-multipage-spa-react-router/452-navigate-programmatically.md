# Navigating Programmatically

Let's now look at how to navigate **programmatically** using `react-router-dom`.
In some situations (e.g. a form was sumbitted, a timer expired, etc.), you may
want to trigger navigation from **inside your code**. You can use the
`useNavigate` hook for this purpose.

Refactor `HomePage.js` as follows:

```js
import { Link, useNavigate } from 'react-router-dom';

export default function HomePage() {
  const navigate = useNavigate();

  function navigateHandler() {
    navigate('/products');
  }

  return (
    <>
      <h1>Home Page</h1>
      <p>
        Go to <Link to="/products">the list of products.</Link>
      </p>
      <p>
        <button onClick={navigateHandler}>Navigate</button>
      </p>
    </>
  );
}
```

Note that we're attaching the imperative navigation to a button here for **demo
purposes** only. In a real project, you should **prefer navigation with `Links`
or `NavLinks`**. Use imperative navigation to navigate programmatically, in
response to the type of examples we discussed above.
