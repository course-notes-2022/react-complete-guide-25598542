# Practice Solution

Let's implement the solution to the challenge together.

0. Run `npm install react-router-dom

1. Create a new `src/pages` directory, and add the following files:

- EditEvent.js
- EventDetail.js
- Events.js
- Home.js
- NewEvent.js

2. Add and export a component function that returns a dummy `<h1>` element from
   each new file:

```js
function EventsPage() {
  return <h1>Events Page</h1>;
}

export default EventsPage;
```

3. Add routing and route definitions:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/Home';
import EventsPage from './pages/Events';
import NewEventPage from './pages/NewEvent';
import EventDetailPage from './pages/EventDetail';
import EditEventPage from './pages/EditEvent';

const router = createBrowserRouter([
  { path: '/', element: <HomePage /> },
  { path: '/events', element: <EventsPage /> },
  { path: '/events/:eventId', element: <EventDetailPage /> },
  { path: '/events/new', element: <NewEventPage /> },
  { path: '/events/:id/edit', element: <EditEventPage /> },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

4. Create a new `src/pages/Root.js` file for the root layout,and add the
   following:

```js
import { Outlet } from 'react-router-dom';
import MainNavigation from '../components/MainNavigation';

const RootLayout = () => {
  return (
    <>
      <MainNavigation />
      <main>
        <Outlet />
      </main>
    </>
  );
};
```

We can now refactor our route definitions so that they are **children** of the
root `/` route:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/Home';
import EventsPage from './pages/Events';
import NewEventPage from './pages/NewEvent';
import EventDetailPage from './pages/EventDetail';
import EditEventPage from './pages/EditEvent';
import RootLayout from './pages/Root';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      { index: true, element: <HomePage /> },
      { path: 'events', element: <EventsPage /> },
      { path: 'events/:eventId', element: <EventDetailPage /> },
      { path: 'events/new', element: <NewEventPage /> },
      { path: 'events/:id/edit', element: <EditEventPage /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

5. Add functioning navigation links to the MainNavigation:

```js
import { Link } from 'react-router-dom';
import classes from './MainNavigation.module.css';

function MainNavigation() {
  return (
    <header className={classes.header}>
      <nav>
        <ul className={classes.list}>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/events">Events</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default MainNavigation;
```

6. Adjust the styling of the links to reflect its "active" status:

```js
import { NavLink } from 'react-router-dom';
import classes from './MainNavigation.module.css';

export default function MainNavigation() {
  return (
    <header className={classes.header}>
      <nav>
        <ul className={classes.list}>
          <li>
            <NavLink
              to=""
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
              end
            >
              Home
            </NavLink>
          </li>
          <li>
            <NavLink
              to="products"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Products
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}
```

7. Output a list of dummy events with a link to the event details page:

```js
const DUMMY_EVENTS = [
  {
    id: 'e1',
    title: 'some event',
  },
  {
    id: 'e2',
    title: 'another event',
  },
];

function EventsPage() {
  return (
    <>
      <h1>Events Page</h1>
      <ul>
        {DUMMY_EVENTS.map((event) => {
          <li key={event.id}>
            <Link to={event.id}>{event.title}</Link>
          </li>;
        })}
      </ul>
    </>
  );
}

export default EventsPage;
```

8. Output id of selected event on event detail page:

```js
import { useParams } from 'react-router-dom';

function EventDetailPage() {
  const params = useParams();

  return (
    <>
      <h1>Event Details</h1>
      <p>Event Id: {params.id}</p>
    </>
  );
}

export default EventDetailPage;
```

9. Bonus task: Add nested layout containing `/events/...` routes.

Add the following to a new `src/pages/EventsRoot.js`:

```js
import { Outlet } from 'react-router-dom';
import EventsNavigation from '../components/EventsNavigation';

function EventsRootLayout() {
  return (
    <>
      <EventsNavigation />
      <Outlet />
    </>
  );
}

export default EventsRootLayout;
```

Refactor `App.js`:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/Home';
import EventsPage from './pages/Events';
import NewEventPage from './pages/NewEvent';
import EventDetailPage from './pages/EventDetail';
import EditEventPage from './pages/EditEvent';
import RootLayout from './pages/Root';
import EventsRootLayout from './pages/EventsRoot';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          { index: true, element: <EventsPage /> },
          { path: ':eventId', element: <EventDetailPage /> },
          { path: 'new', element: <NewEventPage /> },
          { path: ':id/edit', element: <EditEventPage /> },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

Update `EventsNavigation` with `NavLink` components:

```js
import { NavLink } from 'react-router-dom';
import classes from './EventsNavigation.module.css';

function EventsNavigation() {
  return (
    <header className={classes.header}>
      <nav>
        <ul className={classes.list}>
          <li>
            <NavLink
              to="/events"
              className={({ isActive }) => {
                isActive ? classes.active : undefined;
              }}
              end
            >
              All Events
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/events/new"
              className={({ isActive }) => {
                isActive ? classes.active : undefined;
              }}
            >
              New Event
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}

export default EventsNavigation;
```
