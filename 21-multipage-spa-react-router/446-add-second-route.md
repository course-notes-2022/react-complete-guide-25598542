# Adding a Second Route

Let's add a second route to our application. Create a new file,
`src/pages/ProductsPage.js`:

```js
import { Link } from 'react-router-dom';

const PRODUCTS = [
  { id: 'p1', title: 'Product 1' },
  { id: 'p2', title: 'Product 2' },
  { id: 'p3', title: 'Product 3' },
];
export default function ProductsPage() {
  return (
    <>
      <h1>The Products Page</h1>
      <ul>
        {PRODUCTS.map((product) => (
          <li key={product.id}>
            <Link to={`/products/product-${product.id}`}>{product.title}</Link>
          </li>
        ))}
      </ul>
    </>
  );
}
```

Refactor `App.js` to add a supporting route:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/HomePage';
import ProductsPage from './pages/Products';

const router = createBrowserRouter([
  { path: '/', element: <HomePage /> },
  { path: '/products', element: <ProductsPage /> },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

Save changes and visit `/products` in the browser. Verify that the products page
is visible.
