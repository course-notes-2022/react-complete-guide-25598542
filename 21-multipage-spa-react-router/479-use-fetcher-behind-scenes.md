# Behind-the-Scenes Work with `useFetcher()`

Let's learn a new feature. Add the files attached to the lesson, including the
`Newsletter` component. Save changes and note that we now have the newsletter
sign-up on **every application page**.

The problem now is that we want to trigger the `newsletterAction` any time the
user clicks the signup button on **any route**. We would have to add the action
to **all routes**, creating redundant code and **clashing with any `action`s
already defined** on the route.

`react-router-dom` provides a utility for this scenario: the `useFetcher` hook.
`useFetcher` gives an object with useful properties and methods, including a
`Form` and a `submit` function. These are **not** the same as the `Form`
component we've used returned by `react-router-dom`, and the `submit` function
we got from the `useSubmit` hook:

```js
import { useFetcher } from 'react-router-dom';
import classes from './NewsletterSignup.module.css';

function NewsletterSignup() {
  const fetcher = useFetcher();

  return (
    <fetcher.Form method="post" className={classes.newsletter}>
      <input
        type="email"
        placeholder="Sign up for newsletter..."
        aria-label="Sign up for newsletter"
      />
      <button>Sign up</button>
    </fetcher.Form>
  );
}

export default NewsletterSignup;
```

The difference is that the `fetcher.Form` component triggers an action, but
**does not initialize a route transition**.

> `fetcher` should be used whenever you want to trigger an `action` or a
> `loader` **without navigating to the page to which the `action` or `loader`
> belongs**!

For example, in the `NewsletterSignup` component, we want to trigger the
`action` associated with the `/newsletter` route, but **not navigate/load the
component** associated with it!

Refactor `NewsletterSignup` as follows:

```js
import { useEffect } from 'react';
import { useFetcher } from 'react-router-dom';
import classes from './NewsletterSignup.module.css';

function NewsletterSignup() {
  const fetcher = useFetcher();
  const { data, state } = fetcher; // Access data and state on fetcher

  useEffect(() => {
    if (state === 'idle' && data && data.message) {
      window.alert(data.message);
    }
  }, [data, state]);

  return (
    <fetcher.Form
      method="post"
      action="/newsletter"
      className={classes.newsletter}
    >
      <input
        type="email"
        placeholder="Sign up for newsletter..."
        aria-label="Sign up for newsletter"
      />
      <button>Sign up</button>
    </fetcher.Form>
  );
}

export default NewsletterSignup;
```

Save changes and refresh. Enter an email address in the 'newsletter signup
field' and click the "Signup" button. Note that an alert is shown with the
status of the request, but **no page navigation takes place**:

![use fetcher success](./screenshots/use-fetcher.png)
