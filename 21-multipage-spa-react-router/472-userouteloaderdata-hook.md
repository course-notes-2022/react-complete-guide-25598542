# The `useRouteLoaderData()` Hook and Accessing Data from Other Routes

Let's look at one more aspect of loading data.

If the user clicks the "Edit" button in the `EventItem` component (in the
`EventDetail` view), we want to navigate to the `EditEvent` page. Refactor
`EventItem` to use the `Link` component as follows:

```js
import { Link } from 'react-router-dom';
import classes from './EventItem.module.css';

function EventItem({ event }) {
  function startDeleteHandler() {
    // ...
  }

  return (
    <article className={classes.event}>
      <img src={event.image} alt={event.title} />
      <h1>{event.title}</h1>
      <time>{event.date}</time>
      <p>{event.description}</p>
      <menu className={classes.actions}>
        <Link to="edit">Edit</Link>
        <button onClick={startDeleteHandler}>Delete</button>
      </menu>
    </article>
  );
}

export default EventItem;
```

In the `EditEventPage` component, we want to output `EventForm`:

```js
import EventForm from '../components/EventForm';

function EditEventPage() {
  return <EventForm />;
}

export default EditEventPage;
```

We want to **pre-populate** the event detail data in the `EditEventPage` as
well. We need to fetch the same data here as we did on the `EventDetailPage`. Do
we need to write **another** `loader` function?

_Thankfully, **no**_! We can **refactor our routes** to **nest**
`EventDetailsPage` _and_ `EditEventPage`, both of which are accessible under the
`/events/:id` route, under a _new_ `:eventId` route:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import HomePage from './pages/Home';
import EventsPage, { loader as eventsLoader } from './pages/Events';
import NewEventPage from './pages/NewEvent';
import EventDetailPage, {
  loader as eventDetailLoader,
} from './pages/EventDetail';
import EditEventPage from './pages/EditEvent';
import ErrorPage from './pages/ErrorPage';
import RootLayout from './pages/Root';
import EventsRootLayout from './pages/EventsRoot';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: eventsLoader,
          },
          {
            path: ':eventId',
            loader: eventDetailLoader, // Loader will be available to children
            id: 'event-detail', // Add an `id` attribute for use in `useRouteLoaderData
            children: [
              {
                index: true,
                element: <EventDetailPage />,
              },
              { path: 'edit', element: <EditEventPage /> },
            ],
          },
          { path: 'new', element: <NewEventPage /> },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

We can now refactor `EditEvent` and `EventDetail` to use the loader of their
**parent route**:

```js
import { useRouteLoaderData } from 'react-router-dom';
import EventForm from '../components/EventForm';

function EditEventPage() {
  const data = useRouteLoaderData('event-detail'); // `useRouteLoaderData` takes an id assigned to a route

  return <EventForm event={data.event} />;
}

export default EditEventPage;
```

```js
import { json, useRouteLoaderData } from 'react-router-dom';
import EventItem from '../components/EventItem';

function EventDetailPage() {
  const data = useRouteLoaderData('event-detail');

  return <EventItem event={data.event} />;
}

export default EventDetailPage;

export async function loader({ request, params }) {
  const id = params.eventId;
  const response = await fetch(`http://localhost:8080/events/${id}`);
  if (!response.ok) {
    throw json(
      { message: 'Could not fetch details for selected event.' },
      { status: 500 }
    );
  } else {
    return response;
  }
}
```

Save changes and navigate to the `Edit` view for our single event. Note that the
event data is now populated in the edit form:

![route loader data OK](./screenshots/route-loader-data-ok.png)

## Summary

To get access to a higher-level loader from a route that doesn't have a loader,
use `routeLoaderData` instead of `loaderData`.
