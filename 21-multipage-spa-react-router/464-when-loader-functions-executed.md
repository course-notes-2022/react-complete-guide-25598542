# When Are `loader()` Functions Executed?

`loader` functions are executed when we _start_ navigating to a page, **before**
the page navigation completes and the page is rendered. **You can rely on the
data being available before the page is rendered**.

The downside is that for a short time, it appears to the user as if nothing is
happening while the application is waiting for the data to be fetched. We do
have some options for making the user experience better here, though. Let's
begin to investigate some of them in the next lesson.
