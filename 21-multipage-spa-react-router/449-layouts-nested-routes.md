# Layouts and Nested Routes

We might want to add more features to our application, such as:

- A navigation bar
- Some default styling to apply

Create a new `src/components/MainNavigation.js` file:

```js
import { Link } from 'react-router-dom';

export default function MainNavigation() {
  return (
    <header>
      <nav>
        <ul>
          <li>
            <Link to="/">Home</Link>
          </li>
          <li>
            <Link to="/products">Products</Link>
          </li>
        </ul>
      </nav>
    </header>
  );
}
```

We want the `MainNavigation` to be visible on **every page** of our app. We
_could_ add it manually to each of our `pages`, but that would be redundant.
Instead, we could make use of **layouts**. A layout is a **wrapper component**
that **contains** other components that correspond to application routes.

Create a new component, `src/pages/Root.js`, and add the following:

```js
import { Outlet } from 'react-router-dom';

export default function RootLayout() {
  return (
    <>
      <h1>Root Layout</h1>
      {/*
        Outlet is a special component from `react-router-dom`. It tells React where to render **child elements** of the wrapper
         */}
      <Outlet />
    </>
  );
}
```

Refactor our route definitions to make the `HomePage` and `ProductsPage` child
routes of the `Root` component:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import RootLayout from './pages/Root';
import HomePage from './pages/HomePage';
import ProductsPage from './pages/Products';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      { path: '/', element: <HomePage /> },
      { path: '/products', element: <ProductsPage /> },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

Save changes and refresh. Note that the `HomePage` and `ProductsPage` now appear
as nested components **inside the `Root`**.

We can now include our header navigation component as part of the layout, **so
that it appears on every page**:

```js
import { Outlet } from 'react-router-dom';
import MainNavigation from '../components/MainNavigation';

export default function RootLayout() {
  return (
    <>
      <MainNavigation />
      <Outlet />
    </>
  );
}
```

Note that we can have **multiple different layouts** that are
**path-dependent**, as in the following example:

```js
import { createBrowserRouter, RouterProvider } from 'react-router-dom';
import RootLayout from './pages/Root';
import HomePage from './pages/HomePage';
import ProductsPage from './pages/Products';

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      { path: '/', element: <HomePage /> },
      { path: '/products', element: <ProductsPage /> },
    ],
  },
  {
    path: '/admin',
    element: <AdminLayout/>,
    children: {
        [
            // Any nested routes we want...
        ]
    }
  }
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```
