# Deferring Data Fetching with `defer()`

Let's learn one more feature that allows us to **defer when data is loaded**.

What does this mean?

Imagine that our call to fetch the events data from the backend is experiencing
some **latency**, perhaps 1 - 2 seconds. Rather than waiting until the request
completes to render the events list, we may want to render only **parts** of the
events page until the request completes.

We can **defer** loading, i.e. tell React that we **do** want to render the page
**even though the data fetch is not yet complete**. That's what we'll learn to
do now.

Update `Events.js` as follows:

```js
import { defer, useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';

async function loadEvents() {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    throw new Response(JSON.stringify({ message: 'Could not fetch events.' }), {
      status: 500,
    });
  } else {
    return response;
  }
}

export const loader = () => {
  return defer(
    // `defer` accepts an argument that is an OBJECT
    // that contains all the HTTP request functions
    // that take place on this page
    {
      events: loadEvents(), // EXECUTE the function and store the return value on `events`
    }
  );
};

function EventsPage() {
  const data = useLoaderData();
  const events = data.events;

  return <EventsList events={events} />;
}

export default EventsPage;
```

In the component where we want to **use the deferred data**, make the following
changes:

```js
// `useLoaderData` is a hook provided by
// react-router-dom that we can use to get
// access to the "closest loader data"
import { Await, defer, useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';
import { Suspense } from 'react';

async function loadEvents() {
  const response = await fetch('http://localhost:8080/events');

  if (!response.ok) {
    throw new Response(JSON.stringify({ message: 'Could not fetch events.' }), {
      status: 500,
    });
  } else {
    const responseData = await response.json();
    return responseData.events;
  }
}

export const loader = () => {
  return defer(
    // `defer` accepts an argument that is an OBJECT
    // that contains all the HTTP request functions
    // that take place on this page
    {
      events: loadEvents(), // EXECUTE the function and store the return value on `events`
    }
  );
};

function EventsPage() {
  const { events } = useLoaderData();

  /*
  The `Await` component will wait until the data passed by
  the `resolve` prop
  */
  return (
    <Suspense fallback={<p>Loading...</p>}>
      <Await resolve={events}>
        {(loadedEvents) => <EventsList events={loadedEvents} />}
      </Await>
    </Suspense>
  );
}

export default EventsPage;
```
