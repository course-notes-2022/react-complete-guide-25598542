# Understanding Relative & Absolute Paths

_NOTE: Return to this lecture_

Let's take a look at **paths** again. When defining our routes, we define the
paths for which the routes should be active. Currently, all the paths we're
defining are **absolute paths**, starting with a `/`:

```js
const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: '/', element: <HomePage /> },
      { path: '/products', element: <ProductsPage /> },
      { path: '/products/:productId', element: <ProductDetailPage /> },
    ],
  },
]);
```

**Absolute paths** mean that the path is always seen from after the **domain
name**.

Imagine we refactored the above code to:

```js
const router = createBrowserRouter([
  {
    path: '/root',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: '/', element: <HomePage /> },
      { path: '/products', element: <ProductsPage /> },
      { path: '/products/:productId', element: <ProductDetailPage /> },
    ],
  },
]);
```

Our application would break with the following error:

```
Uncaught Error: Absolute route path "/" nested under path "/root" is not valid. An absolute child route path must start with the combined path of all its parent routes.
```

because we're defining the **parent element** route as `/root`, _but none of our
absolute child routes start with `/root`_ (note that the following _would_
work):

```js
const router = createBrowserRouter([
  {
    path: '/root',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: '/root/', element: <HomePage /> },
      { path: '/root/products', element: <ProductsPage /> },
      { path: '/root/products/:productId', element: <ProductDetailPage /> },
    ],
  },
]);
```

## Defining Relative Paths

We can **remove the leading `/` from _absolute paths_ to make them _relative
paths_**. Relative paths are **appended _after_ the path of the _parent
route_**:

```js
const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    errorElement: <ErrorPage />,
    children: [
      { path: '', element: <HomePage /> },
      { path: 'products', element: <ProductsPage /> },
      { path: 'products/:productId', element: <ProductDetailPage /> },
    ],
  },
]);
```

Note that we have **removed the leading `/`** from all our child routes, and
refactored it to the **parent route**. This means that all our child routes will
be **appended to the parent route**.

The same rules apply to our `Link` components. Defining our `Link`s as
**absolute paths**:

```js
export default function MainNavigation() {
  return (
    <header className={classes.header}>
      <nav>
        <ul className={classes.list}>
          <li>
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
              end
            >
              Home
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/products"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
            >
              Products
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}
```
