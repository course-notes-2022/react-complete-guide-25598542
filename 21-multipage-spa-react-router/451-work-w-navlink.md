# Working with Navigation Links (`NavLink`)

Currently, we're not getting any feedback about which link is **currently
active**, nor do we get any indication that the links are links on hover. This
is a common desired functionality, so `react-router-dom` provides built-in
support.

Add the following style rules in `MainNavigation.module.css`:

```css
.list a:hover,
.list a.active {
  color: var(--color-primary-800);
  text-decoration: underline;
}
```

To support links that show whether the current page is active or not,
`react-router-dom` provides `NavLink` **as an alternative** to the `Link`
component. The `className` prop to `NavLink` takes a **function** that should
return the CSS class name. The function **automatically** receives an object
from which we can destructure the `isActive` property. `isActive` is a boolean
that indicates if the link is active or not.

Refactor `MainNavigation.js` as follows:

```js
import { NavLink } from 'react-router-dom';
import classes from './MainNavigation.module.css';

export default function MainNavigation() {
  return (
    <header className={classes.header}>
      <nav>
        <ul className={classes.list}>
          <li>
            {
              // Add the `NavLink` component
              // with the `className` prop
            }
            <NavLink
              to="/"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
              end
            >
              Home
            </NavLink>
          </li>
          <li>
            <NavLink
              to="/products"
              className={({ isActive }) =>
                isActive ? classes.active : undefined
              }
              {/* style={({ isActive }) => ({
                textAlign: isActive ? 'center' : 'left',
              })} */}
            >
              Products
            </NavLink>
          </li>
        </ul>
      </nav>
    </header>
  );
}
```

Note that we're using the `end` prop on the `/` NavLink. `end` indicates to
react-router-dom that this link is active only if the current route **ends**
with `/`, i.e. routes such as `/products`, `/about`, `/contact`, etc. will
**not** be considered active since they **start with `/`**, which is the
**default behavior**.

Note that **inline CSS styles** are also possible to set using `isActive`.
