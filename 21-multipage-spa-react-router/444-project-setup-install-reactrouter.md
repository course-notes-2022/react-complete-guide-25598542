# Project Setup and Installing React Router

Install the attached starter project that we'll use to learn the basics of
routing with `React Router`.

After downloading the project, install the `react-router` package with the
following command:

> `npm install react-router-dom`

With this installed, we can now begin adding routing to our application.

## Adding Routing: Step by Step

1. **Define** the routes we want to support and which components should be
   loaded for different routes

2. **Activate** the router and load route definitions

3. Make sure we have the components we want to load, and provide a means of
   **navigating between pages**
