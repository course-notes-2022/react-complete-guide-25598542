# Reflecting the Current Navigation State in the UI

How can we give the user some **feedback** that something is happening while
events are being fetched by `loader`?

`react-router-dom` gives us a hook called `useNavigation` that allows us to tell
whether we are in an active transition, loading data, or if no transition is
active. `useNavigation` returns an object with a `state` property. `state` is a
string that can be one of three values:

- `idle`
- `loading`
- `submitted`

We can use the `state` property to show content conditionally depending on the
current state:

```js
import { useNavigation, Outlet } from 'react-router-dom';
import MainNavigation from '../components/MainNavigation';

const RootLayout = () => {
  const navigation = useNavigation();

  return (
    <>
      <MainNavigation />
      <main>
        {navigation.state === 'loading' && 'Loading...'}
        <Outlet />
      </main>
    </>
  );
};

export default RootLayout;
```

This is _one_ way we can find out if we're waiting for data or not. We'll learn
about other ways shortly.
