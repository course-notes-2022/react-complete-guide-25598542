# Using Data from a Loader in the Route Component

Let's learn how to access the data from the `loader`. To do so, we need to go to
the component where we want to use it.

Refactor `App.js` to use our new `loader` function as follows:

```js
// imports omitted...

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        children: [
          {
            index: true,
            element: <EventsPage />,
            loader: async () => {
              const response = await fetch('http://localhost:8080/events');

              if (!response.ok) {
                // ...
              } else {
                const resData = await response.json();
                return resData.events; // react-router will AUTOMATICALLY make any data you return from `loader` available in the component
              }
            },
          },
          { path: ':eventId', element: <EventDetailPage /> },
          { path: 'new', element: <NewEventPage /> },
          { path: ':id/edit', element: <EditEventPage /> },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```

Note that even though the function we're providing to `loader` is an `async`
function that retuns a `Promise`, **`useLoaderData` will resolve the Promise for
us** and return only the **DATA** we need, _not wrapped in the `Promise`_!

## Use the Data with `useLoaderData`

To use the data, simply import the `useLoaderData` hook and use it as follows in
`Events.js`:

```js
// `useLoaderData` is a hook provided by
// react-router-dom that we can use to get
// access to the "closest loader data"
import { useLoaderData } from 'react-router-dom';
import EventsList from '../components/EventsList';

function EventsPage() {
  const events = useLoaderData();
  console.log('events:', events);

  return <EventsList events={events} />;
}

export default EventsPage;
```

Save and refresh, and note the output in the browser:
![use loader success](./screenshots/use-loader-success.png)

This is much less code than we wrote before, and makes the `Events` component
much leaner!
