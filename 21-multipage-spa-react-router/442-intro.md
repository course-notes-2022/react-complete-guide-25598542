# Intro

So far, all of our applications have been a **single page**, and the URL never
changed. This is a core principle of React: the view the user sees is rendered
in a **single page**, and **Javascript** controls what is displayed on that page
at any given time.

This works fine, but it means that we lose out on one of the most important
features of the web: the ability to **link to other resources**. As our apps
become more complex, we may want to be able to **link** to other pages that load
other parts of our app when visited.

## Single Page Application Routing

**Single Page Application Routing** allows us to build applications that are
still _technically_ SPAs, but still have different "pages" with different URLs
that can be loaded by inserting the right URL in the browser.

In this module, we'll learn:

- What routing is and why
- Using **React Router**
- How to **fetch and send data** with React Router
