# Data Fetching with a `loader()`

Let's begin to explore more advanced features of `react-router`, starting with
**data-fetching**.

Replace the contents of `Events.js` with the following code:

```jsx
import { useEffect, useState } from 'react';

import EventsList from '../components/EventsList';

function EventsPage() {
  const [isLoading, setIsLoading] = useState(false);
  const [fetchedEvents, setFetchedEvents] = useState();
  const [error, setError] = useState();

  useEffect(() => {
    async function fetchEvents() {
      setIsLoading(true);
      const response = await fetch('http://localhost:8080/events');

      if (!response.ok) {
        setError('Fetching events failed.');
      } else {
        const resData = await response.json();
        setFetchedEvents(resData.events);
      }
      setIsLoading(false);
    }

    fetchEvents();
  }, []);
  return (
    <>
      <div style={{ textAlign: 'center' }}>
        {isLoading && <p>Loading...</p>}
        {error && <p>{error}</p>}
      </div>
      {!isLoading && fetchedEvents && <EventsList events={fetchedEvents} />}
    </>
  );
}

export default EventsPage;
```

This code should be familiar: we are using the `useEffect` hook to send a
request to fetch data from a backend.

We often have to do similar data fetching in **multiple places** in our
application. Note also that this request will be sent **only when the
`EventsPage` component is rendered**. In more complex applications, this
component could be more complex, with nested child components, and sending the
request only when the component is rendered could be **sub-optimal**.

It might be better if we could send the request **before the component
renders**, and then render the component **once the data is fetched**. With
`react-router-dom` v >6, we can!

## The `loader` Property

`react-router-dom` gives us the `loader` property that we can supply as an
**additional property to our route definitions**. `loader` takes a function as a
value that will be executed by `react-router` before you visit that route. We
can **fetch and load data** in the `loader`.

Let's use the `loader` function to fetch our event data. Make the following
changes in `App.js`:

```js
// imports omitted...

const router = createBrowserRouter([
  {
    path: '/',
    element: <RootLayout />,
    children: [
      { index: true, element: <HomePage /> },
      {
        path: 'events',
        element: <EventsRootLayout />,
        loader: async () => {
          const response = await fetch('http://localhost:8080/events');

          if (!response.ok) {
            // ...
          } else {
            const resData = await response.json();
            return resData.events; // react-router will AUTOMATICALLY make any data you return from `loader` available in the component,
            // or ANY COMPONENT that needs it
          }
        },
        children: [
          { index: true, element: <EventsPage /> },
          { path: ':eventId', element: <EventDetailPage /> },
          { path: 'new', element: <NewEventPage /> },
          { path: ':id/edit', element: <EditEventPage /> },
        ],
      },
    ],
  },
]);

function App() {
  return <RouterProvider router={router} />;
}

export default App;
```
