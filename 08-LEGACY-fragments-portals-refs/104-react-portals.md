# Introducing React Portals

**React Portals** help us write cleaner code. Consider the following code:

```javascript
return (
  <>
    <MyModal />
    <MyInputForm />
  </>
);
```

renders the following to the DOM:

```html
<section>
  <h2>Some other content...</h2>
  <div class="my-modal">
    <!-- Semantically and from a "clean HTML structure" perspecive, this nested modal isn't ideal. It's an overlay to the ENTIRE PAGE -->
    <h2>A modal Title!</h2>
  </div>
  <form>
    <label>Username</label>
    <input type="text" />
  </form>
</section>
```

We can use **portals** to maintain our JSX code structure but **render them in a
different structure in the HTML**.
