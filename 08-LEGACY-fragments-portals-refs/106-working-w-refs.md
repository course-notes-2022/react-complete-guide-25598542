# Working with Refs

**Refs** allow us to get access (references) to DOM elements and work with them.

You create refs with the `useRef` hook. `useRef` returns a value that allows you
to work with a DOM element. You can connect **any** HTML element to a ref.

It's recommended that you don't **manipulate** the data in the `current`
attribute (let React take care of data manipulation), but you _can_ **read
data**.

`AddUser.js`:

```javascript
import { useState, useRef } from 'react';

const AddUser = (props) => {
  // Create refs
  const nameInputRef = useRef(); // value is an object with "current" attr; current is set to a real DOM element on initial render of the component
  const ageInputRef = useRef();

  // ...

  const addUserHandler = (event) => {
    event.preventDefault();

    // Use refs to get value of input fields
    const enteredName = nameInputRef.current.value;

    const enteredUserAge = ageInputRef.current.value;

    if (enteredName.trim().length === 0 || enteredAge.trim().length === 0) {
      setError({
        title: 'Invalid input',
        message: 'Please enter a valid name and age (non-empty values).',
      });
      return;
    }
    if (+enteredUserAge < 1) {
      setError({
        title: 'Invalid age',
        message: 'Please enter a valid age (> 0).',
      });
      return;
    }
    props.onAddUser(enteredName, enteredUserAge);
  };

  return (
    <div>
      {error && (
        <ErrorModal
          title={error.title}
          message={error.message}
          onConfirm={errorHandler}
        />
      )}
      <Card className={classes.input}>
        <form onSubmit={addUserHandler}>
          <label htmlFor="username">Username</label>
          <input
            id="username"
            type="text"
            ref={nameInputRef} // connect this element to "nameInputRef"
          />
          <label htmlFor="age">Age (Years)</label>
          <input
            id="age"
            type="number"
            ref={ageInputRef}
            {
                // These are no longer neccessary because of the ref
                // value={enteredAge}
                // onChange={ageChangeHandler}
            }
          />
          <Button type="submit">Add User</Button>
        </form>
      </Card>
    </div>
  );
};
```

## Are `refs` Better than State?

Not necessarily. For read-only values, `refs` may be quicker to write than
state, which requires more code.
