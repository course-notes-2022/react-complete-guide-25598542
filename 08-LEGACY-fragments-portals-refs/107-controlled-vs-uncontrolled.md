# Controlled vs. Uncontrolled Components

Components that access values with a `ref` are **uncontrolled**, as their
internal state is not controlled by React but the native DOM API.

Components that have state with the `useState` hook and which feed the current
state into element(s) via the `value` attribute are **controlled** components.
