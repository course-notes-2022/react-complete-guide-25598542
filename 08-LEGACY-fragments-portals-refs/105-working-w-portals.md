# Working with Portals

Portals need two things:

1. A place to which to port the component

2. Let the component know that it should have a portal to that place

`index.html`:

```html
<!-- ... -->
<body>
  <noscript>You ned to enable JavaScript to run this app.</noscript>
  <!-- Add marker for the modal overlay -->
  <div id="backdrop-root"></div>
  <!-- Add marker for the modal-->
  <div id="modal-root"></div>
</body>
```

Create Components:

`Backdrop.js`:

```javascript
return <div className={classes.backdrop} onClick={props.onConfirm}>

```

`ModalOverlay.js`

```javascript
const ModalOverlay = (props) => {
  return (
    <Card className={classes.modal}>
      <header className={classes.header}>
        <h2>{props.title}</h2>
      </header>
      <div className={classes.content}>
        <p>{props.message}</p>
      </div>
      <footer className={classes.actions}>
        <Button onClick={props.onConfirm}>Okay</Button>
      </footer>
    </Card>
  );
};
```

`ErrorModal.js`:

```javascript
import ReactDOM from 'react-dom';

const ErrorModal = () => {
  return (
    <>
      {
        // Render the 'Backdrop' component in the #backdrop-root div element
      }
      {ReactDOM.creatPortal(
        <Backdrop onConfirm={props.onConfirm} />,
        document.getElementById('backdrop-root')
      )}

      {
        // Render the 'ModalOverlay' component in the #modal-root div element
      }
      {ReactDOM.createPortal(
        <ModalOverlay title={props.title} message={props.message} />,
        document.getElementById('modal-root')
      )}
    </>
  );
};
```
