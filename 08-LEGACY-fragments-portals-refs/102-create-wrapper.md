# Creating a Wrapper Component

A better solution for grouping adjacent elements is to use a **Wrapper**
component:

`components/Helpers/Wrapper.js`:

```javascript
// This works, because the requirement is only that ONE ROOT ELEMENT is returned
const Wrapper = (props) => {
  return props.children;
};

export default Wrapper;
```

`components/Users/AddUser.js`:

```javascript
// ...

return (
  // This works, because the requirement is only that ONE ROOT ELEMENT is returned
  <Wrapper>
    {error && (
      <ErrorModal
        title={error.title}
        message={error.message}
        onConfirm={errorHandler}
      />
    )}
    <Card className={classes.input}>
      <form onSubmit={addUserHandler}>
        <label htmlFor="username">Username</label>
        <input
          id="username"
          type="text"
          value={enteredUsername}
          onChange={usernameChangeHandler}
        />
        <label htmlFor="age">Age (Years)</label>
        <input
          id="age"
          type="number"
          value={enteredAge}
          onChange={ageChangeHandler}
        />
        <Button type="submit">Add User</Button>
      </form>
    </Card>
  </Wrapper>
);
```
