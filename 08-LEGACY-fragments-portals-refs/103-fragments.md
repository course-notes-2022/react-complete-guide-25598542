# React Fragments

The `Fragment` component is React's **custom implementation** of the `Wrapper`
component we built in the previous lesson:

```javascript

// OK
<React.Fragment>
    <div>...</div>
    <div>...</div>
</React.Fragment>

// Also OK
<>
    <div>...</div>
    <div>...</div>
</>
```
