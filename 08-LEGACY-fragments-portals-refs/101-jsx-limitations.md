# JSX Limitations

JSX is the HTML-like code that is returned by your component and ultimately
rendered in the actual DOM. But it has **limitations**:

1. **You can't return more than one "root" JSX element**. You also can't store
   more than one root JSX element in a variable, because the following isn't
   valid JS:

```javascript
return(
    React.crateElement('h2', {}, 'Hi there!')
    React.createElement('p', {}, 'This does not work!')
)

```

**Workarounds**:

- Wrap adjacent elements with a `<div>` or other "container" element. This may
  result in "div soup" as a result of all the wrapping `<div>`s (or other
  elements) with no semantic meaning.

- Wrap adjacent elements in a JS **array**:

```javascript
return (
    [
        // Note that you must add a 'key' on each element in the array
        <ElementOne key="key-one">
            { ... }
        </ElementOne>,
        <ElementTwo key="key-two">
            { ... }
        </ElementTwo>
    ]
)
```

This solution is not common, as it's a little cumbersome.
