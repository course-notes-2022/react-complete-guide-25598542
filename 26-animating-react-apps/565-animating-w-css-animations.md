# Animating with CSS Animations

CSS gives us a way of adding more complex animations our elements: **CSS
animations**.

Consider the modal that appears when clicking the "Create New Challenge" button.
Let's imagine that we want that modal to slide up from the bottom and fade into
view.

Inspect the `Header.jsx` component file. The challenge modal is being rendered
conditionally here based on the value of the `isCreatingChallenge` piece of
state:

```jsx
import { useState } from 'react';

import NewChallenge from './NewChallenge.jsx';

export default function Header() {
  const [isCreatingNewChallenge, setIsCreatingNewChallenge] = useState();

  function handleStartAddNewChallenge() {
    setIsCreatingNewChallenge(true);
  }

  function handleDone() {
    setIsCreatingNewChallenge(false);
  }

  return (
    <>
      {isCreatingNewChallenge && <NewChallenge onDone={handleDone} />}

      <header id="main-header">
        <h1>Your Challenges</h1>
        <button onClick={handleStartAddNewChallenge} className="button">
          Add Challenge
        </button>
      </header>
    </>
  );
}
```

We can't use a **transition** here to animate the modal from an **initial**
state to a **final** state (at least not the way this app is built), because
there **is no initial value** since the modal isn't in the DOM at the start.

## CSS Animations

We can use CSS animations to help. An animation is a **series of steps** we want
our animation to take: an initial state, an end state, and as many intermediate
states as we want. Add the following to `index.css`:

```css
.modal {
  top: 10%;
  border-radius: 6px;
  padding: 1.5rem;
  width: 30rem;
  max-width: 90%;
  z-index: 10;
  animation-name: slide-up-fade-in;
  animation-duration: 300ms;
  animation-timing-function: ease-out;
}

@keyframes slide-up-fade-in {
  0% {
    transform: translateY(30px);
    opacity: 0;
  }
  100% {
    transform: translateY(0);
    opacity: 1;
  }
}
```

- The `@keyframes` keyword allows us to define a **name** that refers to our
  animation

- We can define a **starting state** with the `0%` rule, an **ending state**
  with the `100%` rule, and as many intermediate states as we wish for what the
  animation state should be (e.g. at 25% complete, 50% complete...). **CSS will
  take care of animating between each state**.

- We apply the animation to the element that we want using the `animation-name`
  attribute and setting it to the value we specifed in `@keyframes`.

Save changes and note that our modal is now sliding up and fading in from the
bottom **with CSS animations**!
