# Animating with CSS Transitions

You might not **need** an animation library like `Framer Motion`. Plain CSS
might be enough!

Let's add a CSS transition to the application. Open the `ChallengeItem.jsx`
file, and locate the `.challenge-item-details-icon` `<span>` tag. Locate the
class name in the `index.css` file, and note he following style rules:

```css
.challenge-item-details-icon {
  display: inline-block;
  font-size: 0.85rem;
  margin-left: 0.25rem;
  transition-property: transform;
  transition-duration: 250ms;
  transition-timing-function: ease-out;
}

.challenge-item-details.expanded .challenge-item-details-icon {
  transform: rotate(180deg);
}
```

In order to execute the animation, we need to make sure the `.expanded` class is
added to the element (or an element **above** the element). Let's add it
conditionally to the containing `div`:

```jsx
<div
  className={
    isExpanded ? `challenge-item-details expanded` : 'challenge-item-details'
  }
>
  <p>
    <button onClick={onViewDetails}>
      View Details <span className="challenge-item-details-icon">&#9650;</span>
    </button>
  </p>

  {isExpanded && (
    <div>
      <p className="challenge-item-description">{challenge.description}</p>
    </div>
  )}
</div>
```

Note that our arrow is now **animated**.
