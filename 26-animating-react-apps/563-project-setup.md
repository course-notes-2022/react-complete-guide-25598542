# Project Setup and Overview

Download and extract the starter project. Install the required npm packages and
run `npm run dev` to start the project locally. The application is already
_working_ but it doesn't have any animations yet. We'll begin changing that in
the next lesson.
