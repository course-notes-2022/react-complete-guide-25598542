# Intro

In this module, we'll learn how to **animate** React apps. Animations enhance
our application and the user's engagement with it. We'll learn:

- "Just CSS" might be enough!
- Building more complex animations with **Framer Motion**
  - Animating elements in and out
  - Scroll-based animations
