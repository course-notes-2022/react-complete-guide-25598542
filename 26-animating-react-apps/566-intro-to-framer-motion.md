# Intro to Framer Motion

CSS transitions and animations are powerful, but they have limitations. For
example, you can use animations to animate the **appearance** of elements, but
not the **disappearance**. Also, complex animations are _possible_ but not easy
to implement.

Another thing that is pretty complex to achieve with plain CSS is creating more
_natural-looking_ animations. For these reasons, you might want to use libraries
such as `Framer Motion`.

To use Framer Motion, you should install it using `npm install framer-motion`.
[Check out the Framer Motion docs](https://www.framer.com/motion/?utm_source=motion-readme-docs)
for detailed information.
